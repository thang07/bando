<?php
/**
 * Created by PhpStorm.
 * User: thangnm
 * Date: 1/11/14
 * Time: 9:37 AM
 */

include 'admin/include/config.php';

function connectdb()
{
    global $_host, $_dbname, $_dbuser, $_dbpass;
    try {
        $dbh = new PDO("mysql:host=$_host;dbname=$_dbname", $_dbuser, $_dbpass, array(PDO::ATTR_PERSISTENT => true));
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->exec('SET NAMES utf8');
        return $dbh;
    } catch (PDOException $e) {
        echo "Connection error: " . $e->getMessage();
    }
}

function finsert($_string_insert,$value){
    $db=connectdb();
    $query=$db->prepare($_string_insert);
    $query->execute($value);
    if($query->rowCount()>0){
        $db=null;
        return true;
    }else return false;
}

function fselect($_table_name,$_column){
    $db=connectdb();
    $_result=$db->prepare("SELECT * FROM ".$_table_name." ORDER BY ".$_column);
    $_result->execute();
    unset($db);
    if(!$_result){
        return false;
    }else
        return $_result;
}
function fselect_id($_string_select,$_value){
    $db=connectdb();
    $_result=$db->prepare($_string_select);
    $_result->execute(array($_value));
    unset($db);
    if(!$_result){
        return false;
    }else
        return $_result;
}
//fdelete is delete
function fdelete($table,$column,$id){
    $db=connectdb();
    $query=$db->prepare("DELETE FROM ".$table. " WHERE ".$column." = ?");
    if($query->execute(array($id))){
        return true;
    }else return false;
}
function fupdate($string_update,$value){
    $db=connectdb();
    $_result=$db->prepare($string_update);
    $_result->execute($value);
    if($_result->rowcount()>0){
        $db=null;
        return true;
    }else return false;
}
