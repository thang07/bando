<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 2/3/15
 * Time: 1:42 PM
 */
require_once 'functions.php';

function getParameterById($id_marker){
$query="select * from parameter where id_marker=?";
    $result=fselect_id($query,$id_marker);
    return $result->fetchAll();
}

//function getParameter(){
//    $result=fselect("parameter","id");
//    return $result->fetchAll();
//}

function getParameter(){
    $result=fselect("parameter_street","id_parameter");
    return $result->fetchAll();
}

//function getAllImg(){
//    $result=fselect("img_street","id");
//    return $result->fetchAll();
//}

function updatePara($values){
    $query="update parameter_street set cycle=?,time_green_horizontal=?,time_green_vertical=?,time_yellow=? where id_parameter=?";

    if(fupdate($query,$values)){
        return true;
    }else {return false;}
}
