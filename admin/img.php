
<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 2/6/15
 * Time: 3:23 PM
 */

require 'include/smarty.php';
require 'include/img_function.php';

session_start();

if (!isset($_SESSION['Login'])) {
    $smarty->assign('title', 'Add Images');
    header("location:login.php");
}

else if (isset($_REQUEST['addImg']) == "Addimg") {

    $smarty->assign('idPara', $_REQUEST['idPara']);
    $smarty->assign('direc',$_REQUEST['direc']);
    $smarty->display('addImg.tpl');
}

else if (isset($_REQUEST['action']) == "add") {
    $id_Para = $_POST['id_idPara'];
    $direc=$_POST['direc'];
    //$file_name_bottom=$_POST['file_name_bottom'];

    if($direc=="along"){

   // $_flag = $_FILES["file_name"];
    $temp_top = preg_split('/[\/\\\\]+/', $_FILES["file_name_top"]["name"]);
    $name_file_top = $temp_top[count($temp_top) - 1];
    $dire_top = "../upload/";
    $upload_file_top = $dire_top .$name_file_top;


    $temp_bottom=preg_split('/[\/\\\\]+/', $_FILES["file_name_bottom"]["name"]);
    $name_file_bottom=$temp_bottom[count($temp_bottom) - 1];
    $dire_bottom = "../upload/";
    $upload_file_bottom = $dire_bottom .$name_file_bottom;

        $top=move_uploaded_file($_FILES["file_name_top"]["tmp_name"], $name_file_top);
    $bottom=move_uploaded_file($_FILES["file_name_bottom"]["tmp_name"], $name_file_bottom);

   $values_top_bottom = array($id_Para, $name_file_top,$name_file_bottom);

     insertImgTopBottom($values_top_bottom);

    }
   else{
        $temp_left = preg_split('/[\/\\\\]+/', $_FILES["file_name_left"]["name"]);
        $name_file_left = $temp_left[count($temp_left) - 1];
        $dire_left= "../upload/";
        $upload_file_left = $dire_left .$name_file_left;


        $temp_right=preg_split('/[\/\\\\]+/', $_FILES["file_name_right"]["name"]);
        $name_file_right=$temp_right[count($temp_right) - 1];
        $dire_right = "../upload/";
        $upload_file_right = $dire_right .$name_file_right;
       $left=move_uploaded_file($_FILES["file_name_left"]["tmp_name"], $name_file_left);
        $right=move_uploaded_file($_FILES["file_name_right"]["tmp_name"], $name_file_right);
       $values_left_right = array($id_Para,$name_file_left,$name_file_right);

       insertImgLeftRight($values_left_right);
   }
    header("location:marker.php?listMarker=listAllMarker");

}