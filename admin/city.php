<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 1/15/14
 * Time: 2:13 PM
 */

require 'include/smarty.php';
require 'include/city_function.php';


session_start();

if (!isset($_SESSION['Login'])) {
    header("location:login.php");
}

//Begin Display form edit new location
if (isset($_REQUEST["editLo"]) == 'edit') {
    $id_location = $_GET['loId'];
    $getLocation = getLocation_id($id_location);
    $smarty->assign("getCities", $getLocation);
    $smarty->assign("title", "Update Location");
    $smarty->assign("url", "http://" . $_SERVER['HTTP_HOST']);
    $smarty->display("updateCity.tpl");
}
//End Display form edit location

//Begin Display form add new location
else if (isset($_REQUEST['frmAdd']) == "frmAdd") {

    $smarty->assign('title', 'Add Location');
    $smarty->display('addCity.tpl');
}
//End Display form add location
//Begin Display list location
else if (isset($_REQUEST['list']) == "listLocation") {
    $listLocation = getListCity();
    $smarty->assign("listLocation", $listLocation);
    $smarty->assign("title", "Manager Location");
    $smarty->assign("url", "http://" . $_SERVER['HTTP_HOST']);
    $smarty->display("listCity.tpl");
}
//End Display lise location

//Begin Insert new location
else if (isset($_REQUEST['action']) == "add") {

    $_name = $_POST['name'];
    $_lat = $_POST['lat'];
    $_long = $_POST['long'];

    $values = array($_name, $_lat, $_long);

    insertCity($values);
    header("location:city.php?list=listCity");

}
//End Insert new location

//Begin update location
else if (isset($_REQUEST['act']) == "updateCity") {
    $id_location = $_POST['location_id'];
    $_name_city = $_POST['name'];
    $_lat_code = $_POST['lat_code'];
    $_long_code = $_POST['long_code'];

    $values = array($_name_city, $_lat_code, $_long_code, $id_location);
    updateCity($values);
    header("location:city.php?list=listCity");


}//End update location

//Begin delete location
else if (isset($_REQUEST['del']) == "del") {
    if (isset($_REQUEST['loId'])) {
        delLocation($_REQUEST['loId']);
        header("location:city.php?list=listLocation");
    }
} //End update location
else {
    header("location:index.php");
}
