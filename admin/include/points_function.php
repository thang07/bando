<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 1/30/15
 * Time: 3:52 PM
 */

require_once 'functions.php';

//get all data from points table
function getListAllPoints(){
    $query=fselect("points","id");
    return $query->fetchAll();
}
//insert new records into table points
function insertPoints($values){
    $query="INSERT INTO points (name_street,address_start,address_end,lat_start,long_start,lat_end,long_end,color)
            VALUES (?,?,?,?,?,?,?,?)";
    if(finsert($query,$values)){
        return true;
    }else {return false;}
}

//get data from points table by point id
function getPointsById($idPoint){
    $query="SELECT * FROM points WHERE id=?";
    $result=fselect_id($query,$idPoint);
    return $result->fetchAll();

}
//Update points table
function updatePoints($values){
    $query="UPDATE points SET name_street=?,address_start=?,address_end=?,lat_start=?,long_start=?,lat_end=?,long_end=?,color=?
            where id=?";
    if(fupdate($query,$values))
        return true;
    else return false;
}
function delPoints($id){
    return $result=fdelete("points","id",$id);
}