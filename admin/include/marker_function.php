<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 2/2/15
 * Time: 2:23 PM
 */
require_once 'functions.php';

//function getListAllMarker(){
//    $query=fselect("marker","id");
//    return $query->fetchAll();
//}

function getListAllMarker(){
    $db=connectdb();
    $_result=$db->prepare("SELECT m.id,lat_marker,long_marker,marker_name,color, c.name as 'city_name' from marker m, city c where m.id_city=c.id_city ");
    $_result->execute();
    unset($db);
    if(!$_result){
        return false;
    }else
        return $_result;
}

function insertMarker($values){
    $query="INSERT INTO marker (lat_marker,long_marker,marker_name,color,id_city) VALUES (?,?,?,?,?)";
    if(finsert($query,$values)){
        return true;
    }else {return false;}
}


function getEditMarker($id){
    $query="SELECT * FROM marker where id=?";
    $result=fselect_id($query,$id);
    return $result->fetchAll();
}

function updateMarker($values){
    $query="UPDATE marker set lat_marker=?,long_marker=?, marker_name=?, color=?,id_city=? where id=?";
    if(fupdate($query,$values)){
        return true;
    }else {return false;}
}
