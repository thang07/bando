<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 2/2/15
 * Time: 4:52 PM
 */
require_once 'functions.php';

function insertParameter($values){
    $query="INSERT INTO parameter (id_marker,name_street,cycle,speed,consistence,time_green,time_yellow,direction) VALUES (?,?,?,?,?,?,?,?)";
    if(finsert($query,$values)){
        return true;
    }else {return false;}
}

function insertParameterStreet($values){
    $query="INSERT INTO parameter_street (id_marker,name_street_horizontal,name_street_vertical,cycle,speech_horizontal,
                                          speech_vertical,consistence_horizontal,consistence_vertical,time_green_horizontal,
                                          time_green_vertical,time_yellow,img_top_vertical,img_bottom_vertical,img_left_horizontal,img_right_horizontal)
                                          VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    if(finsert($query,$values)){
        return true;
    }else {return false;}
}

function getParameter(){
    return fselect("parameter_street","id_marker");
}

function editParameter($values){
    $query="update parameter set name_street =? ,cycle=?,speed=?, consistence=?,time_green=?,time_yellow=? where id=?";
    if(fupdate($query,$values)){
        return true;
    }else {return false;}
}

function getParameter_Byid($id){
    $query="select * from parameter_street where id_parameter=?";
    $result= fselect_id($query,$id);
    return $result->fetchAll();

}
function getNameMarkerById($id){
    $query="select * from marker where id=?";
    $result= fselect_id($query,$id);
    return $result->fetchAll();
}
function updatePara($values){
    $query="update parameter_street set cycle=?, speech_horizontal=?,speech_vertical=?, consistence_horizontal=?,consistence_vertical=?,time_green_vertical=? where id_parameter=?";

    if(fupdate($query,$values)){
        return true;
    }else {return false;}
}

function updateImgVerticalTop($values){
    $query="update parameter_street set img_top_vertical=? where id_parameter=?";
    if(fupdate($query,$values)){
        return true;
    }else {return false;}
}
//update Img Vertical Bottom
function updateImgVerticalBottom($values){
    $query="update parameter_street set img_bottom_vertical=? where id_parameter=?";
    if(fupdate($query,$values)){
        return true;
    }else {return false;}
}
//update Horizontal Left
function updateImgHorizontalLeft($values){
    $query="update parameter_street set img_left_horizontal=? where id_parameter=?";
    if(fupdate($query,$values)){
        return true;
    }else {return false;}
}
//update Img Horizontal Right
function updateImgHorizontalRight($values){
    $query="update parameter_street set img_right_horizontal=? where id_parameter=?";
    if(fupdate($query,$values)){
        return true;
    }else {return false;}
}
//get Img Vertical Top
function getImgTopVer($id){
    $query="select img_top_vertical from parameter_street where id_parameter=?";
    $result= fselect_id($query,$id);
    return $result->fetchAll();
}
//get Img Bottom Vertical
function getImgBottomVer($id){
    $query="select img_bottom_vertical from parameter_street where id_parameter=?";
    $result= fselect_id($query,$id);
    return $result->fetchAll();
}
//get Img Horizontal Right
function getImgHorizontalRight($id){
    $query="select img_right_horizontal from parameter_street where id_parameter=?";
    $result= fselect_id($query,$id);
    return $result->fetchAll();
}
//get Img Horizontal Left
function getImgHorizontalLeft($id){
    $query="select img_left_horizontal from parameter_street where id_parameter=?";
    $result= fselect_id($query,$id);
    return $result->fetchAll();
}