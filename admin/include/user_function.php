<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 1/11/14
 * Time: 10:21 AM
 */
//
require_once 'functions.php';
session_start();
function insertUsers($value){
    $str="INSERT INTO users(email,password,names,phone,address,role) values(?,?,?,?,?,?)";
    if(finsert($str,$value)){
        return true;
    }else return false;
}

//get all user
function getListUser(){
 $query=fselect("users","id");
    return $query->fetchAll();
}



function loginUser($value,$pass){
    $db=connectdb();
    $stmt = $db->prepare("SELECT * from users where (email='$value' or phone = '$value') and password='$pass'");
    $stmt->execute();
    $result = $stmt->fetchAll();
    if ($result != false) {
        $_email = "";
        $_pass = "";
        $_role = "";
        $_user_id="";
        $_name="";
        $_phone="";
        foreach ($result as $row) {
            $_email = $row["email"];
            $_phone=$row["phone"];
            $_pass = $row["password"];
            $_role = $row["role"];
            $_user_id=$row['id'];
            //$_name=$row["names"];
        }
        if (($_email == $value || $_phone==$value) && ($pass == $_pass)){
            $_SESSION['User_id']=$_user_id;
            $_SESSION['Login'] = $_email;
           // $_SESSION['Name_user']=$_name;
            $_SESSION['Role'] = $_role;
            $_SESSION['Email'] = $_email;
            $_SESSION['start'] = time();
            $_SESSION['expire'] = $_SESSION['start'] + (60 * 60);
          return true;

        } else {
            return false;
        }
    }
}
function checkRole($id,$role){
    $db=connectdb();
    $stmt=$db->prepare("select role where id=? and role=?");
    $stmt->execute(array($id, $role));
    if($result = $stmt->fetchAll()){
        return true;
    }else{
        return false;
    }
}
//function check email of users
function checkEmail($_email){
    $db=connectdb();
    $stm=$db->prepare("select email from users where email=? limit 100 ");
    $stm->execute(array($_email));
    $result = $stm->fetchAll();
    if($stm->rowCount()>0)
    {
        $email="";
        foreach($result as $row){
            $email=$row['email'];
        }
        if($_email==$email){
            return true;
        }else{
            return false;
        }
    }
}
function checkPhone($_phone){
    $db=connectdb();
    $stm=$db->prepare("select phone from users where phone=?");
    $stm->execute(array($_phone));
    $result = $stm->fetchAll();
    if($stm->rowCount()>0)
    {
        $phone="";
        foreach($result as $row){
            $phone=$row['phone'];
        }
        if($_phone==$phone){
            return true;
        }else{
            return false;
        }
    }
}

function getUserId($id){
    $query="SELECT * FROM users WHERE id=?";
    $result=fselect_id($query,$id);
    return $result->fetchAll();

}
function updatePassword($password){//function update password of user
    $query="UPDATE users SET password =? WHERE id=?";
    if(fupdate($query,$password)){
        return true;
    }else return false;
}
function updateProfile($value){
    $query="UPDATE users SET name=?,email=?,phone=?, address=? WHERE id=?";
    if(fupdate($query,$value)){
        return true;
    }else{
        return false;
    }
}
function updateUser($value){
    $query="UPDATE users SET email=?, names=?,phone=?, address=?,role=? WHERE id=?";
    if(fupdate($query,$value)){
        return true;
    }else{
        return false;
    }
}

