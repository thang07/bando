<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 1/21/14
 * Time: 2:07 PM
 */

require 'include/functions.php';

function getListCheckin(){
    $db=connectdb();
    $query="SELECT ck.id as 'id_checkin',
                   ck.park_id as 'park_id',
                   start,
                   stop,
                   time_parking,
                   total_amount,
                   u.fname as 'name_user',
                   v.name as 'name_vehicle',
                   v.num_plate,
                   p.name as 'name_park'
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
             AND isparking = 1";
    $smt=$db->prepare($query);
    $smt->execute();
    return $smt->fetchAll();
}
function getListCheckout(){
    $db=connectdb();
    $query="SELECT ck.id as 'id_checkin',
                   ck.park_id as 'park_id',
                   start,
                   stop,
                   time_parking,
                   total_amount,
                   u.fname as 'name_user',
                   v.name as 'name_vehicle',
                   v.num_plate,
                   p.name as 'name_park'
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
              AND isparking = 0 ORDER BY ck.id DESC";

    $smt=$db->prepare($query);
    $smt->execute();
    return $smt->fetchAll();
}

function getListCheckoutByDay($from,$to){
    $db=connectdb();
    $query="SELECT ck.id as 'id_checkin',
                   ck.park_id as 'park_id',
                   start,
                   stop,
                   time_parking,
                   total_amount,
                   u.fname as 'name_user',
                   v.name as 'name_vehicle',
                   v.num_plate,
                   p.name as 'name_park'
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
             and isparking = 0
             and start BETWEEN '".$from."' and '".$to."'
             ORDER BY ck.id DESC";

    $smt=$db->prepare($query);
    $smt->execute();
    return $smt->fetchAll();
//    return $query;
}

function SumAllCheckout($type){
    $db=connectdb();
    $query="SELECT sum(".$type.") as total
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
             and isparking = 0";

    $smt=$db->prepare($query);
    $smt->execute();
    foreach($smt->fetchAll() as $item)
    {
        return $item['total'];
    };
//    return $query;
}

function SumAllCheckoutByUser($type,$user_id){
    $db=connectdb();
    $query="SELECT sum(".$type.") as total
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
             AND ck.user_id=?
             and isparking = 0";

    $smt=$db->prepare($query);
    $smt->execute(array($user_id));
    foreach($smt->fetchAll() as $item)
    {
        return $item['total'];
    };
//    return $query;
}

function SumCheckoutByDay($from,$to,$type){
    $db=connectdb();
    $query="SELECT sum(".$type.") as total
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
             and isparking = 0
             and start BETWEEN '".$from."' and '".$to."'";

    $smt=$db->prepare($query);
    $smt->execute();
    foreach($smt->fetchAll() as $item)
    {
        return $item['total'];
    };
//    return $query;
}

function getListCheckinOfUser($_user_id){
    $db=connectdb();
    $query="SELECT ck.id as 'id_checkin',
                   ck.park_id as 'park_id',
                   start,
                   stop,
                   time_parking,
                   total_amount,
                   u.fname as 'name_user',
                   v.name as 'name_vehicle',
                   v.num_plate,
                   p.name as 'name_park'
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
             AND ck.user_id=?
             AND isparking = 1";
    $smt=$db->prepare($query);
    $smt->execute(array($_user_id));
    return $smt->fetchAll();
}

function getListCheckOutOfUser($_user_id){
    $db=connectdb();
    $query="SELECT ck.id as 'id_checkin',
                   ck.park_id as 'park_id',
                   start,
                   stop,
                   time_parking,
                   total_amount,
                   u.fname as 'name_user',
                   v.name as 'name_vehicle',
                   v.num_plate,
                   p.name as 'name_park'
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
             AND ck.user_id=?
             AND isparking = 0";
    $smt=$db->prepare($query);
    $smt->execute(array($_user_id));
    return $smt->fetchAll();
}

function getListCheckOutOfUserByDay($from,$to,$_user_id){
    $db=connectdb();
    $query="SELECT ck.id as 'id_checkin',
                   ck.park_id as 'park_id',
                   start,
                   stop,
                   time_parking,
                   total_amount,
                   u.fname as 'name_user',
                   v.name as 'name_vehicle',
                   v.num_plate,
                   p.name as 'name_park'
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
             AND ck.user_id=?
             AND isparking = 0
             and start BETWEEN '".$from."' and '".$to."'";
    $smt=$db->prepare($query);
    $smt->execute(array($_user_id));
    return $smt->fetchAll();
}

function SumCheckoutByDayOfUser($from,$to,$type,$_user_id){
    $db=connectdb();
    $query="SELECT sum(".$type.") as total
             FROM checkin ck,
                  users u,
                  vehicle v,
                  park p
             WHERE ck.park_id=p.id
             AND ck.user_id=u.id
             AND ck.vehicle_id=v.id
             AND ck.user_id=?
             AND isparking = 0
             and start BETWEEN '".$from."' and '".$to."'";

    $smt=$db->prepare($query);
    $smt->execute(array($_user_id));
    foreach($smt->fetchAll() as $item)
    {
        return $item['total'];
    };
//    return $query;
}

function delCheckin($check_id){
    return fdelete("checkin","id",$check_id);
}
