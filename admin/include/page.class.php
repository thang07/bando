<?php
class Paging
{
	
    public static function createPaging($total, $current, $perPage, $url = ''){
        $numPage = floor($total/ $perPage);
        if (($total/ $perPage) - $numPage > 0){
            $numPage += 1;
        }
        $html = '<ul class="pagination">';
        if ($numPage == 1){
            return '';
        }
        if ($current == 1){
//            $html .= '<li class="disabled"><a href=""#">Start</a></li>';
            $html .= '<li class="disabled"><a href="#">Previous</a></li>';
        } else {
            $html .= "<li><a href='$url=1'>First</a></li>";
            $html .= "<li><a href='$url=".($current - 1). "'>Previous</a></li>";
        }
        if($current <=3){
            for($i=1; ($i<=5) and ($i <= $numPage); $i++){
                if ($i == $current){
                    $html .= '<li class="active"><a>'.$i. '</a></li>';
                }else{
                    $html .= "<li><a href='$url=$i'>".$i."</a></li>";
                }
            }
        }else{
            if ($numPage >= $current + 2){
                for($i=$current-2; ($i <=$current+2) and ($i <= $numPage); $i++){
                    if ($i == $current){
                        $html .= '<li class="active"><a>'.$i. '</a></li>';
                    }else{
                        $html .= "<li><a href='$url=$i'>".$i."</a></li>";
                    }
                }
            }else{
                for($i = $numPage - 4;$i <= $numPage; $i++){
                    if($i > 0){
                        if ($i == $current){
                            $html .= '<li class="active"><a>'.$i. '</a></li>';
                        }else{
                            $html .= "<li><a href='$url=$i'>".$i."</a><li>";
                        }
                    }                        
                }
            }
        }
        if ($current == $numPage){
            $html .= '<li class="disabled"><a href="#">Next</a></li>';
            $html .= '<li class="disabled"><a href="#">Last</a></li>';
        } else {
            $html .='<li class="disabled"><a href="#">...</a> </li>';
            $html .= "<li><a href='$url=$numPage'>".$numPage."</a></li>";
            $html .= "<li><a href='$url=".($current + 1). "'>Next</a></li></ul>";
//            $html .= "<li><a href='$url=$numPage'>.".End.".</a></li>";
        }
        return $html;
    }
}
?>