
<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 1/15/14
 * Time: 2:01 PM
 */
require_once 'functions.php';

function insertCity($values){
    $query="INSERT INTO city (name,lat_code,long_code) VALUES (?,?,?)";
    if(finsert($query,$values)){
        return true;
   }else {return false;}
}
function getLocation_id($id){
    $query="select * from city where id_city=? order by id_city limit 90";
    $result=fselect_id($query,$id);
    return $result->fetchAll();

}

function getListCity()
{
    $query = fselect("city", "id_city");
    return $query->fetchAll();
}


function delLocation($id){
    return fdelete("city","id_city",$id);
}

function updateCity($values){
    $query="UPDATE city  SET name=?, lat_code=?,long_code=? WHERE id_city=?";
    if(fupdate($query,$values)){
        return true;
    }else {return false;}


}



