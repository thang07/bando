<?php
/**
 * Created by PhpStorm.
 * User: thangnm
 * Date: 1/11/14
 * Time: 9:41 AM
 */

require 'libs/Smarty.class.php';

$smarty=new Smarty();
$smarty->debugging = false;
$smarty->caching = false;
$smarty->cache_lifetime = 120;
