<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 2/2/15
 * Time: 4:53 PM
 */

require 'include/smarty.php';
require 'include/parameter_function.php';

session_start();

if (!isset($_SESSION['Login'])) {
    header("location:login.php");
}
//Begin Display form add new location
else if (isset($_REQUEST['addPara']) == "AddPara") {
    $smarty->assign("idMarker",$_GET['idMarker']);
    $smarty->assign('title', 'Add Parameter');
    $smarty->display('addParameter_Street.tpl');
}

else if(isset($_REQUEST['action'])=="addPara"){
    $id_marker=$_POST['id_marker'];
    $name_street_horizontal=$_POST['name_street_horizontal'];
    $speed_horizontal=$_POST['speed_horizontal'];
    $consistence_horizontal=$_POST['consistence_horizontal'];
    $time_green_horizontal=$_POST['time_green_horizontal'];

    $cycle=$_POST['cycle'];
    $time_yellow=$_POST['time_yellow'];
    $name_street_vertical=$_POST['name_street_vertical'];
    $speed_vertical=$_POST['speed_vertical'];
    $consistence_vertical=$_POST['consistence_vertical'];
    $time_green_vertical=$_POST['time_green_vertical'];
///////
    $temp_left_hori = preg_split('/[\/\\\\]+/', $_FILES["file_name_left_ho"]["name"]);
    $name_file_left_hori = $temp_left_hori[count($temp_left_hori) - 1];
    $dire_left_hori = "../upload/";
    $upload_file_left_hori = $dire_left_hori .$name_file_left_hori;

    $temp_right_ho = preg_split('/[\/\\\\]+/', $_FILES["file_name_right_ho"]["name"]);
    $name_file_right_hori = $temp_right_ho[count($temp_right_ho) - 1];
    $dire_right_hori = "../upload/";
    $upload_file_right_hori = $dire_right_hori .$name_file_right_hori;

    $temp_top_verti = preg_split('/[\/\\\\]+/', $_FILES["file_name_top_vertical"]["name"]);
    $name_file_top_verti = $temp_top_verti[count($temp_top_verti) - 1];
    $dire_top_verti = "../upload/";
    $upload_file_top_verti = $dire_top_verti .$name_file_top_verti;

    $temp_bottom_verti = preg_split('/[\/\\\\]+/', $_FILES["file_name_bottom_vertical"]["name"]);
    $name_file_bottom_verti = $temp_bottom_verti[count($temp_bottom_verti) - 1];
    $dire_bottom_verti = "../upload/";
    $upload_file_bottom_verti = $dire_bottom_verti .$name_file_bottom_verti;

    $left_hori=move_uploaded_file($_FILES["file_name_left_ho"]["tmp_name"], $upload_file_top_verti);
    $right_hori=move_uploaded_file($_FILES["file_name_right_ho"]["tmp_name_right"], $name_file_right_hori);
    $top_verti=move_uploaded_file($_FILES["file_name_top_vertical"]["tmp_name_top"], $name_file_top_verti);
    $bottom_verti=move_uploaded_file($_FILES["file_name_bottom_vertical"]["tmp_name_bottom"], $name_file_bottom_verti);

    $values=array($id_marker,$name_street_horizontal,$name_street_vertical,$cycle,$speed_horizontal,
                  $speed_vertical,$consistence_horizontal,$consistence_vertical,$time_green_horizontal,$time_green_vertical,
                  $time_yellow,$name_file_top_verti,$name_file_bottom_verti,$name_file_left_hori,$name_file_right_hori);
    insertParameterStreet($values);
    header("location:parameter.php?listParameter=listPara");
}
else if(isset($_REQUEST['listParameter'])=="listPara"){
    $listParameter=getParameter();
    $smarty->assign("listParameter",$listParameter);
    $smarty->assign("title", "List Parameter");
    $smarty->assign("url", "http://" . $_SERVER['HTTP_HOST']);
    $smarty->display("listParameter.tpl");
}
else if(isset($_REQUEST['editPara'])=="edit"){
    $id=$_GET['idPa'];
    header("location:parameter.php?listParameter=listPara");

}
else if(isset($_REQUEST['viewPa'])=="view"){
    $id_para=$_GET['idPara'];
    $idMar=$_GET['idMa'];
    $getNameMaker=getNameMarkerById($idMar);
    $listP=getParameter_Byid($id_para);

    $smarty->assign("viewListParamater",$listP);
    $smarty->assign("getNameMaker",$getNameMaker);

    $smarty->assign("title", "List Parameter");
    $smarty->display("viewPara.tpl");
}
else if(isset($_REQUEST['upd'])=="upPara"){
    $id_parameter=$_POST['id_parama'];
    $idMa=$_POST['id_marker'];
    $consistence_horizontal=$_POST['consistence_horizontal'];
    $time_green_horizontal=$_POST['time_green_horizontal'];
    $cycle=$_POST['cycle'];
    $time_yellow=$_POST['time_yellow'];
    $consistence_vertical=$_POST['consistence_vertical'];
    $time_green_vertical=$_POST['time_green_vertical'];

    $cycle=$_POST['cycle'];

    $speed_horizontal=$_POST['speed_horizontal'];
    $speed_vertical=$_POST['speed_vertical'];
    $consistence_vertical=$_POST['consistence_vertical'];
    $time_green_vertical=$_POST['time_green_vertical'];

    $values=array($cycle,$speed_horizontal,$speed_vertical,$consistence_horizontal,$consistence_vertical,$time_green_vertical,$id_parameter);
    updatePara($values);
    $smarty->assign("title", "Google map Page");
    header("location:parameter.php?viewPa=view&idPara=$id_parameter&idMa=$idMa");
   // header("location:parameter.php?listParameter=listPara");

}
else if(isset($_REQUEST['addImgVetTop'])=="true"){
    $id_parameter=$_GET['id_para'];
    $id_Ma=$_GET['idMa'];
    $smarty->assign('id_parameter', $id_parameter);
    $smarty->assign('id_Ma', $id_Ma);

    $smarty->display('updateImgVerTop.tpl');
}
else if(isset($_REQUEST['up'])=="updateImgVeTop"){
    $id_parameter=$_POST['id_para'];
    $idMa=$_POST['id_Ma'];
    $_flag = $_FILES["file_img_Vertop"];

    $result=getImgTopVer($id_parameter);
    foreach($result as $i){
        $value_img=$i['img_top_vertical'];
    }
    $_value_flag=$value_img;
    $file = "";
    try{
    if(isset($_flag))

        $temp_top_verti = preg_split('/[\/\\\\]+/', $_FILES["file_img_Vertop"]["name"]);
        $name_file_top_verti = $temp_top_verti[count($temp_top_verti) - 1];
        $dire_top_verti = "../upload/";
        $upload_file_top_verti = $dire_top_verti .$name_file_top_verti;

        if(move_uploaded_file($_FILES["file_img_Vertop"]["tmp_name"],$upload_file_top_verti))
        {
            $file = $name_file_top_verti;
        }else
         $file=$_value_flag;
        $values=array($file,$id_parameter);

        updateImgVerticalTop($values);

    }catch (Exception $ex){
        echo 'erro!';
    }

    header("location:parameter.php?viewPa=view&&idPara=$id_parameter&&idMa=$idMa");

}//hien thi form update img
else if(isset($_REQUEST['addImgVetBottom'])=="true"){
    $id_parameter=$_GET['id_para'];
    $id_Ma=$_GET['idMa'];
    $smarty->assign('id_parameter', $id_parameter);
    $smarty->assign('id_Ma', $id_Ma);

    $smarty->display('updateImgVerBottom.tpl');
}//action update img Vertical Bottom
else if(isset($_REQUEST['upBottom'])=="updateImgVetBottom"){
    $id_parameter=$_POST['id_para'];
    $idMa=$_POST['id_Ma'];
    $_flag = $_FILES["file_img_VerBottom"];

    $result=getImgBottomVer($id_parameter);
    foreach($result as $i){
        $value_img=$i['img_bottom_vertical'];
    }
    $_value_flag=$value_img;
    $file = "";
    try{
        if(isset($_flag))

        $temp_bottom_verti = preg_split('/[\/\\\\]+/', $_FILES["file_img_VerBottom"]["name"]);
        $name_file_bottom_verti = $temp_bottom_verti[count($temp_bottom_verti) - 1];
        $dire_bottom_verti = "../upload/";
        $upload_file_bottom_verti = $dire_bottom_verti .$name_file_bottom_verti;

        if(move_uploaded_file($_FILES["file_img_VerBottom"]["tmp_name"],$upload_file_bottom_verti))
        {
            $file = $name_file_bottom_verti;
        }else
            $file=$_value_flag;
        $values=array($file,$id_parameter);

        updateImgVerticalBottom($values);

    }catch (Exception $ex){
        echo 'erro!';
    }

    header("location:parameter.php?viewPa=view&&idPara=$id_parameter&&idMa=$idMa");
}//Hien thi form img Horiz Right
else if(isset($_REQUEST['addImgHorizonRight'])=="true"){
    $id_parameter=$_GET['id_para'];
    $id_Ma=$_GET['idMa'];
    $smarty->assign('id_parameter', $id_parameter);
    $smarty->assign('id_Ma', $id_Ma);

    $smarty->display('updateImgHorizonRight.tpl');
}
//action update img Hori Right
else if(isset($_REQUEST['updateHoRight'])=="updateHorRight"){

    $id_parameter=$_POST['id_para'];
    $idMa=$_POST['id_Ma'];
    $_flag = $_FILES["file_img_Hor_Right"];

    $result=getImgHorizontalRight($id_parameter);
    foreach($result as $i){
        $value_img=$i['img_right_horizontal'];
    }
    $_value_flag=$value_img;
    $file = "";
    try{
        if(isset($_flag))

        $temp_right_ho = preg_split('/[\/\\\\]+/', $_FILES["file_img_Hor_Right"]["name"]);
        $name_file_right_hori = $temp_right_ho[count($temp_right_ho) - 1];
        $dire_right_hori = "../upload/";
        $upload_file_right_hori = $dire_right_hori .$name_file_right_hori;

        if(move_uploaded_file($_FILES["file_img_Hor_Right"]["tmp_name"],$upload_file_right_hori))
        {
            $file = $name_file_right_hori;
        }else
            $file=$_value_flag;
        $values=array($file,$id_parameter);

        updateImgHorizontalRight($values);

    }catch (Exception $ex){
        echo 'erro!';
    }

    header("location:parameter.php?viewPa=view&&idPara=$id_parameter&&idMa=$idMa");

}//Hien thi form img Horizon Left
else if(isset($_REQUEST['addImgHorizonLeft'])=="true"){
    $id_parameter=$_GET['id_para'];
    $id_Ma=$_GET['idMa'];
    $smarty->assign('id_parameter', $id_parameter);
    $smarty->assign('id_Ma', $id_Ma);

    $smarty->display('updateImgHorizonLeft.tpl');
}
else if(isset($_REQUEST['upLeft'])=="updateImgLeft"){
    $id_parameter=$_POST['id_para'];
    $idMa=$_POST['id_Ma'];
    $_flag = $_FILES["file_img_Hor_Left"];

    $result=getImgHorizontalLeft($id_parameter);
    foreach($result as $i){
        $value_img=$i['img_left_horizontal'];
    }
    $_value_flag=$value_img;
    $file = "";
    try{
        if(isset($_flag))

        $temp_left_hori = preg_split('/[\/\\\\]+/', $_FILES["file_img_Hor_Left"]["name"]);
        $name_file_left_hori = $temp_left_hori[count($temp_left_hori) - 1];
        $dire_left_hori = "../upload/";
        $upload_file_left_hori = $dire_left_hori .$name_file_left_hori;

        if(move_uploaded_file($_FILES["file_img_Hor_Left"]["tmp_name"],$name_file_left_hori))
        {
            $file = $name_file_left_hori;
        }else
            $file=$_value_flag;
        $values=array($file,$id_parameter);

        updateImgHorizontalLeft($values);

    }catch (Exception $ex){
        echo 'erro!';
    }

    header("location:parameter.php?viewPa=view&&idPara=$id_parameter&&idMa=$idMa");
}