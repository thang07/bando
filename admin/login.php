<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 1/11/14
 * Time: 10:27 AM
 */

require "include/smarty.php";
require "include/user_function.php";

//session_start();
//session_destroy();
if (isset($_SESSION['Login'])) {
    header("location:index.php");
} else {
    if (!empty($_POST['email']) && !empty($_POST['password'])) {
        $_email= trim($_POST['email']);
       // $_password=sha1(($_POST['password']));
        $_password=(($_POST['password']));
        if(loginUser($_email,$_password)){
            $smarty->assign("User_id",$_SESSION["User_id"]);
            $smarty->assign("Role",$_SESSION["Role"]);
            $smarty->assign("Email",$_SESSION["Email"]);
            $smarty->assign("Login",$_SESSION["Login"]);
            header("location:index.php");
        }
     else {
         $smarty->assign("title","Login Page");
        header("location:login.php");
    }
    }
    $smarty->assign("title","Login Page");
    $smarty->display("login.tpl");
}