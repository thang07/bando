<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 1/11/14
 * Time: 10:22 AM
 */


require 'include/smarty.php';
require 'include/user_function.php';

$db=connectdb();
session_start();
if(!isset($_SESSION['Login'])){
    header("location:login.php");
}

if (isset($_GET["list"])=="listUser") {
        $listUser = getListUser();
        $smarty->assign("listUser", $listUser);
        $smarty->clearCache('listUser.tpl');
        $smarty->assign("title", "User manager");
        $smarty->display('index.tpl');
}

else if (isset($_REQUEST['edit']) == "profile") {

    $getProfile = getUserId($_SESSION['User_id']);
    $smarty->assign("getProfile", $getProfile);
    $smarty->assign("title", "Edit Your Profile");
    $smarty->display('profile.tpl');
}

else if (isset($_REQUEST['display']) == "frmEdit") {
    $getUerId = getUserId($_REQUEST['id']);
    $smarty->assign("getUerId", $getUerId);
    $smarty->assign('title', 'Update user');
    $smarty->display('updateUser.tpl');
}

else if (isset($_REQUEST['act']) == "upUser") {
    $id = $_POST['idUp'];
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $selTypeUser = $_POST['selTypeUser'];
    $value = array($email, $lname, $phone, $address, $selTypeUser, $id);
    $result = updateUser($value);

    $smarty->assign("title", "User manager");
    header('location:users.php?list=listUser');

}
//check email

else if (isset($_GET['email'])) {
    $email = $_GET['email'];
    if (checkEmail($email)) {
        echo "Email already exists, Try other";
    }
}
else if(isset($_GET['phone'])){
    $phone=$_GET['phone'];
    if(checkPhone($phone)){
        echo "Phone already exists";
    }
}
//action addUser //them user
else if (isset($_GET['action']) == "addUser") {

    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $selTypeUser = $_POST['selTypeUser'];
    $values = array($email, sha1($password),$lname, $phone, $address, $selTypeUser);
    if (checkEmail($email) == 0 && checkPhone($phone)==0) {
        $bl = insertUsers($values);
        if ($bl == true) {
            $listUser = getListUser();
            $smarty->assign("listUser", $listUser);
            $smarty->assign('title', 'user mangers');
            $smarty->clearCache('listUser.tpl');
            header('location:users.php?list=listUser');
        }
    } else {
        echo "<script language='javascript'>
            alert('Email or Phone already exists ');
            javascript:window.history.back(-1);
	        </script>";
    }

}
else if (isset($_REQUEST['delUser']) == 'delUser') {
    if ($_GET['id']) {
        $_id_user = $_GET['id'];
        fdelete("users","id",$_id_user);
      }
}
else if (isset($_REQUEST['frmAdd']) == "AddNew") {
    $smarty->assign('title', 'Add user');
    $smarty->display('addUser.tpl');
}
else if (isset($_GET['profile']) == "updateProfile") {
    $name = $_POST['yourname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $value = array($name, $email, $phone, $address, $_SESSION['User_id']);
    $result = updateProfile($value);
    header("location:users.php?edit=profile");

}
else if (isset($_REQUEST['pssw']) == "pss") {
    $newpass = $_POST['new_password'];
    updatePassword(array(sha1($newpass), $_SESSION['User_id']));
    header("location:users.php?edit=profile");
}else{
    header("location:index.php");
}



