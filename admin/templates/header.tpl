<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8">
    <link rel="shortcut icon" href="">

    <title>{$title}</title>

    <link href="templates/css/bootstrap.min.css" rel="stylesheet">
    <link href="templates/css/style.css" rel="stylesheet">
    <link href="templates/css/bootstrap.icon-large.min.css" rel="stylesheet" media="screen">
    <link href="templates/css/font-awesome.css" rel="stylesheet" media="screen">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <script src="templates/js/jquery.js"></script>
    <script src="templates/js/bootstrap.min.js"></script>
    <script type="text/javascript">if (window.location.hash == '#_=_')window.location.hash = '';</script>
    <script>
        $(function () {
            var url = window.location.pathname,
                    urlRegExp = new RegExp(url.replace(/\/$/, ''));
            $('#nav li a').each(function () {
                if (urlRegExp.test(this.href)) {
                    $(this).addClass('active');
                }
            });
        });
    </script>



</head>

<body class="body">
<header class="navbar-fixed-top">
    <div class="img-logo"><a title="Page Index" href="" />GOOGLE MAP</a></div>
    <div class="navbar-right" id="navbar-right">
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">

                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-user"></i>
                        {if $smarty.session.Login neq ''}
                            {$smarty.session.Email}



                        {/if}
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu animated fadeInUp">
                        <li><a href="users.php?edit=profile"><i class="icon-ok"></i> Profile</a></li>
                        <li><a href="logout.php?logout=out"><i class="icon-off"></i> Logout</a></li>

                    </ul>
                </li>
            </ul>
        </div>
    </div>
</header>
