{include file="header.tpl"}
<link href="templates/css/boostrapTable.css" rel="stylesheet">
<link rel="stylesheet" href="templates/css/colorbox.css"/>
<script src="templates/js/jquery.colorbox.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".addLocation").colorbox(
                {
                    width: "550px"
                });
        $(".updateLocation").colorbox({ width: "550px"});

    });
</script>
<script type="text/javascript" src="templates/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="templates/js/datatables.js"></script>
<script type="text/javascript">
    function ConfirmDel()
    {   kq =confirm("Are you sure to delete ?");
        return kq;
    }
</script>

<div class="content">
    {include file="menuLeft.tpl"}

    <div class="mainbar">
        <div class="col-md-12">
            <div class="round-list">
                <div class="title-table">
                    <i class="icon-list"></i>MANAGER CITY &nbsp&nbsp<a href="city.php?frmAdd=frmAdd" title="Add new" class="addLocation"> <i class="icon-plus-sign"></i> &nbsp&nbspAdd New</a>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"
                       id="example">
                    <thead>
                    <tr>
                        <th class="sorting" id="th-width">No</th>
                        <th class="sorting" id="th-widthx">Location Name</th>
                        <th class="sorting">Lat Code</th>
                        <th class="sorting">Long Code</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {assign var="stt" value="1"}
                    {foreach from=$listLocation item="i"}

                        <tr class="odd gradeX">
                            <td>{$stt++} </td>
                            <td>{$i.name} </td>
                            <td>{$i.lat_code} </td>
                            <td>{$i.long_code} </td>
                            <td class="text-center">
                                {*<a href="city.php?del=del&&loId={$i.id_city}" title="Delete" onClick="return ConfirmDel();"> *}
                                    {*<i class="icon-trash"></i></a>&nbsp&nbsp*}

                                <a href="city.php?editLo=edit&&loId={$i.id_city}" name="upLocation" class="updateLocation" title="Edit"><i class="icon-pencil"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>