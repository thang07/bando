{include file="header.tpl"}
<link href="templates/css/boostrapTable.css" rel="stylesheet">
<link rel="stylesheet" href="templates/css/colorbox.css"/>
<script src="templates/js/jquery.colorbox.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".addMarker").colorbox({
            width: "550px"
        });
        $(".updateLocation").colorbox({ width: "550px"});
        $(".addParameter").colorbox({ width: "550px"});
        $(".addImg").colorbox({ width: "550px"});
    });
</script>
<script type="text/javascript" src="templates/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="templates/js/datatables.js"></script>
<script type="text/javascript">
    function ConfirmDel() {
        kq = confirm("Are you sure to delete ?");
        return kq;
    }
</script>

<div class="content">
    {include file="menuLeft.tpl"}

    <div class="mainbar">
        <div class="col-md-12">
            <div class="round-list">
                <div class="title-table">
                    <i class="icon-list"></i>MANAGER PARAMETER&nbsp&nbsp
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"
                       id="example">
                    <thead>
                    <tr>
                        <th class="sorting" id="th-width">No</th>
                        <th class="sorting" id="th-widthx">Id Marker</th>
                        <th class="sorting" id="th-widthx">name_street_horizontal </th>
                        <th class="sorting">name_street_vertical </th>
                        <th class="sorting">Speed</th>
                        <th class="sorting">action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {assign var="stt" value="1"}
                    {foreach from=$listParameter item="i"}
                        <tr class="odd gradeX">
                            <td>{$stt++} </td>

                            <td>{$i.id_marker}</td>
                            <td>{$i.name_street_horizontal}</td>
                            <td>{$i.name_street_vertical}</td>
                            <td>{$i.cycle}</td>

                            <td class="text-center">

                                {*<a href="parameter.php?editPara=edit&&idPa={$i.id_parameter}" name="upLocation"*}
                                   {*class="updateLocation" title="Edit marker"><i class="icon-pencil"></i></a>*}
                                {*|*}
                                <a target="_blank" href="parameter.php?viewPa=view&&idPara={$i.id_parameter}&&idMa={$i.id_marker}"">View</a>
                                |
                                <a href="img.php?addImg=Addimg&&idPara={$i.id_parameter}&&direc={$i.id_parameter}" name="addImg" class="addImg">Add img</a>

                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>