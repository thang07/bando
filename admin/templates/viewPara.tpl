{include file="header.tpl"}
<link href="templates/css/boostrapTable.css" rel="stylesheet">
<link rel="stylesheet" href="templates/css/colorbox.css"/>
<script src="templates/js/jquery.colorbox.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".addImgVerticalTop").colorbox({
            width: "500px"
        });
        $(".addImgVetBottom").colorbox({
            width: "500px"
        });
        $(".addImgHorizonRight").colorbox({
            width: "500px"
        });
        $(".addImgHorizonLeft").colorbox({
            width: "500px"
        });


    });
</script>
<style>
    .viewpara {
        margin: 0 auto;
        margin-top: 20px;
        background: #fff; font-family: arial, helvetica, sans-serif;
        border: 1px solid #cfcfcf; overflow: hidden;
    }

    .viewpara .info-street, .viewpara .siteNotice {
        float: left;
    }

    .update-form {
    }

    .col-md-12 .form-horizontal .control-label {
        margin-right: 5px;
    }
    .last{
        width: 100%;float: left;
    }
</style>


<div class="viewpara" style="width: 800px">

    <div class="info-street" id="info-street" style="width: 100%;height: 30px;float: left;text-align: center;">
        {foreach  from=$getNameMaker item="i"}
            {$i.marker_name}
        {/foreach} </div>
    <div id="siteNotice" class="siteNotice" style="width: 297px;float: left;margin-left: 20px;">
        {foreach from=$viewListParamater item="para"}
            {if $i.id == $para.id_marker}
                <table border="0px" style="width: 290px;">
                    <tr>
                        <td class="td-top-left"
                            style="height: 65px;width: 130px;text-align: right;vertical-align: top;  border-right: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-right-radius: 5px">
                            <img height="70px" width="85px" style="" src="../upload/{$para.img_top_vertical}"/>

                            <p>
                                <a class="addImgVerticalTop"
                                   href="parameter.php?addImgVetTop=true&id_para={$i.id}&idMa={$para.id_marker}"
                                   style="font-size: 10px">Edit</a>
                            </p>
                        </td>
                        <td style="width: 50px;text-align: left">
                            <div style="font-family:Arial;font-size:11px; "> {$para.name_street_vertical}</div>
                        </td>
                        <td class="td-top-right"
                            style="height: 95px;width: 130px;text-align: right;vertical-align: bottom;bottom: auto; border-left: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-left-radius:5px">
                            <p><a class="addImgHorizonRight"
                                  href="parameter.php?addImgHorizonRight=true&&id_para={$i.id}&idMa={$para.id_marker}"
                                  style="font-size: 10px">Edit</a></p>

                            <img height="65px" style="" width="85px" src="../upload/{$para.img_right_horizontal}"/>

                        </td>
                    </tr>
                    <tr>
                        <td class="td-center-left" style="height: 30px;text-align: center;vertical-align: bottom"><label
                                    style="font-size: 11px;">{$para.name_street_horizontal}</label></td>
                        <td style="height: 40px;width: 30px;text-align: center;vertical-align: middle">
                            {if $i.color == 'red'}
                                <img src="../red.png" height="30px" width="33px"/>
                            {else}
                                <img src="../green.png" height="29px" width="33px"/>
                            {/if}
                        </td>
                        <td class="td-center-right" style="text-align: center;height: 30px;vertical-align: top"><label
                                    style="font-size: 11px;">{$para.name_street_horizontal}</label></td>
                    </tr>
                    <tr>
                        <td class="td-bottom-left"
                            style="height: 95px;text-align: left;vertical-align: top;border-top: 2px solid #cfcfcf;border-right: 2px solid #cfcfcf;border-top-right-radius: 5px">
                            <img height="65px" width="85px" style="" src="../upload/{$para.img_left_horizontal}"/>

                            <p><a class="addImgHorizonLeft"
                                  href="parameter.php?addImgHorizonLeft=true&id_para={$i.id}&idMa={$para.id_marker}"
                                  style="font-size: 10px">Edit</a></p>

                        </td>

                        <td style="width: 50px;text-align: right">
                            <div style="font-size:11px;">{$para.name_street_vertical}</div>
                        </td>
                        <td class="td-bottom-right"
                            style="height: 95px;border-top: 2px solid #cfcfcf;border-left: 2px solid #cfcfcf;border-top-left-radius: 5px">
                            <p><a class="addImgVetBottom"
                                  href="parameter.php?addImgVetBottom=true&id_para={$i.id}&idMa={$para.id_marker}"
                                  style="font-size: 10px">Edit</a></p>

                            <img height="65px" width="85px" src="../upload/{$para.img_bottom_vertical}"/>

                        </td>
                    </tr>
                </table>
            {/if}
        {/foreach}
    </div>
    <div id="info" style="width: 320px;float: right;overflow: hidden">
        <table id="example" class="table-marker"
               style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">
            {foreach from=$viewListParamater item="pa"}
                <tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">
                    <th style="border: 1px solid #cfcfcf">Thông số</th>
                    <th style="border: 1px solid #cfcfcf">{$pa.name_street_horizontal}</th>
                    <th style="border: 1px solid #cfcfcf">{$pa.name_street_vertical}</th>
                </tr>
                <tbody>
                <tr>
                    <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">Chu Kỳ</td>
                    <td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">{$pa.cycle} giây</td>
                </tr>
                <tr style="background: #f6f6f6">
                    <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">Vận tốc</td>
                    <td style="border: 1px solid #cfcfcf;text-align: center">{$pa.speech_horizontal} km/h</td>
                    <td style="border: 1px solid #cfcfcf;text-align: center">{$pa.speech_vertical} km/h</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">Mật độ</td>
                    <td style="border: 1px solid #cfcfcf;text-align: center">{$pa.consistence_horizontal} m/2</td>
                    <td style="border: 1px solid #cfcfcf;text-align: center">{$pa.consistence_vertical} m/2</td>
                </tr>
                <tr style="background: #f6f6f6">
                    <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">T/g Xanh</td>
                    <td style="border: 1px solid #cfcfcf;text-align: center">{$pa.time_green_horizontal} giây</td>
                    <td style="border: 1px solid #cfcfcf;text-align: center">{$pa.time_green_vertical} giây</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">T/g Vàng</td>
                    <td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">{$pa.time_yellow} giây</td>
                </tr>
                </tbody>
            {/foreach}
        </table>
        <p></p>
            </br>
        <div class="update-form">
            <form action="parameter.php?upd=upPara" method="post">
                <table id="example" class="table-marker"
                       style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;"
                       cellspacing="0">
                    {foreach from=$viewListParamater item="pa"}
                        <tr>
                            <td colspan="2" style="text-align: center;font-weight:bold ">Điều Chỉnh Thông số</td>
                        </tr>
                        <tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">
                            <th style="border: 1px solid #cfcfcf">Thông số</th>
                            <th style="border: 1px solid #cfcfcf">{$pa.name_street_horizontal}</th>
                            <th style="border: 1px solid #cfcfcf">{$pa.name_street_vertical}</th>
                        </tr>
                        <tbody>
                        <tr>
                            <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">Chu Kỳ</td>
                            <td style="border: 1px solid #cfcfcf;text-align: center" colspan="2" class="numbers-row">
                                <input type="text" name="cycle" id="partridge"
                                       style="width: 30px;text-align:center;font-size:12px" value="{$pa.cycle}">giây
                            </td>
                        </tr>
                        <tr style="background: #f6f6f6">
                            <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px"> Vận tốc</td>
                            <td style="border: 1px solid #cfcfcf;text-align: center">
                                <input type="text" name="speed_horizontal"
                                       style="width: 30px;text-align:center;font-size:12px"
                                       value="{$pa.speech_horizontal}">
                                km/h
                            </td>
                            <td style="border: 1px solid #cfcfcf;text-align: center">
                                <input type="text"
                                       name="speed_vertical"
                                       style="width: 30px;text-align:center;font-size:12px"
                                       value="{$pa.speech_vertical}">km/h
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">Mật độ</td>
                            <td style="border: 1px solid #cfcfcf;text-align: center">
                                <input type="text"
                                       name="consistence_horizontal"
                                       id="consistence_horizontal"
                                       style="width: 30px;text-align:center;font-size:12px"
                                       value="{$pa.consistence_horizontal}">
                                m/2
                            </td>
                            <td style="border: 1px solid #cfcfcf;text-align: center">
                                <input type="text"
                                       name="consistence_vertical"
                                       style="width: 30px;text-align:center;font-size:12px"
                                       value="{$pa.consistence_vertical}">
                                m/2
                            </td>
                        </tr>
                        <tr style="background: #f6f6f6">
                            <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px"> T/g Xanh</td>
                            <td style="border: 1px solid #cfcfcf;text-align: center"><input type="text"
                                                                                            name="time_green_horizontal"
                                                                                            style="width: 30px;text-align:center;font-size:12px"
                                                                                            value="{$pa.time_green_horizontal}">
                                giây
                            </td>
                            <td style="border: 1px solid #cfcfcf;text-align: center"><input type="text"
                                                                                            name="time_green_vertical"
                                                                                            style="width: 30px;text-align:center;font-size:12px"
                                                                                            value="{$pa.time_green_vertical}">
                                giây
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #cfcfcf;text-align: center;height: 30px"> T/g Vàng</td>
                            <td style="border: 1px solid #cfcfcf;text-align: center" colspan="2"><input type="text"
                                                                                                        name="time_yellow"
                                                                                                        style="width: 30px;text-align:center;font-size:12px"
                                                                                                        value="{$pa.time_yellow}">
                                giây <input type="submit" value="Cập nhật"><input type="hidden" name="id_parama"
                                                                                  value="{$pa.id_parameter}">
                                <input type="hidden" name="id_marker"
                                       value="{$pa.id_marker}">
                            </td>

                        </tr>

                        </tbody>
                    {/foreach}
                </table>
            </form>
        </div>

    </div>
    <div class="last"><a href="http://bando.dev.com/admin/parameter.php?listParameter=listPara">Back to</a> </div>
</div>



