<script type="text/javascript" src="templates/js/jquery.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New Parameter</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="parameter.php?action=addPara" id="frmCreateParameter"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label">Street name</label>

                    <div class="controls">
                        <input class="input-xlarge" id="name_street" name="name_street" class="name_street" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Green time</label>

                    <div class="controls">
                        <input class="input-xlarge" id="time_green" name="time_green" class="time_green"
                               maxlength="200"
                               type="text"> Giây
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Cycle</label>
                    <input type="hidden" value="{$idMarker}" name="id_marker" id="id_marker"/>

                    <div class="controls">
                        <input class="input-xlarge" id="cycle" name="cycle" class="cycle" maxlength="200"
                               type="text"> Giây
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Speed </label>

                    <div class="controls">
                        <input class="input-xlarge" id="speed" name="speed" class="speed" maxlength="200"
                               type="text"> Km/h
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Consistence </label>

                    <div class="controls">
                        <input class="input-xlarge" id="consistence" name="consistence" class="consistence"
                               maxlength="200"
                               type="text"> m/2
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Yellow time </label>

                    <div class="controls">
                        <input class="input-xlarge" id="time_yellow" name="time_yellow" class="time_yellow"
                               maxlength="200"
                               type="text"> Giây
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Green time</label>

                    <div class="controls">
                        <input class="input-xlarge" id="time_green" name="time_green" class="time_green"
                               maxlength="200"
                               type="text"> Giây
                    </div>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div>