<!DOCTYPE html>
<html>
<head>
    <title>{$title}</title>
    <link href="templates/css/bootstrap.min.css" rel="stylesheet">
    <link href="templates/css/bootstrap.icon-large.min.css" rel="stylesheet" media="screen">
    <link href="templates/css/style.css" rel="stylesheet">
    <link href="templates/css/bootstrap.icon-large.min.css" rel="stylesheet" media="screen">
    <link href="templates/css/font-awesome.css" rel="stylesheet" media="screen">
</head>
<body class="body-login">
<div class="round-login">
    <div class="login-with400">
        <div class="title-login"><i class="icon-lock"></i> LOGIN FORM</div>
        <form class="form-horizontal" role="form" method="post" action="login.php">
            <div class="form-group">
                <label class="col-sm-2 control-label">Email or Phone</label>
                <input type="text" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <input type="password" class="form-control" name="password">
            </div>
            <div class="btn-login">
                <button type="submit" class="btn btn-primary" value="Login" name="btnlogin">Login</button>

            </div>
        </form>
    </div>
</div>
</body>
</html>