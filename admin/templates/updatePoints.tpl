<script type="text/javascript" src="templates/js/jquery.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Edit Points</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="points.php?actionUpdate=Points" id="frmCreateLocation"
                  name="frmCreateVehicle">
                {foreach from=$points item="point"}
                    <div class="control-group">
                        <label class="control-label">Street Name</label>

                        <div class="controls">
                            <input class="input-xlarge" id="name_street" value="{$point.name_street}" name="name_street"
                                   class="name_street" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Address (start)</label>

                        <div class="controls">
                            <input class="input-xlarge" id="address_start" value="{$point.address_start}"
                                   name="address_start" class="address_start" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Address (end)</label>

                        <div class="controls">
                            <input class="input-xlarge" id="address_end" value="{$point.address_end}" name="address_end"
                                   class="address_end" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Lat code (start) </label>

                        <div class="controls">
                            <input class="input-xlarge" id="lat_start" value="{$point.lat_start}" name="lat_start"
                                   class="lat_start" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Long code (start) </label>

                        <div class="controls">
                            <input class="input-xlarge" id="long_start" value="{$point.long_start}" name="long_start"
                                   class="long_start" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Lat code (end) </label>

                        <div class="controls">
                            <input class="input-xlarge" id="lat_end" value="{$point.lat_end}" name="lat_end"
                                   class="lat_end" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Long code (end) </label>

                        <div class="controls">
                            <input class="input-xlarge" id="long_end" value="{$point.long_end}" name="long_end"
                                   class="long_end" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Color </label>

                        <div class="controls">
                            <select id="points-color" name="points-color">
                                {if $point.color eq 'red'}
                                    <option value="red" selected="selected">Red</option>
                                    <option value="green">Green</option>
                                    <option value="yellow">Yellow</option>
                                {elseif $point.color eq 'green'}
                                    <option value="green" selected="selected">Green</option>
                                    <option value="yellow">Yellow</option>
                                    <option value="red">Red</option>
                                {else}
                                    <option value="yellow" selected="selected">Yellow</option>
                                    <option value="green">Green</option>
                                    <option value="red">Red</option>
                                {/if}

                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" value="{$point.id}" name="id">
                        <button type="submit" class="btn btn-info pull-right" name="btnCreate">Update</button>
                    </div>
                {/foreach}
            </form>
        </div>
    </div>
</div>