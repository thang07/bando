<link rel="stylesheet" href="templates/css/colorbox.css"/>
<script src="templates/js/jquery.colorbox.js"></script>
<script>
    jQuery(document).ready(function ($) {
        editUser();
        PopupUser();

    });
    function PopupUser() {
        $(".createUser").colorbox(
                {
                    width: "500px"
                });

    }
    function editUser(){
        $(".editUser").colorbox({ width: "500px"});
    }

    function SearchClick() {
        var name = $('#search').val();
        $.ajax({
            url: 'users_action.php',
            type: 'get',
            data: 'type=search&name=' + name ,
            success: function (response) {
                $('table#example tbody').html(response);
            }
        });
    }
    function delUser(id) {
        var answer = confirm("Are you sure to delete ?")
        if (answer) {
            $.ajax
            ({
                //url: "park.php?action=delete&id=" + id,
                url: "users.php?delUser=delUser&id="+ id,
                success: function () {
                    location.reload();
                }
            });
        }
    }
</script>
<!--End popup trang-->

<!--Begin script xoa user tung id-->
{*<script type="text/javascript">*}
{*jQuery(document).ready(function ($) {*}
{*$(".delbutton").click(function () {*}
{*//Save the link in a variable called element*}
{*var element = $(this);*}
{*//Find the id of the link that was clicked*}
{*var del_id = element.attr("id");*}
{*//Built a url to send*}
{*var info = 'id=' + del_id;*}
{*if (confirm("Are you sure you want to delete this Record?")) {*}
{*$.ajax({*}
{*type: "GET",*}
{*url: "users.php?delUser=delUser",*}
{*data: info,*}
{*success: function () {*}
{*}*}
{*});*}
{*$(this).parents("#record").animate({ backgroundColor: "#52B9E9", border: "1px solid red" }, "fast")*}
{*.animate({ opacity: "hide" }, "slow");*}
{*}*}
{*return false;*}
{*});*}
{*});*}
{*</script>*}
<!--End script xoa user-->

{*<script src="templates/js/jquery-1.10.2.min.js" type="text/javascript" language="javascript"></script>*}
<!--Begin script phan trang-->
<script src="templates/js/jquery.dataTables.min.js" type="text/javascript" language="javascript"></script>
<script src="templates/js/jquery-DT-pagination.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript">
    /* Table initialisation */
    $(document).ready(function () {
        $('#example').dataTable({
            "bSort": false,      // Disable sorting
            "iDisplayLength": 15,   //records per page
            "sDom": "t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap"
        });
    });
</script>
<!--End  script phan trang-->

<div class="col-md-12">
    <div class="widget wred">
        <div class="widget-header">
            <div class="pull-left"><i class="icon-user"></i> USER MANAGEMENT</div>
            <div class="widget-icons pull-right">
                <a class="wminimize" href="#"><i class="icon-chevron-up"></i></a>
                <a class="wclose" href="#"><i class="icon-remove"></i></a>
            </div>
            <div class="clearfix"> &nbsp&nbsp<a href="users.php?frmAdd=AddNew" class="createUser" title="Add new">
                    <i class="icon-plus-sign"></i>&nbsp&nbspAdd new</a></div>
        </div>
        <div class="seacrh-user"><input name="search" class="input-xxlarge" id="search" placeholder="email, name, phone"/>
            &nbsp<input value="Search" type="button" class="btn-search" onclick="SearchClick()"/></div>
        <div class="widget-content">
            <table class="table table-bordered" id="example">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Role</th>
                    <th>Address</th>

                    <th class="title-action">Action</th>
                </tr>
                </thead>
                <tbody>
                {assign var="stt" value="1"}
                {foreach from=$listUser item="i"}
                    <tr class="record" id="record">
                        <td>{$stt++}</td>

                        <td>{$i.names}</td>
                        <td>{$i.email}</td>
                        <td>{$i.phone}</td>
                        <td>{$i.role}</td>
                        <td>{$i.address}</td>

                        <td class="td-action">

                                <a href="users.php?display=frmEdit&&id={$i.id}" title="Edit" class="editUser"><i class="icon-pencil"></i></a>

                                <a href="#" title="Delete" id="{$i.id}" onclick="delUser('{$i.id}')" class="delbutton">
                                    <i class="icon-trash"></i></a>


                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</div>

