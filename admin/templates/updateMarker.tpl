<script type="text/javascript" src="templates/js/jquery.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>UpdateMarker</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="marker.php?actUpdate=updateMarker" id="frmCreateLocation"
                  name="frmCreateVehicle">
                {foreach from=$editMarkers item ="m"}
                    <div class="control-group">
                        <label class="control-label">Name </label>
                        <input type="hidden" name="id_marker" value="{$m.id}"/>

                        <div class="controls">
                            <input class="input-xlarge" id="name_marker" name="name_marker" class="name_marker"
                                   value="{$m.marker_name}"
                                   maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Lat code </label>

                        <div class="controls">
                            <input class="input-xlarge" id="lat" name="lat" class="lat" maxlength="200"
                                   value="{$m.lat_marker}"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Long code </label>

                        <div class="controls">
                            <input class="input-xlarge" id="long" name="long" class="long" maxlength="200"
                                   value="{$m.long_marker}"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">City </label>

                        <div class="controls">
                            <select id="city" name="city">
                                {foreach from=$listCity item ="i"}
                                    {if $i.id_city eq $m.id_city}
                                        <option value="{$i.id_city}" selected="selected">{$i.name}</option>
                                    {else}
                                        <option value="{$i.id_city}">{$i.name}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Color </label>

                        <div class="controls">
                            <select id="points-color" name="points-color">
                                {if $m.color eq 'red'}
                                    <option value="red" selected="selected">Red</option>
                                    <option value="green">Green</option>
                                    <option value="yellow">Yellow</option>
                                {elseif $m.color eq 'green'}
                                    <option value="red">Red</option>
                                    <option value="green" selected="selected">Green</option>
                                    <option value="yellow">Yellow</option>
                                {else}
                                    <option value="red">Red</option>
                                    <option value="green">Green</option>
                                    <option value="yellow" selected="selected">Yellow</option>
                                {/if}

                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-info pull-right" name="btnCreate">Update</button>
                    </div>
                {/foreach}
            </form>
        </div>
    </div>
</div>