<script type="text/javascript" src="templates/js/jquery.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New City</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="city.php?action=add" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label">Location Name </label>
                    <div class="controls">
                        <input class="input-xlarge" id="flag" name="name" class="name" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Lat code </label>
                    <div class="controls">
                        <input class="input-xlarge" id="flag" name="lat" class="lat" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Long code </label>
                    <div class="controls">
                        <input class="input-xlarge" id="flag" name="long" class="long" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div>