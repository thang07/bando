{include file="header.tpl"}
<link href="templates/css/boostrapTable.css" rel="stylesheet">
<link rel="stylesheet" href="templates/css/colorbox.css"/>
<script src="templates/js/jquery.colorbox.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".addPoints").colorbox(
                {
                    width: "550px"
                });
        $(".updatePoints").colorbox({ width: "550px"});

    });
</script>
<script type="text/javascript" src="templates/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="templates/js/datatables.js"></script>
<script type="text/javascript">
    function ConfirmDel()
    {   kq =confirm("Are you sure to delete ?");
        return kq;
    }
</script>

<div class="content">
    {include file="menuLeft.tpl"}

    <div class="mainbar">
        <div class="col-md-12">
            <div class="round-list">
                <div class="title-table">
                    <i class="icon-list"></i>MANAGER POINTS &nbsp&nbsp<a href="points.php?frmAdd=frmAdd" title="Add new" class="addPoints"> <i class="icon-plus-sign"></i> &nbsp&nbspAdd New</a>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"
                       id="example">
                    <thead>
                    <tr>
                        <th class="sorting" id="th-width">No</th>
                        <th class="sorting" id="th-widthx">Street Name</th>
                        <th class="sorting" id="th-widthx">Address (start)</th>
                        <th class="sorting" id="th-widthx">Address (end)</th>
                        <th class="sorting">Lat Start</th>
                        <th class="sorting">Long Start</th>
                        <th class="sorting">Lat End</th>
                        <th class="sorting">Long End</th>
                        <th class="sorting">Color</th>
                        <th class="sorting">Datetime</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {assign var="stt" value="1"}
                    {foreach from=$listPoints item="i"}

                        <tr class="odd gradeX">
                            <td>{$stt++} </td>
                            <td>{$i.name_street} </td>
                            <td>{$i.address_start} </td>
                            <td>{$i.address_end} </td>
                            <td>{$i.lat_start} </td>
                            <td>{$i.long_start} </td>
                            <td>{$i.lat_end} </td>
                            <td>{$i.long_end} </td>
                            <td>{$i.color} </td>
                            <td>{$i.datime} </td>
                            <td class="text-center">
                                <a href="points.php?actionDel=del&&Id={$i.id}" title="Delete" onClick="return ConfirmDel();">
                                <i class="icon-trash"></i></a>&nbsp&nbsp
                                <a href="points.php?editPoints=edit&&Id={$i.id}" name="upLocation" class="updatePoints" title="Edit"><i class="icon-pencil"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>