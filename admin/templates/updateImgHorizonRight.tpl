<!DOCTYPE html>
<html>
<head>
    <title>{$title}</title>
    <meta charset="utf-8">
</head>
<body>
<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add img</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="parameter.php?updateHoRight=updateHorRight" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label">Img Hori Right</label>
                    <input value="{$id_parameter}" type="hidden" name="id_para"/>
                    <input value="{$id_Ma}" type="hidden" name="id_Ma"/>
                    <input class="input-xlarge" id="" name="file_img_Hor_Right" class="imgVertical" maxlength="200"
                           type="file">
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>