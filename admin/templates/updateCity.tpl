<script type="text/javascript" src="templates/js/jquery.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Update Location</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="city.php?act=updateCity"
                  id="frmUpdateLocation"
                  name="frmUpdateLocation">
                {foreach from=$getCities item="i"}

                <input name="location_id" id="location_id" value="{$i.id_city}" type="hidden">

                    <div class="control-group">
                        <label class="control-label">City Name </label>
                        <div class="controls">
                            <input type="text" value="{$i.name}" id="name" name="name"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Lat Code </label>
                        <div class="controls">
                            <input class="input-xlarge" id="lat_code" value="{$i.lat_code}" name="lat_code" placeholder=""
                                   >
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Long Code</label>
                        <div class="controls">
                            <input class="input-xlarge" id="long_code" value="{$i.long_code}" name="long_code" placeholder=""
                                   >
                        </div>
                    </div>

                {/foreach}
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="act_update_location">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>