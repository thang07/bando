<script type="text/javascript" src="templates/js/jquery.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New Marker</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="marker.php?add=addMarker" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label">Name </label>

                    <div class="controls">
                        <input class="input-xlarge" id="name_marker" name="name_marker" class="name_marker"
                               maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Lat code </label>

                    <div class="controls">
                        <input class="input-xlarge" id="lat" name="lat" class="lat" maxlength="200"
                               type="text">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Long code </label>

                    <div class="controls">
                        <input class="input-xlarge" id="long" name="long" class="long" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">City </label>

                    <div class="controls">
                        <select id="city" name="city">
                            {foreach from=$listCity item ="i"}
                                <option value="{$i.id_city}">{$i.name}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Color </label>

                    <div class="controls">
                        <select id="points-color" name="points-color">
                            <option value="red">Red</option>
                            <option value="green">Green</option>
                            <option value="yellow">Yellow</option>
                        </select>
                    </div>
                </div>



                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div>