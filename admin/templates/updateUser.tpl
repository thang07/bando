<script type="text/javascript" src="templates/js/jquery.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#frmUpdateUser').validate(
                {
                    rules: {
                        name: {
                            minlength: 2,
                            required: true
                        },
                        password: {
                            minlength: 6,
                            required: true
                        },
                        repassword: {
                            required: true,
                            equalTo: "#password"
                        },
                        email: {
                            required: true,
                            email: true
                        }
                    },
                    messages: {
                        name: "(*)",
                        password: "(*)",
                        email: "(*)",
                        repassword: "(*)"
                    }

                });
    });
</script>
<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Update Account</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" method="post" action="users.php?act=upUser" id="frmUpdateUser"
                  name="frmCreateUser">
                {foreach from=$getUerId item="i"}
                    <input type="hidden" value="{$i.id}" name="idUp"/>
                    <div class="control-group">
                        <label class="control-label" for="username"> Name *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="name" value="{$i.names}" name="lname"
                                   placeholder="Enter Name" maxlength="199">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email">Email Address *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="email" value="{$i.email}" name="email"
                                   class="required email form-control"
                                   type="email" placeholder="Enter Email" maxlength="200">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Phone Number </label>

                        <div class="controls">
                            <input class="input-xlarge" value="{$i.phone}" name="phone" type="text"
                                   placeholder="Enter Phone Number" maxlength="20">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">User Role </label>

                        <div class="controls">
                            <select name="selTypeUser">
                                {if $i.role eq 'admin'}
                                    <option value="{$i.role}" selected="selected">Admin</option>
                                    <option value="user">User</option>
                                    <option value="mod">Mod</option>
                                {elseif $i.role eq 'mod'}
                                    <option value="{$i.role}" selected="selected">Mod</option>
                                    <option value="user">User</option>
                                    <option value="admin">Admin</option>
                                {else}
                                    <option value="{$i.role}" selected="selected">User</option>
                                    <option value="admin">Admin</option>
                                    <option value="mod">Mod</option>
                                {/if}
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Address</label>

                        <div class="controls">
                            <textarea class="input-xlarge" name="address" placeholder="Enter Address"
                                      rows="2" maxlength="200">{$i.address}</textarea>
                        </div>
                    </div>
                {/foreach}
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnUpdate">Update Account</button>
                </div>
            </form>
        </div>
    </div>
</div>
