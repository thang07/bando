<script type="text/javascript" src="templates/js/jquery.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New Image for Marker</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="img.php?action=add" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label"> </label>
                    <div class="controls">
                        <input class="input-xlarge readonly" id="id_idPara" value="{$idPara}" name="id_idPara" class="id_marker" maxlength="200"
                               type="hidden" readonly="true">
                        <input class="direc" name="direc" value="{$direc}" type="hidden" readonly="true"/>
                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label">Images left </label>
                    <div class="controls">
                        <input class="input-xlarge" id="file_name" name="file_name_left" class="file_name_left" maxlength="200"
                               type="file">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Images rigth </label>
                    <div class="controls">
                        <input class="input-xlarge" id="file_name" name="file_name_right" class="file_name_right" maxlength="200"
                               type="file">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Images top </label>
                    <div class="controls">
                        <input class="input-xlarge" id="file_name" name="file_name_top" class="file_name_top" maxlength="200"
                               type="file">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Images bottom </label>
                    <div class="controls">
                        <input class="input-xlarge" id="file_name" name="file_name_bottom" class="file_name_bottom" maxlength="200"
                               type="file">
                    </div>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div>