{include file="header.tpl"}
<link href="templates/css/boostrapTable.css" rel="stylesheet">
<link rel="stylesheet" href="templates/css/colorbox.css"/>
<script src="templates/js/jquery.colorbox.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".addMarker").colorbox({
                    width: "550px"
                });
        $(".updateLocation").colorbox({ width: "550px"});
        $(".addParameter").colorbox({ width: "1100px"});
        $(".addImg").colorbox({ width: "550px"});
    });
</script>
<script type="text/javascript" src="templates/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="templates/js/datatables.js"></script>
<script type="text/javascript">
    function ConfirmDel() {
        kq = confirm("Are you sure to delete ?");
        return kq;
    }
</script>

<div class="content">
    {include file="menuLeft.tpl"}

    <div class="mainbar">
        <div class="col-md-12">
            <div class="round-list">
                <div class="title-table">
                    <i class="icon-list"></i>MANAGER MARKER&nbsp&nbsp<a href="marker.php?frmAdd=frmAddMarker" title="Add new"
                                                                        class="addMarker"> <i
                                class="icon-plus-sign"></i> &nbsp&nbspAdd New</a>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"
                       id="example">
                    <thead>
                    <tr>
                        <th class="sorting" id="th-width">Id</th>
                        <th class="sorting" id="th-widthx">Name</th>
                        <th class="sorting" id="th-widthx">City</th>
                        <th class="sorting">Lat</th>
                        <th class="sorting">Long</th>
                        <th class="sorting">Color</th>
                        <th class="text-center">Action | Add para | Add img</th>
                    </tr>
                    </thead>
                    <tbody>
                    {assign var="stt" value="1"}
                    {foreach from=$listAllMarker item="i"}
                        <tr class="odd gradeX">
                            {*<td>{$stt++} </td>*}
                            <td>{$i.id} </td>
                            <td>{$i.marker_name} </td>
                            <td>{$i.city_name} </td>
                            <td>{$i.lat_marker} </td>
                            <td>{$i.long_marker} </td>
                            <td style="color: {$i.color}">{$i.color} </td>
                            <td class="text-center">

                                <a href="marker.php?editMark=edit&&idMarker={$i.id}" name="upLocation"
                                   class="updateLocation" title="Edit marker"><i class="icon-pencil"></i></a>
                                |
                                <a href="parameter.php?addPara=AddPara&&idMarker={$i.id}" title="Add Parameter" name="addParameter" class="addParameter">
                                    <i class="icon-plus-sign"></i>
                                </a>
                                {*|*}
                                {*<a href="img.php?addImg=Addimg&&idMarker={$i.id}" name="addImg" class="addImg">Add img</a>*}


                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>