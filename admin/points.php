<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 1/30/15
 * Time: 3:45 PM
 */
require 'include/smarty.php';
require 'include/points_function.php';



session_start();

//check login of user
if (!isset($_SESSION['Login'])) {
    header("location:login.php");
}
//display list points
else if(isset($_GET['listPoint'])=="listPoints"){
    $listPoints=getListAllPoints();
    $smarty->assign("listPoints", $listPoints);
    $smarty->assign("title", "List Options");
    $smarty->assign("url", "http://" . $_SERVER['HTTP_HOST']);
    $smarty->display("listPoints.tpl");
}
//check to display form add points
else if(isset($_REQUEST['frmAdd']) == "frmAdd"){
        $smarty->assign('title', 'Add Points');
        $smarty->display('addPoints.tpl');
}
//insert new records into points table
else if(isset($_REQUEST['add']) == "addPoints"){
    $name_street=$_POST['name_street'];
    $address_start=$_POST['address_start'];
    $address_end=$_POST['address_end'];
    $lat_start=$_POST['lat_start'];
    $long_start=$_POST['long_start'];
    $lat_end=$_POST['lat_end'];
    $long_end=$_POST['long_end'];
    $color=$_POST['points-color'];
    $values=array($name_street,$address_start,$address_end,$lat_start,$long_start,$lat_end,$long_end,$color);
    insertPoints($values);

    header("location:points.php?listPoint=listPoints");//respone list point

}
//display update form
else if(isset($_REQUEST['editPoints'])=="edit"){
    $idPoint=$_REQUEST['Id'];
    $point=getPointsById($idPoint);
    $smarty->assign("points",$point);
    $smarty->assign('title', 'Add Points');
    $smarty->display('updatePoints.tpl');
}
//
else if(isset($_REQUEST['actionUpdate'])=="Points"){
    $_name_street=$_POST['name_street'];
    $_address_start=$_POST['address_start'];
    $_address_end=$_POST['address_end'];
    $_lat_start=$_POST['lat_start'];
    $_long_start=$_POST['long_start'];
    $_lat_end=$_POST['lat_end'];
    $_long_end=$_POST['long_end'];
    $_color=$_POST['points-color'];
    $_id=$_POST['id'];
    $values=array($_name_street,$_address_start,$_address_end,$_lat_start,$_long_start,$_lat_end,$_long_end,$_color,$_id) ;
    $update=updatePoints($values);
    header("location:points.php?listPoint=listPoints");
}
else if(isset($_REQUEST['actionDel'])=="del"){
    $id=$_GET['Id'];
    delPoints($id);
    $smarty->assign('title', 'Add Points');
    header("location:points.php?listPoint=listPoints");
}