<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 2/2/15
 * Time: 2:24 PM
 */
require 'include/smarty.php';
require 'include/marker_function.php';
require 'include/city_function.php';

session_start();

if (!isset($_SESSION['Login'])) {
    header("location:login.php");
}
//display list points
else if(isset($_GET['listMarker'])=="listAllMarker"){
    $listAllMarker=getListAllMarker();
    $smarty->assign("listAllMarker", $listAllMarker);
    $smarty->assign("title", "List Options");
    $smarty->assign("url", "http://" . $_SERVER['HTTP_HOST']);
    $smarty->display("listMarker.tpl");
}
else if(isset($_GET['frmAdd'])=="frmAddMarker"){
    $listCity=getListCity();
    $smarty->assign('listCity', $listCity);
    $smarty->assign('title', 'Add Marker');
    $smarty->display('addMarker.tpl');

}
else if(isset($_GET['add'])=="addMarker"){
    $_name = $_POST['name_marker'];
    $_lat = $_POST['lat'];
    $_long = $_POST['long'];
    $color=$_POST['points-color'];
    $id_city=$_POST['city'];
    $values = array( $_lat, $_long,$_name,$color,$id_city);

    insertMarker($values);
    header("location:marker.php?listMarker=listAllMarker");
}
else if(isset($_GET['editMark'])=="edit"){

    $id_marker=$_GET['idMarker'];
    $editMarkers=getEditMarker($id_marker);
    $smarty->assign('editMarkers', $editMarkers);
    $listCity=getListCity();
    $smarty->assign('listCity', $listCity);
    $smarty->assign("title", "edit marker");
    $smarty->display("updateMarker.tpl");
}
else if($_REQUEST['actUpdate']=='updateMarker'){
    $id_marker=$_POST['id_marker'];
    $_name = $_POST['name_marker'];
    $_lat = $_POST['lat'];
    $_long = $_POST['long'];
    $color=$_POST['points-color'];
    $id_city=$_POST['city'];
    $values = array( $_lat, $_long,$_name,$color,$id_city,$id_marker);
    updateMarker($values);

    header("location:marker.php?listMarker=listAllMarker");
}