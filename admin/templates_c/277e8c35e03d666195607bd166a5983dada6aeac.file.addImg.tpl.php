<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-02-10 09:08:41
         compiled from ".\templates\addImg.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2016454d47cc06ddf09-86513882%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '277e8c35e03d666195607bd166a5983dada6aeac' => 
    array (
      0 => '.\\templates\\addImg.tpl',
      1 => 1423554955,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2016454d47cc06ddf09-86513882',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54d47cc07118d4_85456008',
  'variables' => 
  array (
    'idPara' => 0,
    'direc' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54d47cc07118d4_85456008')) {function content_54d47cc07118d4_85456008($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New Image for Marker</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="img.php?action=add" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label">Lat code </label>
                    <div class="controls">
                        <input class="input-xlarge readonly" id="id_idPara" value="<?php echo $_smarty_tpl->tpl_vars['idPara']->value;?>
" name="id_idPara" class="id_marker" maxlength="200"
                               type="text" readonly="true">
                        <input class="direc" name="direc" value="<?php echo $_smarty_tpl->tpl_vars['direc']->value;?>
" type="text" readonly="true"/>
                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label">Images left </label>
                    <div class="controls">
                        <input class="input-xlarge" id="file_name" name="file_name_left" class="file_name_left" maxlength="200"
                               type="file">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Images rigth </label>
                    <div class="controls">
                        <input class="input-xlarge" id="file_name" name="file_name_right" class="file_name_right" maxlength="200"
                               type="file">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Images top </label>
                    <div class="controls">
                        <input class="input-xlarge" id="file_name" name="file_name_top" class="file_name_top" maxlength="200"
                               type="file">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Images bottom </label>
                    <div class="controls">
                        <input class="input-xlarge" id="file_name" name="file_name_bottom" class="file_name_bottom" maxlength="200"
                               type="file">
                    </div>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div><?php }} ?>
