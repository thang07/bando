<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-21 08:45:57
         compiled from ".\templates\updateUser.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2408955adeaa539cc52-25263144%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '504bde249bf55082dae6e8992b4d96b6e28a0715' => 
    array (
      0 => '.\\templates\\updateUser.tpl',
      1 => 1422428386,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2408955adeaa539cc52-25263144',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'getUerId' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55adeaa54651b9_25033436',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55adeaa54651b9_25033436')) {function content_55adeaa54651b9_25033436($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#frmUpdateUser').validate(
                {
                    rules: {
                        name: {
                            minlength: 2,
                            required: true
                        },
                        password: {
                            minlength: 6,
                            required: true
                        },
                        repassword: {
                            required: true,
                            equalTo: "#password"
                        },
                        email: {
                            required: true,
                            email: true
                        }
                    },
                    messages: {
                        name: "(*)",
                        password: "(*)",
                        email: "(*)",
                        repassword: "(*)"
                    }

                });
    });
<?php echo '</script'; ?>
>
<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Update Account</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" method="post" action="users.php?act=upUser" id="frmUpdateUser"
                  name="frmCreateUser">
                <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['getUerId']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>
                    <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" name="idUp"/>
                    <div class="control-group">
                        <label class="control-label" for="username"> Name *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="name" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['names'];?>
" name="lname"
                                   placeholder="Enter Name" maxlength="199">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email">Email Address *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="email" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['email'];?>
" name="email"
                                   class="required email form-control"
                                   type="email" placeholder="Enter Email" maxlength="200">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Phone Number </label>

                        <div class="controls">
                            <input class="input-xlarge" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['phone'];?>
" name="phone" type="text"
                                   placeholder="Enter Phone Number" maxlength="20">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">User Role </label>

                        <div class="controls">
                            <select name="selTypeUser">
                                <?php if ($_smarty_tpl->tpl_vars['i']->value['role']=='admin') {?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value['role'];?>
" selected="selected">Admin</option>
                                    <option value="user">User</option>
                                    <option value="mod">Mod</option>
                                <?php } elseif ($_smarty_tpl->tpl_vars['i']->value['role']=='mod') {?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value['role'];?>
" selected="selected">Mod</option>
                                    <option value="user">User</option>
                                    <option value="admin">Admin</option>
                                <?php } else { ?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value['role'];?>
" selected="selected">User</option>
                                    <option value="admin">Admin</option>
                                    <option value="mod">Mod</option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Address</label>

                        <div class="controls">
                            <textarea class="input-xlarge" name="address" placeholder="Enter Address"
                                      rows="2" maxlength="200"><?php echo $_smarty_tpl->tpl_vars['i']->value['address'];?>
</textarea>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnUpdate">Update Account</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php }} ?>
