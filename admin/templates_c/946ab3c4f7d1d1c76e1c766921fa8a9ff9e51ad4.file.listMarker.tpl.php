<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-21 08:26:04
         compiled from ".\templates\listMarker.tpl" */ ?>
<?php /*%%SmartyHeaderCode:335755ade5fcb45db5-72877022%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '946ab3c4f7d1d1c76e1c766921fa8a9ff9e51ad4' => 
    array (
      0 => '.\\templates\\listMarker.tpl',
      1 => 1425609912,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '335755ade5fcb45db5-72877022',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'listAllMarker' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55ade5fcc15b19_89824288',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55ade5fcc15b19_89824288')) {function content_55ade5fcc15b19_89824288($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<link href="templates/css/boostrapTable.css" rel="stylesheet">
<link rel="stylesheet" href="templates/css/colorbox.css"/>
<?php echo '<script'; ?>
 src="templates/js/jquery.colorbox.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".addMarker").colorbox({
                    width: "550px"
                });
        $(".updateLocation").colorbox({ width: "550px"});
        $(".addParameter").colorbox({ width: "1100px"});
        $(".addImg").colorbox({ width: "550px"});
    });
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/datatables.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    function ConfirmDel() {
        kq = confirm("Are you sure to delete ?");
        return kq;
    }
<?php echo '</script'; ?>
>

<div class="content">
    <?php echo $_smarty_tpl->getSubTemplate ("menuLeft.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <div class="mainbar">
        <div class="col-md-12">
            <div class="round-list">
                <div class="title-table">
                    <i class="icon-list"></i>MANAGER MARKER&nbsp&nbsp<a href="marker.php?frmAdd=frmAddMarker" title="Add new"
                                                                        class="addMarker"> <i
                                class="icon-plus-sign"></i> &nbsp&nbspAdd New</a>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"
                       id="example">
                    <thead>
                    <tr>
                        <th class="sorting" id="th-width">Id</th>
                        <th class="sorting" id="th-widthx">Name</th>
                        <th class="sorting" id="th-widthx">City</th>
                        <th class="sorting">Lat</th>
                        <th class="sorting">Long</th>
                        <th class="sorting">Color</th>
                        <th class="text-center">Action | Add para | Add img</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->tpl_vars["stt"] = new Smarty_variable("1", null, 0);?>
                    <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listAllMarker']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>
                        <tr class="odd gradeX">
                            
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['marker_name'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['city_name'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['lat_marker'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['long_marker'];?>
 </td>
                            <td style="color: <?php echo $_smarty_tpl->tpl_vars['i']->value['color'];?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value['color'];?>
 </td>
                            <td class="text-center">

                                <a href="marker.php?editMark=edit&&idMarker=<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" name="upLocation"
                                   class="updateLocation" title="Edit marker"><i class="icon-pencil"></i></a>
                                |
                                <a href="parameter.php?addPara=AddPara&&idMarker=<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" title="Add Parameter" name="addParameter" class="addParameter">
                                    <i class="icon-plus-sign"></i>
                                </a>
                                
                                


                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><?php }} ?>
