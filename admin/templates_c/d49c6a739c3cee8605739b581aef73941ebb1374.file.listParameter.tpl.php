<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-03-06 03:54:43
         compiled from ".\templates\listParameter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:285554d862387784c2-78327311%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd49c6a739c3cee8605739b581aef73941ebb1374' => 
    array (
      0 => '.\\templates\\listParameter.tpl',
      1 => 1425610478,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '285554d862387784c2-78327311',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54d86238bbbf29_02048592',
  'variables' => 
  array (
    'listParameter' => 0,
    'stt' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54d86238bbbf29_02048592')) {function content_54d86238bbbf29_02048592($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<link href="templates/css/boostrapTable.css" rel="stylesheet">
<link rel="stylesheet" href="templates/css/colorbox.css"/>
<?php echo '<script'; ?>
 src="templates/js/jquery.colorbox.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".addMarker").colorbox({
            width: "550px"
        });
        $(".updateLocation").colorbox({ width: "550px"});
        $(".addParameter").colorbox({ width: "550px"});
        $(".addImg").colorbox({ width: "550px"});
    });
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/datatables.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    function ConfirmDel() {
        kq = confirm("Are you sure to delete ?");
        return kq;
    }
<?php echo '</script'; ?>
>

<div class="content">
    <?php echo $_smarty_tpl->getSubTemplate ("menuLeft.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <div class="mainbar">
        <div class="col-md-12">
            <div class="round-list">
                <div class="title-table">
                    <i class="icon-list"></i>MANAGER PARAMETER&nbsp&nbsp
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"
                       id="example">
                    <thead>
                    <tr>
                        <th class="sorting" id="th-width">No</th>
                        <th class="sorting" id="th-widthx">Id Marker</th>
                        <th class="sorting" id="th-widthx">name_street_horizontal </th>
                        <th class="sorting">name_street_vertical </th>
                        <th class="sorting">Speed</th>
                        <th class="sorting">action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->tpl_vars["stt"] = new Smarty_variable("1", null, 0);?>
                    <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listParameter']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>
                        <tr class="odd gradeX">
                            <td><?php echo $_smarty_tpl->tpl_vars['stt']->value++;?>
 </td>

                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['id_marker'];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['name_street_horizontal'];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['name_street_vertical'];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['cycle'];?>
</td>

                            <td class="text-center">

                                
                                   
                                
                                <a target="_blank" href="parameter.php?viewPa=view&&idPara=<?php echo $_smarty_tpl->tpl_vars['i']->value['id_parameter'];?>
&&idMa=<?php echo $_smarty_tpl->tpl_vars['i']->value['id_marker'];?>
"">View</a>
                                |
                                <a href="img.php?addImg=Addimg&&idPara=<?php echo $_smarty_tpl->tpl_vars['i']->value['id_parameter'];?>
&&direc=<?php echo $_smarty_tpl->tpl_vars['i']->value['id_parameter'];?>
" name="addImg" class="addImg">Add img</a>

                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><?php }} ?>
