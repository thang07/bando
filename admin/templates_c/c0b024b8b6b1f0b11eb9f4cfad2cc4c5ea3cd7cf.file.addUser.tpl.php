<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-21 08:15:01
         compiled from ".\templates\addUser.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1655ade3651631f6-38583010%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c0b024b8b6b1f0b11eb9f4cfad2cc4c5ea3cd7cf' => 
    array (
      0 => '.\\templates\\addUser.tpl',
      1 => 1422426349,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1655ade3651631f6-38583010',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55ade365245f09_19029642',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55ade365245f09_19029642')) {function content_55ade365245f09_19029642($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#frmCreateUser').validate(
                {
                    rules: {

                        lname: {
                            minlength: 2,
                            required: true
                        },
                        password: {
                            required: true
                        },
                        repassword: {
                            required: true,
                            equalTo: "#password"
                        },
                        email: {
                            required: true,
                            email: true
                        }
                    },
                    messages: {

                        lname: "(*)",
                        password:"(*)",
                        email:"(*)",
                        repassword:"(*)"
                }

                });
    });

<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    function ajax(str) {
        var xmlhttp;

        if(str.length==0)
        {
            document.getElementById("alert").innerHTML="";
            return;
        }
        try {
            xmlhttp=new XMLHttpRequest();
        }
        catch (e) {
            try {
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    alert("Your browser does not support AJAX!");
                    return false;
                }
            }
        }
        xmlhttp.onreadystatechange=function() {
            if(xmlhttp.readyState==4 && xmlhttp.status==200) {

                document.getElementById("alert").innerHTML=xmlhttp.responseText;
            }
        }
        var url='users.php?email='+str+'';
        xmlhttp.open("GET",url,true);
        xmlhttp.send();
    }
    function checkPhone(phone) {
        var xmlhttp;

        if(phone.length==0)
        {
            document.getElementById("alert").innerHTML="";
            return;
        }
        try {
            xmlhttp=new XMLHttpRequest();
        }
        catch (e) {
            try {
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    alert("Your browser does not support AJAX!");
                    return false;
                }
            }
        }
        xmlhttp.onreadystatechange=function() {
            if(xmlhttp.readyState==4 && xmlhttp.status==200) {

                document.getElementById("phone").innerHTML=xmlhttp.responseText;
            }
        }
        var url='users.php?phone='+phone+'';
        xmlhttp.open("GET",url,true);
        xmlhttp.send();
    }
<?php echo '</script'; ?>
>
<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Create Account</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" method="post" action="users.php?action=addUser" id="frmCreateUser"
                  name="frmCreateUser">

                <div class="control-group">
                    <label class="control-label">Name *</label>
                    <div class="controls">
                        <input class="input-xlarge required" id="lname" name="lname" placeholder="Enter Name" maxlength="199">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Email Address *</label>
                    <div class="controls">
                        <input class="input-xlarge" id="email" name="email" class="required email form-control"
                                maxlength="199" placeholder="Enter Email" onkeyup="ajax(this.value);">
                        <label id="alert" style="color: red;font-weight: normal;font-size: 13px"></label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Password *</label>
                    <div class="controls">
                        <input class="input-xlarge" id="password" name="password" type="password"
                               placeholder="Enter Password" maxlength="200"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Repeat Password *</label>
                    <div class="controls">
                        <input class="input-xlarge" name="repassword" type="password"
                               placeholder="Enter Repeat Password"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Phone Number </label>
                    <div class="controls">
                        <input class="input-xlarge" name="phone" type="text" onkeyup="checkPhone(this.value);" placeholder="Enter Phone Number" maxlength="20"/>
                        <label id="phone" style="color: red;font-weight: normal;font-size: 13px"></label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">User Type </label>
                    <div class="controls">
                        <select name="selTypeUser">
                            <option value="user">User</option>
                            <option value="mod">Mod</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Address</label>
                    <div class="controls">
                        <textarea class="input-xlarge" name="address" placeholder="Enter Address" rows="2" maxlength="200"></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Create Account</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php }} ?>
