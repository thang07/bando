<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-02-02 09:37:16
         compiled from ".\templates\addMarker.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1604454cf2c8cb22c95-88209198%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8b2fb419bd2a1f135795d587584851900e6ce542' => 
    array (
      0 => '.\\templates\\addMarker.tpl',
      1 => 1422866229,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1604454cf2c8cb22c95-88209198',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54cf2c8cb52686_76500012',
  'variables' => 
  array (
    'listCity' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54cf2c8cb52686_76500012')) {function content_54cf2c8cb52686_76500012($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New Marker</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="marker.php?add=addMarker" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label">Name </label>

                    <div class="controls">
                        <input class="input-xlarge" id="name_marker" name="name_marker" class="name_marker"
                               maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Lat code </label>

                    <div class="controls">
                        <input class="input-xlarge" id="lat" name="lat" class="lat" maxlength="200"
                               type="text">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Long code </label>

                    <div class="controls">
                        <input class="input-xlarge" id="long" name="long" class="long" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">City </label>

                    <div class="controls">
                        <select id="city" name="city">
                            <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listCity']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value['id_city'];?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Color </label>

                    <div class="controls">
                        <select id="points-color" name="points-color">
                            <option value="red">Red</option>
                            <option value="green">Green</option>
                            <option value="yellow">Yellow</option>
                        </select>
                    </div>
                </div>



                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div><?php }} ?>
