<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-03-02 03:37:33
         compiled from ".\templates\addParameter_Street.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1262254d9cfaa8baf24-98910878%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0576bf3a6593d6a10ca36164ec83e1cdfd356bc6' => 
    array (
      0 => '.\\templates\\addParameter_Street.tpl',
      1 => 1423642403,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1262254d9cfaa8baf24-98910878',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54d9cfaa907763_32578792',
  'variables' => 
  array (
    'idMarker' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54d9cfaa907763_32578792')) {function content_54d9cfaa907763_32578792($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New Parameter</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="parameter.php?action=addPara" id="frmCreateParameter"
                  name="frmCreateVehicle">
                <div class="column-left">
                    <div class="control-group">
                        <label class="control-label">Id redonly</label>
                        <div class="controls">
                            <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['idMarker']->value;?>
" name="id_marker" class="redonly" id="id_marker"
                                   readonly="true"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Street name ngang</label>
                        <div class="controls">
                            <input class="input-xlarge" id="name_street_horizontal" name="name_street_horizontal" class="name_street_horizontal"
                                   maxlength="200"
                                   type="text">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Speed ngang</label>

                        <div class="controls">
                            <input class="input-xlarge" id="speed_horizontal" name="speed_horizontal" class="speed_horizontal" maxlength="200"
                                   type="text"> Km/h
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Consistence horizontal</label>

                        <div class="controls">
                            <input class="input-xlarge" id="consistence_horizontal" name="consistence_horizontal" class="consistence_horizontal"
                                   maxlength="200"
                                   type="text"> m/2
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Green time ngang</label>
                        <div class="controls">
                            <input class="input-xlarge" id="time_green_horizontal" name="time_green_horizontal" class="time_green"
                                   maxlength="200"
                                   type="text"> Giây
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Images left(ngang) </label>
                        <div class="controls">
                            <input class="input-xlarge" id="file_name_left_ho" name="file_name_left_ho" class="file_name_left_ho" maxlength="200"
                                   type="file">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Images right(ngang) </label>
                        <div class="controls">
                            <input class="input-xlarge" id="file_name_right_ho" name="file_name_right_ho" class="file_name_right_ho" maxlength="200"
                                   type="file">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Cycle</label>
                        <div class="controls">
                            <input class="input-xlarge" id="cycle" name="cycle" class="cycle" maxlength="200"
                                   type="text"> Giây
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Yellow time </label>

                        <div class="controls">
                            <input class="input-xlarge" id="time_yellow" name="time_yellow" class="time_yellow"
                                   maxlength="200"
                                   type="text"> Giây
                        </div>
                    </div>
                </div>
                <div class="column-right">
                    <div class="control-group">
                        <label class="control-label"></label>

                        <div class="controls">
                            <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['idMarker']->value;?>
"  class="redonly"
                                   readonly="true"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Street name vertical</label>

                        <div class="controls">
                            <input class="input-xlarge" id="name_street_vertical" name="name_street_vertical" class="name_street_vertical"
                                   maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Speed vertical</label>

                        <div class="controls">
                            <input class="input-xlarge" id="speed" name="speed_vertical" class="speed_vertical" maxlength="200"
                                   type="text"> Km/h
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Consistence vertical</label>

                        <div class="controls">
                            <input class="input-xlarge" id="consistence" name="consistence_vertical" class="consistence"
                                   maxlength="200"
                                   type="text"> m/2
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Green time vertical</label>
                        <div class="controls">
                            <input class="input-xlarge" id="time_green" name="time_green_vertical" class="time_green"
                                   maxlength="200"
                                   type="text"> Giây
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Images top(vertical) </label>
                        <div class="controls">
                            <input class="input-xlarge" id="file_name" name="file_name_top_vertical" class="file_name_left" maxlength="200"
                                   type="file">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Images bottom(vertical) </label>
                        <div class="controls">
                            <input class="input-xlarge" id="file_name" name="file_name_bottom_vertical" class="file_name_left" maxlength="200"
                                   type="file">
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div><?php }} ?>
