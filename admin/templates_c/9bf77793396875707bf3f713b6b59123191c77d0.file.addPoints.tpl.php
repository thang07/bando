<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-02-06 04:30:05
         compiled from ".\templates\addPoints.tpl" */ ?>
<?php /*%%SmartyHeaderCode:469654cb4c3165c033-01328965%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9bf77793396875707bf3f713b6b59123191c77d0' => 
    array (
      0 => '.\\templates\\addPoints.tpl',
      1 => 1423129960,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '469654cb4c3165c033-01328965',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54cb4c31690481_06706841',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54cb4c31690481_06706841')) {function content_54cb4c31690481_06706841($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New Points</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="points.php?add=addPoints" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label">Street Name</label>
                    <div class="controls">
                        <input class="input-xlarge" id="name_street" name="name_street" class="name_street" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Address (start)</label>
                    <div class="controls">
                        <input class="input-xlarge" id="address_start" name="address_start" class="address_start" maxlength="200"
                               type="text">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Address (end)</label>
                    <div class="controls">
                        <input class="input-xlarge" id="address_end" name="address_end" class="address_end" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Lat code (start) </label>
                    <div class="controls">
                        <input class="input-xlarge" id="lat_start" name="lat_start" class="lat_start" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Long code (start) </label>
                    <div class="controls">
                        <input class="input-xlarge" id="long_start" name="long_start" class="long_start" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Lat code (end) </label>
                    <div class="controls">
                        <input class="input-xlarge" id="lat_end" name="lat_end" class="lat_end" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Long code (end) </label>
                    <div class="controls">
                        <input class="input-xlarge" id="long_end" name="long_end" class="long_end" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Color </label>
                    <div class="controls">
                        <select id="points-color" name="points-color">
                            <option value="red">Red</option>
                            <option value="green">Green</option>
                            <option value="yellow">Yellow</option>
                        </select>
                    </div>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div><?php }} ?>
