<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-21 08:26:06
         compiled from ".\templates\listPoints.tpl" */ ?>
<?php /*%%SmartyHeaderCode:705855ade5fe686901-09214805%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5b2ca027d39e5e38cec2879be5d51cb8c4192b09' => 
    array (
      0 => '.\\templates\\listPoints.tpl',
      1 => 1423211189,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '705855ade5fe686901-09214805',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'listPoints' => 0,
    'stt' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55ade5fe75b990_38240299',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55ade5fe75b990_38240299')) {function content_55ade5fe75b990_38240299($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<link href="templates/css/boostrapTable.css" rel="stylesheet">
<link rel="stylesheet" href="templates/css/colorbox.css"/>
<?php echo '<script'; ?>
 src="templates/js/jquery.colorbox.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".addPoints").colorbox(
                {
                    width: "550px"
                });
        $(".updatePoints").colorbox({ width: "550px"});

    });
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/datatables.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    function ConfirmDel()
    {   kq =confirm("Are you sure to delete ?");
        return kq;
    }
<?php echo '</script'; ?>
>

<div class="content">
    <?php echo $_smarty_tpl->getSubTemplate ("menuLeft.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <div class="mainbar">
        <div class="col-md-12">
            <div class="round-list">
                <div class="title-table">
                    <i class="icon-list"></i>MANAGER POINTS &nbsp&nbsp<a href="points.php?frmAdd=frmAdd" title="Add new" class="addPoints"> <i class="icon-plus-sign"></i> &nbsp&nbspAdd New</a>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"
                       id="example">
                    <thead>
                    <tr>
                        <th class="sorting" id="th-width">No</th>
                        <th class="sorting" id="th-widthx">Street Name</th>
                        <th class="sorting" id="th-widthx">Address (start)</th>
                        <th class="sorting" id="th-widthx">Address (end)</th>
                        <th class="sorting">Lat Start</th>
                        <th class="sorting">Long Start</th>
                        <th class="sorting">Lat End</th>
                        <th class="sorting">Long End</th>
                        <th class="sorting">Color</th>
                        <th class="sorting">Datetime</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->tpl_vars["stt"] = new Smarty_variable("1", null, 0);?>
                    <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listPoints']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>

                        <tr class="odd gradeX">
                            <td><?php echo $_smarty_tpl->tpl_vars['stt']->value++;?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['name_street'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['address_start'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['address_end'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['lat_start'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['long_start'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['lat_end'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['long_end'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['color'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['datime'];?>
 </td>
                            <td class="text-center">
                                <a href="points.php?actionDel=del&&Id=<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" title="Delete" onClick="return ConfirmDel();">
                                <i class="icon-trash"></i></a>&nbsp&nbsp
                                <a href="points.php?editPoints=edit&&Id=<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" name="upLocation" class="updatePoints" title="Edit"><i class="icon-pencil"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><?php }} ?>
