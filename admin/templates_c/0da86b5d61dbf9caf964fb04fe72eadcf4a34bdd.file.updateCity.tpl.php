<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-01-28 10:18:05
         compiled from ".\templates\updateCity.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2745554c8a378251413-08631238%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0da86b5d61dbf9caf964fb04fe72eadcf4a34bdd' => 
    array (
      0 => '.\\templates\\updateCity.tpl',
      1 => 1422436680,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2745554c8a378251413-08631238',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54c8a378301404_60934068',
  'variables' => 
  array (
    'getCities' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c8a378301404_60934068')) {function content_54c8a378301404_60934068($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Update Location</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="city.php?act=updateCity"
                  id="frmUpdateLocation"
                  name="frmUpdateLocation">
                <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['getCities']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>

                <input name="location_id" id="location_id" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['id_city'];?>
" type="hidden">

                    <div class="control-group">
                        <label class="control-label">City Name </label>
                        <div class="controls">
                            <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
" id="name" name="name"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Lat Code </label>
                        <div class="controls">
                            <input class="input-xlarge" id="lat_code" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['lat_code'];?>
" name="lat_code" placeholder=""
                                   >
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Long Code</label>
                        <div class="controls">
                            <input class="input-xlarge" id="long_code" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['long_code'];?>
" name="long_code" placeholder=""
                                   >
                        </div>
                    </div>

                <?php } ?>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="act_update_location">Update</button>
                </div>
            </form>
        </div>
    </div>
</div><?php }} ?>
