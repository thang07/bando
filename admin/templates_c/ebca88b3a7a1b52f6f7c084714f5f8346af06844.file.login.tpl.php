<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-21 08:13:36
         compiled from ".\templates\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2660355ade31009f1a7-00622514%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebca88b3a7a1b52f6f7c084714f5f8346af06844' => 
    array (
      0 => '.\\templates\\login.tpl',
      1 => 1422418612,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2660355ade31009f1a7-00622514',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55ade310435597_34732683',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55ade310435597_34732683')) {function content_55ade310435597_34732683($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
    <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
    <link href="templates/css/bootstrap.min.css" rel="stylesheet">
    <link href="templates/css/bootstrap.icon-large.min.css" rel="stylesheet" media="screen">
    <link href="templates/css/style.css" rel="stylesheet">
    <link href="templates/css/bootstrap.icon-large.min.css" rel="stylesheet" media="screen">
    <link href="templates/css/font-awesome.css" rel="stylesheet" media="screen">
</head>
<body class="body-login">
<div class="round-login">
    <div class="login-with400">
        <div class="title-login"><i class="icon-lock"></i> LOGIN FORM</div>
        <form class="form-horizontal" role="form" method="post" action="login.php">
            <div class="form-group">
                <label class="col-sm-2 control-label">Email or Phone</label>
                <input type="text" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <input type="password" class="form-control" name="password">
            </div>
            <div class="btn-login">
                <button type="submit" class="btn btn-primary" value="Login" name="btnlogin">Login</button>

            </div>
        </form>
    </div>
</div>
</body>
</html><?php }} ?>
