<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-01-28 08:45:41
         compiled from ".\templates\addCity.tpl" */ ?>
<?php /*%%SmartyHeaderCode:542854c891c06c9d89-19458702%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '147af4cefffbbccda6f319c52043481d15b94fd4' => 
    array (
      0 => '.\\templates\\addCity.tpl',
      1 => 1422430766,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '542854c891c06c9d89-19458702',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54c891c06fde45_81542679',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c891c06fde45_81542679')) {function content_54c891c06fde45_81542679($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New City</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="city.php?action=add" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label">Location Name </label>
                    <div class="controls">
                        <input class="input-xlarge" id="flag" name="name" class="name" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Lat code </label>
                    <div class="controls">
                        <input class="input-xlarge" id="flag" name="lat" class="lat" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Long code </label>
                    <div class="controls">
                        <input class="input-xlarge" id="flag" name="long" class="long" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div><?php }} ?>
