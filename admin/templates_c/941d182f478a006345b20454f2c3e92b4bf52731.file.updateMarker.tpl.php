<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-02-02 10:47:13
         compiled from ".\templates\updateMarker.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1414854cf45c46061d5-75371353%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '941d182f478a006345b20454f2c3e92b4bf52731' => 
    array (
      0 => '.\\templates\\updateMarker.tpl',
      1 => 1422870277,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1414854cf45c46061d5-75371353',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54cf45c47cbea0_41402616',
  'variables' => 
  array (
    'editMarkers' => 0,
    'm' => 0,
    'listCity' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54cf45c47cbea0_41402616')) {function content_54cf45c47cbea0_41402616($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>UpdateMarker</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="marker.php?actUpdate=updateMarker" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <?php  $_smarty_tpl->tpl_vars["m"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["m"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['editMarkers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["m"]->key => $_smarty_tpl->tpl_vars["m"]->value) {
$_smarty_tpl->tpl_vars["m"]->_loop = true;
?>
                    <div class="control-group">
                        <label class="control-label">Name </label>
                        <input type="hidden" name="id_marker" value="<?php echo $_smarty_tpl->tpl_vars['m']->value['id'];?>
"/>

                        <div class="controls">
                            <input class="input-xlarge" id="name_marker" name="name_marker" class="name_marker"
                                   value="<?php echo $_smarty_tpl->tpl_vars['m']->value['marker_name'];?>
"
                                   maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Lat code </label>

                        <div class="controls">
                            <input class="input-xlarge" id="lat" name="lat" class="lat" maxlength="200"
                                   value="<?php echo $_smarty_tpl->tpl_vars['m']->value['lat_marker'];?>
"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Long code </label>

                        <div class="controls">
                            <input class="input-xlarge" id="long" name="long" class="long" maxlength="200"
                                   value="<?php echo $_smarty_tpl->tpl_vars['m']->value['long_marker'];?>
"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">City </label>

                        <div class="controls">
                            <select id="city" name="city">
                                <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listCity']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>
                                    <?php if ($_smarty_tpl->tpl_vars['i']->value['id_city']==$_smarty_tpl->tpl_vars['m']->value['id_city']) {?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value['id_city'];?>
" selected="selected"><?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
</option>
                                    <?php } else { ?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value['id_city'];?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
</option>
                                    <?php }?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Color </label>

                        <div class="controls">
                            <select id="points-color" name="points-color">
                                <?php if ($_smarty_tpl->tpl_vars['m']->value['color']=='red') {?>
                                    <option value="red" selected="selected">Red</option>
                                    <option value="green">Green</option>
                                    <option value="yellow">Yellow</option>
                                <?php } elseif ($_smarty_tpl->tpl_vars['m']->value['color']=='green') {?>
                                    <option value="red">Red</option>
                                    <option value="green" selected="selected">Green</option>
                                    <option value="yellow">Yellow</option>
                                <?php } else { ?>
                                    <option value="red">Red</option>
                                    <option value="green">Green</option>
                                    <option value="yellow" selected="selected">Yellow</option>
                                <?php }?>

                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-info pull-right" name="btnCreate">Update</button>
                    </div>
                <?php } ?>
            </form>
        </div>
    </div>
</div><?php }} ?>
