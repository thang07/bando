<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-01-28 07:18:53
         compiled from ".\templates\listUser.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2883754c87e9dee4d05-77504642%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3d56c86ea14e0b32a9b53f4f6b560fe85f73f216' => 
    array (
      0 => '.\\templates\\listUser.tpl',
      1 => 1422425920,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2883754c87e9dee4d05-77504642',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54c87e9e1ce691_75334100',
  'variables' => 
  array (
    'listUser' => 0,
    'stt' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c87e9e1ce691_75334100')) {function content_54c87e9e1ce691_75334100($_smarty_tpl) {?><link rel="stylesheet" href="templates/css/colorbox.css"/>
<?php echo '<script'; ?>
 src="templates/js/jquery.colorbox.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    jQuery(document).ready(function ($) {
        editUser();
        PopupUser();

    });
    function PopupUser() {
        $(".createUser").colorbox(
                {
                    width: "500px"
                });

    }
    function editUser(){
        $(".editUser").colorbox({ width: "500px"});
    }

    function SearchClick() {
        var name = $('#search').val();
        $.ajax({
            url: 'users_action.php',
            type: 'get',
            data: 'type=search&name=' + name ,
            success: function (response) {
                $('table#example tbody').html(response);
            }
        });
    }
    function delUser(id) {
        var answer = confirm("Are you sure to delete ?")
        if (answer) {
            $.ajax
            ({
                //url: "park.php?action=delete&id=" + id,
                url: "users.php?delUser=delUser&id="+ id,
                success: function () {
                    location.reload();
                }
            });
        }
    }
<?php echo '</script'; ?>
>
<!--End popup trang-->

<!--Begin script xoa user tung id-->
























<!--End script xoa user-->


<!--Begin script phan trang-->
<?php echo '<script'; ?>
 src="templates/js/jquery.dataTables.min.js" type="text/javascript" language="javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="templates/js/jquery-DT-pagination.js" type="text/javascript" language="javascript"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
    /* Table initialisation */
    $(document).ready(function () {
        $('#example').dataTable({
            "bSort": false,      // Disable sorting
            "iDisplayLength": 15,   //records per page
            "sDom": "t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap"
        });
    });
<?php echo '</script'; ?>
>
<!--End  script phan trang-->

<div class="col-md-12">
    <div class="widget wred">
        <div class="widget-header">
            <div class="pull-left"><i class="icon-user"></i> USER MANAGEMENT</div>
            <div class="widget-icons pull-right">
                <a class="wminimize" href="#"><i class="icon-chevron-up"></i></a>
                <a class="wclose" href="#"><i class="icon-remove"></i></a>
            </div>
            <div class="clearfix"> &nbsp&nbsp<a href="users.php?frmAdd=AddNew" class="createUser" title="Add new">
                    <i class="icon-plus-sign"></i>&nbsp&nbspAdd new</a></div>
        </div>
        <div class="seacrh-user"><input name="search" class="input-xxlarge" id="search" placeholder="email, name, phone"/>
            &nbsp<input value="Search" type="button" class="btn-search" onclick="SearchClick()"/></div>
        <div class="widget-content">
            <table class="table table-bordered" id="example">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Role</th>
                    <th>Address</th>

                    <th class="title-action">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $_smarty_tpl->tpl_vars["stt"] = new Smarty_variable("1", null, 0);?>
                <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listUser']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>
                    <tr class="record" id="record">
                        <td><?php echo $_smarty_tpl->tpl_vars['stt']->value++;?>
</td>

                        <td><?php echo $_smarty_tpl->tpl_vars['i']->value['names'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['i']->value['email'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['i']->value['phone'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['i']->value['role'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['i']->value['address'];?>
</td>

                        <td class="td-action">

                                <a href="users.php?display=frmEdit&&id=<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" title="Edit" class="editUser"><i class="icon-pencil"></i></a>

                                <a href="#" title="Delete" id="<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" onclick="delUser('<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
')" class="delbutton">
                                    <i class="icon-trash"></i></a>


                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php }} ?>
