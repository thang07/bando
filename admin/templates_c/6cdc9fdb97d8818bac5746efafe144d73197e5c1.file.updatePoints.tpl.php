<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-02-06 03:59:14
         compiled from ".\templates\updatePoints.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2608654d422048411c3-39616688%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6cdc9fdb97d8818bac5746efafe144d73197e5c1' => 
    array (
      0 => '.\\templates\\updatePoints.tpl',
      1 => 1423191537,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2608654d422048411c3-39616688',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54d42204876283_92199510',
  'variables' => 
  array (
    'points' => 0,
    'point' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54d42204876283_92199510')) {function content_54d42204876283_92199510($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Edit Points</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="points.php?actionUpdate=Points" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <?php  $_smarty_tpl->tpl_vars["point"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["point"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['points']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["point"]->key => $_smarty_tpl->tpl_vars["point"]->value) {
$_smarty_tpl->tpl_vars["point"]->_loop = true;
?>
                    <div class="control-group">
                        <label class="control-label">Street Name</label>

                        <div class="controls">
                            <input class="input-xlarge" id="name_street" value="<?php echo $_smarty_tpl->tpl_vars['point']->value['name_street'];?>
" name="name_street"
                                   class="name_street" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Address (start)</label>

                        <div class="controls">
                            <input class="input-xlarge" id="address_start" value="<?php echo $_smarty_tpl->tpl_vars['point']->value['address_start'];?>
"
                                   name="address_start" class="address_start" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Address (end)</label>

                        <div class="controls">
                            <input class="input-xlarge" id="address_end" value="<?php echo $_smarty_tpl->tpl_vars['point']->value['address_end'];?>
" name="address_end"
                                   class="address_end" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Lat code (start) </label>

                        <div class="controls">
                            <input class="input-xlarge" id="lat_start" value="<?php echo $_smarty_tpl->tpl_vars['point']->value['lat_start'];?>
" name="lat_start"
                                   class="lat_start" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Long code (start) </label>

                        <div class="controls">
                            <input class="input-xlarge" id="long_start" value="<?php echo $_smarty_tpl->tpl_vars['point']->value['long_start'];?>
" name="long_start"
                                   class="long_start" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Lat code (end) </label>

                        <div class="controls">
                            <input class="input-xlarge" id="lat_end" value="<?php echo $_smarty_tpl->tpl_vars['point']->value['lat_end'];?>
" name="lat_end"
                                   class="lat_end" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Long code (end) </label>

                        <div class="controls">
                            <input class="input-xlarge" id="long_end" value="<?php echo $_smarty_tpl->tpl_vars['point']->value['long_end'];?>
" name="long_end"
                                   class="long_end" maxlength="200"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Color </label>

                        <div class="controls">
                            <select id="points-color" name="points-color">
                                <?php if ($_smarty_tpl->tpl_vars['point']->value['color']=='red') {?>
                                    <option value="red" selected="selected">Red</option>
                                    <option value="green">Green</option>
                                    <option value="yellow">Yellow</option>
                                <?php } elseif ($_smarty_tpl->tpl_vars['point']->value['color']=='green') {?>
                                    <option value="green" selected="selected">Green</option>
                                    <option value="yellow">Yellow</option>
                                    <option value="red">Red</option>
                                <?php } else { ?>
                                    <option value="yellow" selected="selected">Yellow</option>
                                    <option value="green">Green</option>
                                    <option value="red">Red</option>
                                <?php }?>

                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['point']->value['id'];?>
" name="id">
                        <button type="submit" class="btn btn-info pull-right" name="btnCreate">Update</button>
                    </div>
                <?php } ?>
            </form>
        </div>
    </div>
</div><?php }} ?>
