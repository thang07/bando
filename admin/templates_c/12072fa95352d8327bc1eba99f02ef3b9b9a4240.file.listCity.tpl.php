<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-01-28 11:00:44
         compiled from ".\templates\listCity.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1639154c891ba2feb30-25496450%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '12072fa95352d8327bc1eba99f02ef3b9b9a4240' => 
    array (
      0 => '.\\templates\\listCity.tpl',
      1 => 1422439243,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1639154c891ba2feb30-25496450',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54c891ba39fb66_06862432',
  'variables' => 
  array (
    'listLocation' => 0,
    'stt' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c891ba39fb66_06862432')) {function content_54c891ba39fb66_06862432($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<link href="templates/css/boostrapTable.css" rel="stylesheet">
<link rel="stylesheet" href="templates/css/colorbox.css"/>
<?php echo '<script'; ?>
 src="templates/js/jquery.colorbox.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".addLocation").colorbox(
                {
                    width: "550px"
                });
        $(".updateLocation").colorbox({ width: "550px"});

    });
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/datatables.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    function ConfirmDel()
    {   kq =confirm("Are you sure to delete ?");
        return kq;
    }
<?php echo '</script'; ?>
>

<div class="content">
    <?php echo $_smarty_tpl->getSubTemplate ("menuLeft.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <div class="mainbar">
        <div class="col-md-12">
            <div class="round-list">
                <div class="title-table">
                    <i class="icon-list"></i>MANAGER CITY &nbsp&nbsp<a href="city.php?frmAdd=frmAdd" title="Add new" class="addLocation"> <i class="icon-plus-sign"></i> &nbsp&nbspAdd New</a>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"
                       id="example">
                    <thead>
                    <tr>
                        <th class="sorting" id="th-width">No</th>
                        <th class="sorting" id="th-widthx">Location Name</th>
                        <th class="sorting">Lat Code</th>
                        <th class="sorting">Long Code</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->tpl_vars["stt"] = new Smarty_variable("1", null, 0);?>
                    <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listLocation']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>

                        <tr class="odd gradeX">
                            <td><?php echo $_smarty_tpl->tpl_vars['stt']->value++;?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['lat_code'];?>
 </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['i']->value['long_code'];?>
 </td>
                            <td class="text-center">
                                
                                    

                                <a href="city.php?editLo=edit&&loId=<?php echo $_smarty_tpl->tpl_vars['i']->value['id_city'];?>
" name="upLocation" class="updateLocation" title="Edit"><i class="icon-pencil"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><?php }} ?>
