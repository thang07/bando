<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-21 08:14:53
         compiled from ".\templates\header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:508955ade35da77a51-77033016%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9233a694d612a10cfe0d6be8864e0dfee31fc3d8' => 
    array (
      0 => '.\\templates\\header.tpl',
      1 => 1422420934,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '508955ade35da77a51-77033016',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55ade35df418c8_56845823',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55ade35df418c8_56845823')) {function content_55ade35df418c8_56845823($_smarty_tpl) {?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8">
    <link rel="shortcut icon" href="">

    <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

    <link href="templates/css/bootstrap.min.css" rel="stylesheet">
    <link href="templates/css/style.css" rel="stylesheet">
    <link href="templates/css/bootstrap.icon-large.min.css" rel="stylesheet" media="screen">
    <link href="templates/css/font-awesome.css" rel="stylesheet" media="screen">

    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="templates/js/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="templates/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript">if (window.location.hash == '#_=_')window.location.hash = '';<?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
        $(function () {
            var url = window.location.pathname,
                    urlRegExp = new RegExp(url.replace(/\/$/, ''));
            $('#nav li a').each(function () {
                if (urlRegExp.test(this.href)) {
                    $(this).addClass('active');
                }
            });
        });
    <?php echo '</script'; ?>
>



</head>

<body class="body">
<header class="navbar-fixed-top">
    <div class="img-logo"><a title="Page Index" href="" />GOOGLE MAP</a></div>
    <div class="navbar-right" id="navbar-right">
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">

                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-user"></i>
                        <?php if ($_SESSION['Login']!='') {?>
                            <?php echo $_SESSION['Email'];?>




                        <?php }?>
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu animated fadeInUp">
                        <li><a href="users.php?edit=profile"><i class="icon-ok"></i> Profile</a></li>
                        <li><a href="logout.php?logout=out"><i class="icon-off"></i> Logout</a></li>

                    </ul>
                </li>
            </ul>
        </div>
    </div>
</header>
<?php }} ?>
