<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-01-28 08:26:04
         compiled from ".\templates\addCity.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3223854c88ecba697f0-26271628%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0e05f41c8c825dce70eb3d7a8db1210d75c773c4' => 
    array (
      0 => '.\\templates\\addCity.tpl',
      1 => 1422429960,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3223854c88ecba697f0-26271628',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54c88ecba9f974_29198157',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c88ecba9f974_29198157')) {function content_54c88ecba9f974_29198157($_smarty_tpl) {?><?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="templates/js/jquery.validate.min.js"><?php echo '</script'; ?>
>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Add New Location</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" enctype="multipart/form-data" method="post"
                  action="city.php?action=add" id="frmCreateLocation"
                  name="frmCreateVehicle">
                <div class="control-group">
                    <label class="control-label">Location Name </label>
                    <div class="controls">
                        <input class="input-xlarge" id="flag" name="name" class="name" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Lat code </label>
                    <div class="controls">
                        <input class="input-xlarge" id="flag" name="lat" class="lat" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Long code </label>
                    <div class="controls">
                        <input class="input-xlarge" id="flag" name="long" class="long" maxlength="200"
                               type="text">
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Add New</button>
                </div>
            </form>
        </div>
    </div>
</div><?php }} ?>
