<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-23 04:40:50
         compiled from ".\templates\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:712354c8b07e1891f7-23268742%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '46c42bdf69db94827adfeaa45e7cd820b54b8ab0' => 
    array (
      0 => '.\\templates\\index.tpl',
      1 => 1435027247,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '712354c8b07e1891f7-23268742',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_54c8b07e1c2d02_67580285',
  'variables' => 
  array (
    'listMarkers' => 0,
    'i' => 0,
    'getParameter' => 0,
    'para' => 0,
    'pa' => 0,
    'listPoints' => 0,
    'p' => 0,
    'getCityIndex' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c8b07e1c2d02_67580285')) {function content_54c8b07e1c2d02_67580285($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Google map thong minh</title>
<link type="text/css" href="../css/style.css">
<style type="text/css">
    html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
    }

    .guide {
        position: absolute;
        right: 0;
        border: 1px solid;
        width: 300px;
        z-index: 5; font-size: 15px; font-family: Arial;
        top: 5px; background: #fff; padding: 15px 6px 6px 10px
    }
    .guide-all div.row {
        float: left;width: 100%;overflow: hidden;height: 40px;
    }

    .guide-all div.red {
        background: red; margin-right: 5px; width: 80px;float: left;height: 10px;
    }

    .guide-all div.yellow {
        background: #ffff00; margin-right: 5px;width: 80px;float: left;height: 10px;
    }
    .guide-all .span{
        margin-top: -4px;
    }

    .guide-all div.green {
        background: green; margin-right: 5px;width: 80px;float: left;height: 10px;
    }

    .option_cities {
        position: absolute;
        top: 5px;
        left: 20%;
        margin-left: -180px;
        z-index: 5;
    }

    .option_cities select {
        height: 32px;
        width: 200px; border: 2px solid blue;
    }
    * {
        margin: 0;
        padding: 0;
    }
    body {
        overflow-x: hidden;
    }
    #demo-top-bar {
        text-align: left;
        background: #222;
        position: relative;
        zoom: 1;
        width: 100% !important;
        z-index: 6000;
        padding: 20px 0 20px;
    }
    #demo-bar-inside {
        width: 960px;
        margin: 0 auto;
        position: relative;
    }
    #demo-bar-buttons {
        padding-top: 10px;
        float: right;
    }
    #demo-bar-buttons a {
        font-size: 12px;
        margin-left: 20px;
        color: white;
        margin: 2px 0;
        text-decoration: none;
        font: 14px "Lucida Grande", Sans-Serif !important;
    }
    #demo-bar-buttons a:hover,
    #demo-bar-buttons a:focus {
        text-decoration: underline;
    }
    #demo-bar-badge {
        display: inline-block;
        width: 302px;
        padding: 0 !important;
        margin: 0 !important;
        background-color: transparent !important;
    }
    #demo-bar-badge a {
        display: block;
        width: 100%;
        height: 38px;
        border-radius: 0;
        bottom: auto;
        margin: 0;

        background-size: 100%;
        overflow: hidden;
        text-indent: -9999px;
    }
    #demo-bar-badge:before, #demo-bar-badge:after {
        display: none !important;
    }

</style>
<?php echo '<script'; ?>
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA35L7WJm-ED4KS9VQe1tHMEPtCvjzeKwY"
        type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="../js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">

var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var infoWindow;
var info;


// su dung kieu icon cho cac nga tu
var customIcons = {
    green: {
        icon: 'http://demo.lms.vn/bando/images/green.png',
        shadow: 'http://labs.google.com/ridefinder/images/mm_20_shadow.png'
    },
    red: {
        icon: 'http://demo.lms.vn/bando/images/red.png',
        shadow: 'http://labs.google.com/ridefinder/images/mm_20_shadow.png'
    },
    yellow: {
        icon: 'http://demo.lms.vn/bando/images/yellow.png',
        shadow: 'http://labs.google.com/ridefinder/images/mm_20_shadow.png'
    }
    //scaledSize: new google.maps.Size(5, 5)
};
//su dung icon cho diem dau va diem cuoi
var markerIcon = {
    green: {
        icon: 'http://demo.lms.vn/bando/images/mgreen.png'
    },
    red: {
        icon: 'http://demo.lms.vn/bando/images/mred.png'
    },
    yellow: {
        icon: 'http://demo.lms.vn/bando/images/myellow.png'
    }
};

// ham khoi tao google
function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();

    var tphcm = new google.maps.LatLng(10.760009, 106.680547);
    var mapOptions = {
        zoom:17,
        center: tphcm,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false,
        scaleControl: true,
        scrollwheel: false,
        disableDoubleClickZoom: true
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    directionsDisplay.setMap(map);
    infoWindow = new google.maps.InfoWindow;
    directionsDisplay.setMap(map);

    //Hien thi marker
    getMarkerIcon();

    //ve duong
    getPoint();
    displayMarkerEndPoint();

}
//end
//ham chon city
function selectCityRoute() {
    var start = document.getElementById('option_select_city').value;
    var end = document.getElementById('option_select_city').value;
    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.TravelMode.WALKING,
        optimizeWaypoints: false


    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });

}
//
//Ham hien thi marker icon
function getMarkerIcon() {
    <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listMarkers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>
    var id_marker = '<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
';
    var name = '<?php echo $_smarty_tpl->tpl_vars['i']->value['marker_name'];?>
' + " ";
    var lat_marker = '<?php echo $_smarty_tpl->tpl_vars['i']->value['lat_marker'];?>
';
    var long_marker = '<?php echo $_smarty_tpl->tpl_vars['i']->value['long_marker'];?>
';
    var color = '<?php echo $_smarty_tpl->tpl_vars['i']->value['color'];?>
';
    var point = new google.maps.LatLng(
            parseFloat(lat_marker),
            parseFloat(long_marker));
    var html = "<b>" + name + "</b> <br/>" + color;
    var icon = customIcons[color];
    var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: icon.icon,
        shadow: icon.shadow

    });

    contentString =
            '<div class="content_marker" style="width: 650px;">' +
                    '<div id="info-street" style="width: 100%;height: 30px;float: left;text-align: center;">' + name + '</div>' +

                    '<div id="siteNotice" style="width: 297px;float: left">' +
                    <?php  $_smarty_tpl->tpl_vars["para"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["para"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['getParameter']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["para"]->key => $_smarty_tpl->tpl_vars["para"]->value) {
$_smarty_tpl->tpl_vars["para"]->_loop = true;
?>

                    <?php if ($_smarty_tpl->tpl_vars['i']->value['id']==$_smarty_tpl->tpl_vars['para']->value['id_marker']) {?>

                    '<table  border="0px" style="width: 290px;">' +
                    '<tr>' +
                    '<td class="td-top-left" style="height: 65px;width: 130px;text-align: right;vertical-align: top;  border-right: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-right-radius: 5px">' +
                    '<img height="70px" width="85px" style="" src="upload/<?php echo $_smarty_tpl->tpl_vars['para']->value['img_top_vertical'];?>
"/>' + '</td>' +
                    '<td style="width: 50px;text-align: left">' +
                    '<div style="font-family:Arial;font-size:11px; "> <?php echo $_smarty_tpl->tpl_vars['para']->value['name_street_vertical'];?>
</div>' + '</td>' +
                    '<td class="td-top-right" style="height: 95px;width: 130px;text-align: right;vertical-align: bottom;bottom: auto; border-left: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-left-radius:5px">' +
                    '<img height="65px" style="" width="85px" src="upload/<?php echo $_smarty_tpl->tpl_vars['para']->value['img_right_horizontal'];?>
"/>' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td class="td-center-left" style="height: 30px;text-align: center;vertical-align: bottom">' + '<label style="font-size: 11px;"><?php echo $_smarty_tpl->tpl_vars['para']->value['name_street_horizontal'];?>
</label>' + '</td>' +
                    '<td style="height: 40px;width: 30px;text-align: center;vertical-align: middle">' +
                    <?php if ($_smarty_tpl->tpl_vars['i']->value['color']=='red') {?>
                    '<img src="red.png" height="30px" width="33px"/>' +
                    <?php } else { ?>
                    '<img src="green.png" height="29px" width="33px"/>' +
                    <?php }?>
                    '</td>' +
                    '<td class="td-center-right" style="text-align: center;height: 30px;vertical-align: top">' + '<label style="font-size: 11px;"><?php echo $_smarty_tpl->tpl_vars['para']->value['name_street_horizontal'];?>
</label>' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td class="td-bottom-left" style="height: 95px;text-align: left;vertical-align: top;border-top: 2px solid #cfcfcf;border-right: 2px solid #cfcfcf;border-top-right-radius: 5px">' +
                    '<img height="65px" width="85px" style="" src="upload/<?php echo $_smarty_tpl->tpl_vars['para']->value['img_left_horizontal'];?>
"/>' + '</td>' +
                    '<td style="width: 50px;text-align: right">' + '<div style="font-size:11px;"><?php echo $_smarty_tpl->tpl_vars['para']->value['name_street_vertical'];?>
</div> ' + '</td>' +
                    '<td class="td-bottom-right" style="height: 95px;border-top: 2px solid #cfcfcf;border-left: 2px solid #cfcfcf;border-top-left-radius: 5px">' +
                    '<img height="65px" width="85px" src="upload/<?php echo $_smarty_tpl->tpl_vars['para']->value['img_bottom_vertical'];?>
"/>' + '</td>' +
                    '</tr>' +

                    '</table>' +
                    <?php }?>

                    <?php } ?>
                    '</div>' +
                    '<div id="info" style="width: 327px;float: right;overflow: hidden">' +
                    '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +

                    <?php  $_smarty_tpl->tpl_vars["pa"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["pa"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['getParameter']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["pa"]->key => $_smarty_tpl->tpl_vars["pa"]->value) {
$_smarty_tpl->tpl_vars["pa"]->_loop = true;
if ($_smarty_tpl->tpl_vars['pa']->value['id_marker']==$_smarty_tpl->tpl_vars['i']->value['id']) {?>

                    '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
                    '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
                    '<th style="border: 1px solid #cfcfcf">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['name_street_horizontal'];?>
' + '</th>' +
                    '<th style="border: 1px solid #cfcfcf">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['name_street_vertical'];?>
' + '</th>' +
                    '</tr>' +
                    '<tbody>' +

                    '<tr>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['cycle'];?>
 giây' + '</td>' +
                    '</tr>' +
                    '<tr style="background: #f6f6f6">' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Vận tốc' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['speech_horizontal'];?>
 km/h' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['speech_vertical'];?>
 km/h' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Mật độ' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['consistence_horizontal'];?>
 m/2' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['consistence_vertical'];?>
 m/2' + '</td>' +
                    '</tr>' +
                    '<tr style="background: #f6f6f6">' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Xanh' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['time_green_horizontal'];?>
 giây' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['time_green_vertical'];?>
 giây' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Vàng' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['time_yellow'];?>
 giây' + '</td>' +
                    '</tr>' +

                    '</tbody>' +
                    <?php }
} ?>
                    '</table>' +
                    '</br>'+

                    <?php if ($_SESSION['Email']=='nosession') {?>

                    <?php } else { ?>


                    '<form action="parameter.php" method="post"> '+
                    '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +

                    <?php  $_smarty_tpl->tpl_vars["pa"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["pa"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['getParameter']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["pa"]->key => $_smarty_tpl->tpl_vars["pa"]->value) {
$_smarty_tpl->tpl_vars["pa"]->_loop = true;
?>
                    <?php if ($_smarty_tpl->tpl_vars['pa']->value['id_marker']==$_smarty_tpl->tpl_vars['i']->value['id']) {?>
                    '<tr><td colspan="2" style="text-align: center;font-weight:bold ">Điều Chỉnh Thông số</td></tr>'+
                    '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
                    '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
                    '<th style="border: 1px solid #cfcfcf">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['name_street_horizontal'];?>
' + '</th>' +
                    '<th style="border: 1px solid #cfcfcf">' + '<?php echo $_smarty_tpl->tpl_vars['pa']->value['name_street_vertical'];?>
' + '</th>' +
                    '</tr>' +
                    '<tbody>' +

                    '<tr>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2" class="numbers-row">' + '<input type="text" name="cycle" id="partridge" style="width: 30px;text-align:center;font-size:12px" value="<?php echo $_smarty_tpl->tpl_vars['pa']->value['cycle'];?>
" >' +' giây' + '</td>' +
                    '</tr>' +
                    '<tr style="background: #f6f6f6">' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Vận tốc' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' +'</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Mật độ' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
                    '</tr>' +
                    '<tr style="background: #f6f6f6">' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Xanh' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_horizontal" style="width: 30px;text-align:center;font-size:12px" value="<?php echo $_smarty_tpl->tpl_vars['pa']->value['time_green_horizontal'];?>
">' +' giây' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_vertical" style="width: 30px;text-align:center;font-size:12px" value="<?php echo $_smarty_tpl->tpl_vars['pa']->value['time_green_vertical'];?>
">'+ ' giây' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Vàng' + '</td>' +
                    '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' +'<input type="text" name="time_yellow" style="width: 30px;text-align:center;font-size:12px" value="<?php echo $_smarty_tpl->tpl_vars['pa']->value['time_yellow'];?>
">'+ ' giây' +'  ' + '<input type="submit" value="Cập nhật">'+'<input type="hidden" name="id_parama" value="<?php echo $_smarty_tpl->tpl_vars['pa']->value['id_parameter'];?>
">' +'</td>' +

                    '</tr>' +

                    '</tbody>' +
                    <?php }?>
                    <?php } ?>
                    '</table>' +
                    '</form>'+

                    <?php }?>
                    '</div>'+
                    '</div>';

    bindInfoWindow(marker, map, infoWindow, contentString);

    <?php } ?>
}
//
//mo hop tohia window
function bindInfoWindow(marker, map, infoWindow, html) {
    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
    });
}



//ham lay point chi duonng
function getPoint() {

    <?php  $_smarty_tpl->tpl_vars["p"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["p"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listPoints']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["p"]->key => $_smarty_tpl->tpl_vars["p"]->value) {
$_smarty_tpl->tpl_vars["p"]->_loop = true;
?>
    var lat_begin = '<?php echo $_smarty_tpl->tpl_vars['p']->value['lat_start'];?>
';
    var long_begin = '<?php echo $_smarty_tpl->tpl_vars['p']->value['long_start'];?>
';
    var lat_end = '<?php echo $_smarty_tpl->tpl_vars['p']->value['lat_end'];?>
';
    var long_end = '<?php echo $_smarty_tpl->tpl_vars['p']->value['long_end'];?>
';
    var lat_long_start = new google.maps.LatLng(lat_begin, long_begin);
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var myTrip = [lat_long_start, lat_long_end];
    var col = '<?php echo $_smarty_tpl->tpl_vars['p']->value['color'];?>
';
    //Khoi tao polyline ve duong dan
    var flightPath = new google.maps.Polyline({
        path: myTrip,
        fillColor: '<?php echo $_smarty_tpl->tpl_vars['p']->value['color'];?>
',
        strokeColor: '<?php echo $_smarty_tpl->tpl_vars['p']->value['color'];?>
',
        strokeOpacity: 0.5,
        strokeWeight: 8,
        fillOpacity: 0.35
        //scale:2
    });
    var icon = markerIcon[col];//mang icon
    //khoi tao marker
    var marker = new google.maps.Marker({
        position: lat_long_start,
        icon: icon.icon

    });
    contentString = '<?php echo $_smarty_tpl->tpl_vars['p']->value['address_start'];?>
';//gan bien chuoi hien thi infoWindow
    bindInfoWindow(marker, map, infoWindow, contentString);
    marker.setMap(map);//setup marker vao map
    flightPath.setMap(map);//ran ve duong vao map
    <?php } ?>

}
//Hien thi custom icon marker cho lat long diem cuoi
function displayMarkerEndPoint() {
    <?php  $_smarty_tpl->tpl_vars["p"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["p"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['listPoints']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["p"]->key => $_smarty_tpl->tpl_vars["p"]->value) {
$_smarty_tpl->tpl_vars["p"]->_loop = true;
?>

    var lat_begin = '<?php echo $_smarty_tpl->tpl_vars['p']->value['lat_start'];?>
';
    var long_begin = '<?php echo $_smarty_tpl->tpl_vars['p']->value['long_start'];?>
';
    var lat_end = '<?php echo $_smarty_tpl->tpl_vars['p']->value['lat_end'];?>
';
    var long_end = '<?php echo $_smarty_tpl->tpl_vars['p']->value['long_end'];?>
';
    var col = '<?php echo $_smarty_tpl->tpl_vars['p']->value['color'];?>
';
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var icon = markerIcon[col];//mang custom icon
    var marker = new google.maps.Marker({
        position: lat_long_end,
        icon: icon.icon //ran icon vao
    });
    contentString = '<?php echo $_smarty_tpl->tpl_vars['p']->value['address_end'];?>
';
    marker.setMap(map);//set up marker

    bindInfoWindow(marker, map, infoWindow, contentString);
    <?php } ?>

}
//end

google.maps.event.addDomListener(window, 'load', initialize);
//getPoint();
<?php echo '</script'; ?>
>

</head>

<body onload="getMarkerIcon()">
<input type="hidden" value="" id="getParamete" name="getParamete"/>

<div id="idd" name="idd" class="idd"></div>

<div class="option_cities">
    <select class="option_select_city" id="option_select_city" name="option_select_city" onchange="selectCityRoute();">
        <option selected="selected" value="10.825108, 106.605099">Chọn các quận</option>
        <?php  $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["i"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['getCityIndex']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["i"]->key => $_smarty_tpl->tpl_vars["i"]->value) {
$_smarty_tpl->tpl_vars["i"]->_loop = true;
?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value['lat_code'];?>
,<?php echo $_smarty_tpl->tpl_vars['i']->value['long_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
</option>
        <?php } ?>
    </select>
</div>

<div class="guide">
    <div class="guide-all">
        <div class="row">
            <div class="green"></div>
            <div class="span">Đường thông thoáng</div>
        </div>
        <div class="row">
            <div class="yellow"></div>
            <div class="span">Đường sắp tắc nghẽn</div>
        </div>

        <div class="row">
            <div class="red"></div>
            <div class="span">Đường tắc nghẽn</div>
        </div>
    </div>
</div>
<div id="map-canvas"></div>
</body>
</html><?php }} ?>
