-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2015 at 05:46 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `google_map`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_marker` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `name`, `id_marker`) VALUES
(1, 'Ngã ba', 0),
(2, 'Ngã tư', 0),
(3, 'Ngã năm', 0),
(4, 'Ngã sáu', 0),
(7, 'Ngã bảy', 0);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `id_city` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lat_code` double DEFAULT NULL,
  `long_code` double DEFAULT NULL,
  PRIMARY KEY (`id_city`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id_city`, `name`, `lat_code`, `long_code`) VALUES
(1, 'Quận 1', 10.776111, 106.695833),
(2, 'Quận 2', 10.780833, 106.756944),
(3, 'Quận 3', 10.78, 106.679444),
(4, 'Quận 4', 10.761667, 106.7025),
(5, 'Quận 5', 10.756667, 106.666667),
(6, 'Quận 6', 10.746111, 106.636111),
(7, 'Quận 7', 10.738611, 106.726389),
(8, 'Quận 8', 10.723333, 106.627778),
(9, 'Quận 9', 10.830278, 106.8175),
(10, 'Quận 10', 10.773167, 106.670058),
(11, 'Quận 11', 10.763386, 106.646678),
(12, 'Quận 12', 10.875195, 106.649091),
(13, 'Bình Tân', 10.813306, 106.651964),
(14, 'Bình Thạnh', 10.81195, 106.70634),
(15, 'Gò Vấp', 10.843332, 106.667452),
(16, 'Phú Nhuận', 10.801509, 106.678111),
(17, 'Tân Bình', 10.816594, 106.651793),
(18, ' Tân Phú', 10.805213, 106.622996),
(19, 'Thủ Đức', 10.845764, 106.743943);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_marker` int(11) NOT NULL,
  `file_img` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `id_marker`, `file_img`, `datetime`) VALUES
(2, 2, 'ketxe.jpg', NULL),
(3, 3, 'ketxe.jpg', NULL),
(4, 5, 'kx.jpg', NULL),
(5, 13, '20916257-images1982833_hinh4.jpg', NULL),
(6, 5, 'ketxe2.jpg', NULL),
(7, 7, 'ketxequa.jpg', NULL),
(8, 6, 'nguyenmk.jpg', NULL),
(9, 12, 'ketxe2.jpg', NULL),
(10, 9, 'nguyenmk.jpg', NULL),
(11, 16, 'nguyenmk.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `marker`
--

DROP TABLE IF EXISTS `marker`;
CREATE TABLE IF NOT EXISTS `marker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lat_marker` double NOT NULL,
  `long_marker` double NOT NULL,
  `marker_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `id_city` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `marker`
--

INSERT INTO `marker` (`id`, `lat_marker`, `long_marker`, `marker_name`, `color`, `id_city`) VALUES
(1, 10.76429, 106.670233, '301 Nguyễn Duy Dương, p4, Quận 10,Hồ Chí Minh,Việt Nam', 'red', 10),
(2, 10.76427, 106.670237, '168 Vĩnh Viễn, phường 2, Quận 10, Hồ Chí Minh, Việt Nam', 'green', 10),
(3, 10.766657, 106.671788, '109 Bà Hạt, 9 Hồ Chí Minh, Việt Nam ', 'yellow', 10),
(5, 10.769676, 106.670764, '31 Sư Vạn Hạnh, phường 10, Quận 10, Hồ Chí Minh, Vietnam', 'red', 10),
(6, 10.782859, 106.69804, '106 Hai Bà Trưng, phường 6, Quận 1, Hồ Chí Minh, Việt Nam', 'red', 1),
(7, 10.781493, 106.699593, '125 Hai Bà Trưng, Bến Nghé Hồ Chí Minh, Việt Nam ', 'green', 1),
(8, 10.784506, 106.707907, '1 Nguyễn Hữu Cảnh, Bến Nghé, Quận 1,  Hồ Chí Minh, Việt Nam', 'yellow', 1),
(9, 10.788663, 106.755084, '25 Nguyễn Duy Trinh, Bình Trưng Tây, Quận 2, Hồ Chí Minh,', 'red', 2),
(10, 10.784329, 106.757551, '22 đường Số 6 Bình Trưng Tây Quận 2 Hồ Chí Minh,', 'green', 2),
(11, 10.777245, 106.754307, 'Lê Hiến Mai, Thạnh Mỹ Lợi Quận 2 Hồ Chí Minh', 'green', 2),
(12, 10.79863, 106.631801, '218 Tân Sơn Nhì, Tân Phú Hồ Chí Minh, Vietnam', 'green', 18),
(13, 10.78701, 106.644081, '308 Hồng Lạc, 11, Tân Bình Hồ Chí Minh, Vietnam', 'yellow', 17),
(14, 10.758447, 106.661484, '26 Lý Thường Kiệt, 7 Hồ Chí Minh, Vietnam', 'yellow', 10),
(15, 10.760192, 106.661047, '92 Lý Thường Kiệt, 7 Hồ Chí Minh, Vietnam', 'green', 10),
(16, 10.759739, 106.6588, '45 Lê Đại Hành, phường 7, Quận 11 Hồ Chí Minh, Vietnam', 'yellow', 10),
(17, 10.774045, 106.69903, '163 Lê Thánh Tôn, Bến Thành, Quận 1, Hồ Chí Minh, Vietnam', 'green', 1),
(18, 10.773261, 106.699837, '3 Nguyễn Trung Trực, Bến Thành, Quận 1, Hồ Chí Minh, Vietnam', 'yellow', 1);

-- --------------------------------------------------------

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
CREATE TABLE IF NOT EXISTS `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_marker` int(11) NOT NULL,
  `name_street` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cycle` int(11) NOT NULL,
  `speed` float NOT NULL,
  `consistence` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `time_green` int(11) NOT NULL,
  `time_yellow` int(11) NOT NULL,
  `time_red` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `parameter`
--

INSERT INTO `parameter` (`id`, `id_marker`, `name_street`, `cycle`, `speed`, `consistence`, `time_green`, `time_yellow`, `time_red`) VALUES
(1, 1, 'Nguyễn Duy Dương', 85, 5, '15/10', 5, 40, NULL),
(2, 2, 'Vĩnh Viễn', 80, 20, '5', 5, 45, NULL),
(3, 3, 'Bà Hạt', 40, 25, '10/10', 40, 3, NULL),
(4, 5, 'Sư Vạn Hạnh', 80, 5, '10/10', 50, 50, NULL),
(5, 6, 'Hai Bà Trưng', 45, 20, '15/10', 30, 5, NULL),
(6, 7, 'Hai Bà Trưng', 45, 20, '10/10', 30, 3, NULL),
(7, 3, 'Su vạn hạnh', 40, 10, '10/10', 35, 5, NULL),
(8, 13, 'Bầu cát', 35, 15, '10/10', 30, 5, NULL),
(9, 13, 'Hồng Lạc', 70, 20, '15/20', 62, 8, NULL),
(10, 6, 'Nguyễn thị minh khai', 50, 15, '10/10', 45, 5, NULL),
(11, 7, 'Lê Duẩn', 35, 20, '15/10', 30, 5, NULL),
(12, 12, 'Diệp minh châu', 40, 10, '10', 36, 4, NULL),
(13, 5, 'Dường 3/2', 40, 16, '20', 35, 5, NULL),
(14, 12, 'Tân Sơn Nhì', 30, 10, '10', 27, 3, NULL),
(15, 9, 'Nguyễn Duy Trinh', 30, 10, '10', 27, 3, NULL),
(16, 9, 'Nguyễn thị định', 35, 15, '15', 30, 5, NULL),
(17, 10, 'Nguyễn thị định', 25, 5, '5', 20, 5, NULL),
(18, 10, 'Đường số 6', 20, 10, '5', 15, 5, NULL),
(19, 16, ' Lê Đại Hành', 40, 20, '15', 35, 5, NULL),
(20, 16, 'Trần quý', 30, 16, '10', 27, 3, NULL),
(21, 16, 'Hòa hảo', 30, 12, '10', 25, 5, NULL),
(22, 15, 'Lý Thường Kiệt', 40, 15, '10', 35, 5, NULL),
(23, 8, 'Nguyễn Hữu Cảnh', 30, 20, '15', 27, 3, NULL),
(24, 8, 'Nguyễn bỉnh khiêm', 35, 20, '15', 30, 5, NULL),
(25, 17, 'Lê Thánh Tôn', 40, 10, '10', 30, 10, NULL),
(26, 11, 'Thạnh Mỹ Lợi', 40, 20, '15', 34, 6, NULL),
(27, 11, 'Lê Hiến Mai', 40, 20, '16', 37, 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `point`
--

DROP TABLE IF EXISTS `point`;
CREATE TABLE IF NOT EXISTS `point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `names` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lat` float NOT NULL,
  `long` float NOT NULL,
  `type` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `point`
--

INSERT INTO `point` (`id`, `names`, `address`, `lat`, `long`, `type`, `date`) VALUES
(1, 'karaoke Ban Mai', '121 Đại La, Trương Định, Hai Bà Trưng Hanoi', 20.9967, 105.847, 'karaoke', '0000-00-00 00:00:00'),
(2, 'karaoke Hoa H?ng', '74 phố Vọng, Phương Mai, Đống Đa Hanoi, Vietnam', 20.9978, 20.9978, 'karaoke', '0000-00-00 00:00:00'),
(3, 'nhà hàng Xuân Tuyết', '226, Đồng Tâm, Hai Bà Trưng, Hanoi, Vietnam', 21.0022, 105.843, 'nhahang', '0000-00-00 00:00:00'),
(4, 'karaoke New Star', '345 phố Vọng, Đồng Tâm, Hai Bà Trưng Hanoi, Vietnam', 20.9935, 105.842, 'karaoke', '0000-00-00 00:00:00'),
(5, 'Nhà hàng Hoàng Gia', '4 Tạ Quang Bửu, Bách Khoa, Hai Bà Trưng Hanoi, Vietnam', 21.008, 105.848, 'nhahang', '0000-00-00 00:00:00'),
(6, 'nhà hàng Mái Đỏ', '29 Đại Cồ Việt, Hai Bà Trưng Hanoi, Vietnam', 21.0086, 105.85, 'nhahang', '0000-00-00 00:00:00'),
(7, 'karaoke Alibaba', '5 Hoa Lư, Lê Đại Hành, Hai Bà Trưng Hanoi, Vietnam', 21.0104, 105.848, 'karaoke', '0000-00-00 00:00:00'),
(8, 'Nhà hàng Hải Hưng', '283 HỒ BA MẪU, Phương Liên, Đống Đa Hanoi, Vietnam', 21.0104, 105.841, 'nhahang', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

DROP TABLE IF EXISTS `points`;
CREATE TABLE IF NOT EXISTS `points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_street` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address_start` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address_end` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `lat_start` double NOT NULL,
  `long_start` double NOT NULL,
  `lat_end` double NOT NULL,
  `long_end` double NOT NULL,
  `color` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `datime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `name_street`, `address_start`, `address_end`, `lat_start`, `long_start`, `lat_end`, `long_end`, `color`, `datime`) VALUES
(1, ' Nguyễn Thái Học ', '199 Nguyễn Thái Học, Phạm Ngũ Lão, Hồ Chí Minh, Vi', '165 Nguyễn Thái Học, Phạm Ngũ Lão, Hồ Chí Minh, Vietnam', 10.769173, 106.694664, 10.768074, 106.695279, 'red', '0000-00-00 00:00:00'),
(2, 'Trần Hưng Đạo', '71 Trần Hưng Đạo, Nguyễn Thái Bình, Quận 1, Hồ Chí', '6 Trần Hưng Đạo, Nguyễn Thái Bình, Quận 1, Hồ Chí Minh, Vietnam', 10.768065, 106.695427, 10.769182, 106.696404, 'green', '0000-00-00 00:00:00'),
(3, 'Nguyễn Thái Học', '117 Nguyễn Thái Học, Quận 1, Hồ Chí Minh, Vietnam', 'Cầu Ông Lãnh, Cầu Ông Lãnh, Quận 1, Hồ Chí Minh, Vietnam', 10.767936, 106.695409, 10.765067, 106.697053, 'yellow', '0000-00-00 00:00:00'),
(4, 'Nguyễn Kim', '85 Nguyễn Kim, Hồ Chí Minh, Vietnam', '23 Nguyễn Kim, 7, Hồ Chí Minh, Vietnam', 10.759581, 106.662603, 10.758698, 106.662733, 'red', '0000-00-00 00:00:00'),
(5, 'Nguyễn Chí Thanh', '394 Nguyễn Chí Thanh, Hồ Chí Minh, Vietnam', '268 Nguyễn Chí Thanh, Quận 11, Hồ Chí Minh, Vietnam', 10.75925, 106.665572, 10.759888, 106.668707, 'green', '0000-00-00 00:00:00'),
(6, 'Ngô Gia Tự', '268 Nguyễn Chí Thanh, Quận 11, Hồ Chí Minh, Vietna', '546 Ngô Gia Tự, 9, Hồ Chí Minh, Vietnam', 10.759701, 106.668805, 10.756251, 106.6663, 'yellow', '0000-00-00 00:00:00'),
(7, ' Trường Chinh', '532 Trường Chinh, 13, Tân Bình Hồ Chí Minh, Vietna', '8-17 Trường Chinh, Phường 4, Tân Bình Hồ Chí Minh, Vietnam', 10.801762, 106.636786, 10.793132, 106.652959, 'red', '0000-00-00 00:00:00'),
(8, 'Thủ Khoa Huân', '73 Thủ Khoa Huân, Bến Thành, Quận 1, Hồ Chí Minh, ', '53 Thủ Khoa Huân, Bến Thành, Quận 1, Hồ Chí Minh, Vietnam', 10.774823, 106.696237, 10.773993, 106.697097, 'green', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `street`
--

DROP TABLE IF EXISTS `street`;
CREATE TABLE IF NOT EXISTS `street` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_marker` int(11) NOT NULL,
  `name_street` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `names` varchar(45) NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(45) NOT NULL,
  `role` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `names`, `phone`, `address`, `role`) VALUES
(1, 'hongdiepbach@gmail.com', '123456', 'Hong Diep', 12223444, 'nguyen duy', 'admin'),
(3, 'hoadiepdo@gmail.com', '20eabe5d64b0e216796e834f52d61fd0b70332fc', 'Quynh', 4232322, '168, ly thuong kiet, q10, tphcm', 'user');
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
