<?php
require "include/smarty.php";
require "include/index_function.php";
require "include/points_function.php";
require "include/marker_funtion.php";
require "include/parameter_function.php";

session_start();

$smarty=new Smarty();
$smartys=new SmartyBC();

//hien thi trang index
$listCity=getCityIndex();
$smarty->assign("getCityIndex",$listCity);
//$_SESSION['Email']="";
if($_SESSION["Email"]==null || $_SESSION['Email']==""){
    $_SESSION['Email']="nosession";
    $smarty->assign("Email", $_SESSION['Email']);
}

//hien thi marker marker
$listMarkers=getAllMarkers();
$smarty->assign("listMarkers",$listMarkers);
//lay point
$listPoints=getPoints();
$smarty->assign("listPoints",$listPoints);
$getParameter=getParameter();
$smarty->assign('getParameter', $getParameter);
//$getImgById=getAllImg();
//$smarty->assign('getImgById',$getImgById);
$smarty->assign("title","Google map Page");

$smarty->display("index.tpl");