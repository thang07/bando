-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2015 at 08:57 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `google_map`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `id_city` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lat_code` double DEFAULT NULL,
  `long_code` double DEFAULT NULL,
  PRIMARY KEY (`id_city`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id_city`, `name`, `lat_code`, `long_code`) VALUES
(1, 'Quận 1', 10.770437, 106.693017),
(2, 'Quận 2', 10.780833, 106.756944),
(3, 'Quận 3', 10.78, 106.679444),
(4, 'Quận 4', 10.761667, 106.7025),
(5, 'Quận 5', 10.756667, 106.666667),
(6, 'Quận 6', 10.746111, 106.636111),
(7, 'Quận 7', 10.738611, 106.726389),
(8, 'Quận 8', 10.723333, 106.627778),
(9, 'Quận 9', 10.830278, 106.8175),
(10, 'Quận 10', 10.773167, 106.670058),
(11, 'Quận 11', 10.763386, 106.646678),
(12, 'Quận 12', 10.875195, 106.649091),
(13, 'Bình Tân', 10.813306, 106.651964),
(14, 'Bình Thạnh', 10.81195, 106.70634),
(15, 'Gò Vấp', 10.843332, 106.667452),
(16, 'Phú Nhuận', 10.801509, 106.678111),
(17, 'Tân Bình', 10.816594, 106.651793),
(18, ' Tân Phú', 10.805213, 106.622996),
(19, 'Thủ Đức', 10.845764, 106.743943);

-- --------------------------------------------------------

--
-- Table structure for table `marker`
--

DROP TABLE IF EXISTS `marker`;
CREATE TABLE IF NOT EXISTS `marker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lat_marker` double NOT NULL,
  `long_marker` double NOT NULL,
  `marker_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `id_city` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `marker`
--

INSERT INTO `marker` (`id`, `lat_marker`, `long_marker`, `marker_name`, `color`, `id_city`) VALUES
(1, 10.76427, 106.67023, '293 Nguyễn Duy Dương, phường 4, Quận 10, Hồ Chí Minh, Vietnam', 'green', 10),
(2, 10.758854, 106.677632, '195 Lê Hồng Phong, Quận 5 Hồ Chí Minh, Vietnam ', 'red', 10),
(3, 10.769295, 106.698133, '159 Calmette, Nguyễn Thái Bình, Quận 1 Hồ Chí Minh, Vietnam ', 'red', 1),
(4, 10.755863, 106.683648, '106 Nguyễn Biểu, 1 Hồ Chí Minh, Vietnam ', 'green', 1),
(5, 10.754052, 106.676911, '764 Trần Hưng Đạo Hồ Chí Minh, Vietnam ', 'green', 1),
(6, 10.752647, 106.675038, '81 Bùi Hữu Nghĩa, Quận 5 Hồ Chí Minh, Vietnam ', 'red', 1),
(7, 10.770384, 106.692062, '8 Nguyễn Văn Tráng, Bến Thành Hồ Chí Minh, Việt Nam ', 'red', 1),
(8, 10.768307, 106.698885, '114 Calmette, Nguyễn Thái Bình Hồ Chí Minh, Việt Nam ', 'red', 1),
(9, 10.759802, 106.680164, '225 Trần Bình Trọng, phường 3, Quận 5 Hồ Chí Minh, Việt Nam ', 'green', 19),
(10, 10.758137, 106.680824, '167 Trần Bình Trọng, Quận 5 Hồ Chí Minh, Việt Nam ', 'green', 1),
(11, 10.757067, 106.678185, '112 Lê Hồng Phong, phường 2, Quận 5 Hồ Chí Minh, Việt Nam ', 'green', 1);

-- --------------------------------------------------------

--
-- Table structure for table `parameter_street`
--

DROP TABLE IF EXISTS `parameter_street`;
CREATE TABLE IF NOT EXISTS `parameter_street` (
  `id_parameter` int(11) NOT NULL AUTO_INCREMENT,
  `id_marker` int(11) NOT NULL,
  `name_street_horizontal` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_street_vertical` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cycle` int(11) DEFAULT NULL,
  `speech_horizontal` int(11) DEFAULT NULL,
  `speech_vertical` int(11) NOT NULL,
  `consistence_horizontal` int(11) DEFAULT NULL,
  `consistence_vertical` int(11) DEFAULT NULL,
  `time_green_horizontal` int(11) DEFAULT NULL,
  `time_green_vertical` int(11) DEFAULT NULL,
  `time_yellow` int(11) DEFAULT NULL,
  `img_top_vertical` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_bottom_vertical` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_left_horizontal` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_right_horizontal` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_parameter`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `parameter_street`
--

INSERT INTO `parameter_street` (`id_parameter`, `id_marker`, `name_street_horizontal`, `name_street_vertical`, `cycle`, `speech_horizontal`, `speech_vertical`, `consistence_horizontal`, `consistence_vertical`, `time_green_horizontal`, `time_green_vertical`, `time_yellow`, `img_top_vertical`, `img_bottom_vertical`, `img_left_horizontal`, `img_right_horizontal`) VALUES
(1, 1, 'Vĩnh Viễn', 'Nguyễn Duy Dương', 80, 10, 12, 20, 13, 10, 14, 10, '3.jpg', '4.jpg', '1.jpg', '2.jpg'),
(2, 2, 'An Dương Vương', 'Lê Hồng Phong', 70, 70, 60, 10, 15, 5, 5, 65, '7.jpg', '22.jpg', '5555.jpg', '444.jpg'),
(3, 3, 'Lê Thị Hồng Gấm', 'Calmettec', 35, 10, 15, 15, 15, 30, 30, 5, '6.jpg', '1.jpg', '3.jpg', '2.jpg'),
(4, 4, 'Trần Hưng Đạo', 'Nguyễn Biểu', 35, 15, 16, 16, 15, 30, 30, 5, '8.jpg', '1.jpg', '6.jpg', '7.jpg'),
(5, 5, 'Trần Hưng Đạo', 'Huỳnh Mẫn Đạt', 30, 15, 14, 18, 10, 5, 5, 25, '2222.jpg', '111.jpg', '44.jpg', '11111.jpg'),
(6, 6, 'Nghĩa Thục', 'Bùi Hữu Nghĩa', 30, 10, 15, 15, 15, 20, 20, 10, '4.jpg', '8.jpg', '44.jpg', '22.jpg'),
(7, 7, 'Nguyễn Trãi', 'Nguyễn Văn Tráng', 35, 10, 15, 15, 15, 30, 30, 5, '2.jpg', '5.jpg', '1.jpg', '4.jpg'),
(8, 8, 'Nguyễn Thái Bình', 'Calmette', 30, 15, 16, 10, 15, 20, 20, 10, '2.jpg', '3.jpg', '1.jpg', '2.jpg'),
(9, 9, 'An Dương Vương', 'Trần Bình Trọng', 45, 8, 5, 7, 8, 40, 40, 5, '555.jpg', '66.jpg', '777.jpg', '5.jpg'),
(10, 10, 'Nguyễn Trãi', 'Trần Bình Trọng', 40, 10, 14, 5, 9, 30, 30, 10, '55.jpg', '6.jpg', '11.jpg', '33.jpg'),
(11, 11, 'Nguyễn trãi', 'Lê Hồng Phong', 40, 15, 14, 15, 14, 30, 30, 10, '22.jpg', '5.jpg', '1.jpg', '3333.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

DROP TABLE IF EXISTS `points`;
CREATE TABLE IF NOT EXISTS `points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_street` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address_start` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address_end` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `lat_start` double NOT NULL,
  `long_start` double NOT NULL,
  `lat_end` double NOT NULL,
  `long_end` double NOT NULL,
  `color` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `datime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `name_street`, `address_start`, `address_end`, `lat_start`, `long_start`, `lat_end`, `long_end`, `color`, `datime`) VALUES
(9, 'An Dương Vương', '393 An Dương Vương, Quận 5 Hồ Chí Minh, Vietnam , ', '469 An Dương Vương, 3 Hồ Chí Minh, Vietnam ', 10.758894, 106.677558, 10.758164, 106.675846, 'green', '0000-00-00 00:00:00'),
(10, 'Nguyễn Duy Dương', '305 Nguyễn Duy Dương, phường 4, Quận 10 Hồ Chí Min', '337-376 Nguyễn Duy Dương, phường 4, Quận 10 Hồ Chí Minh, Vietnam ', 10.764345, 106.670208, 10.765194, 106.670007, 'green', '0000-00-00 00:00:00'),
(11, ' Vĩnh Viễn', '302 Nguyễn Duy Dương, 4 Hồ Chí Minh, Vietnam ', '320 Nguyễn Duy Dương, 4 Hồ Chí Minh, Vietnam 10.764431, 106.670703', 10.764319, 106.670339, 10.764431, 106.670703, 'red', '0000-00-00 00:00:00'),
(12, 'Vĩnh Viễn', '189 Vĩnh Viễn, phường 4, Quận 10 Hồ Chí Minh, Viet', '318-322 Vĩnh Viễn, phường 4, Quận 10 Hồ Chí Minh, Vietnam ', 10.764284, 106.670177, 10.763798, 106.668108, 'yellow', '0000-00-00 00:00:00'),
(13, 'Hẻm 100 Lê Thị Hồng Gấm, Nguyễn Thái Bình,', 'Hẻm 100 Lê Thị Hồng Gấm, Nguyễn Thái Bình, Quận 1 ', '36 Lê Thị Hồng Gấm, Nguyễn Thái Bình Hồ Chí Minh, Vietnam ', 10.769074, 106.697767, 10.769943, 106.698721, 'yellow', '0000-00-00 00:00:00'),
(14, 'An Dương Vương', '389 An Dương Vương, 3 Hồ Chí Minh, Vietnam ', '377 An Dương Vương, 3 Hồ Chí Minh, Vietnam', 10.758955, 106.6777, 10.759109, 106.678113, 'yellow', '0000-00-00 00:00:00'),
(15, 'Trần Hưng Đạo', '360 Trần Hưng Đạo, phường 1, Quận 5 Hồ Chí Minh, V', '711-787 Trần Hưng Đạo, phường 1, Quận 5 Hồ Chí Minh, Vietnam ', 10.756444, 106.6851, 10.75604, 106.683804, 'red', '0000-00-00 00:00:00'),
(16, 'Nguyễn Văn Cừ', '162 Nguyễn Văn Cừ, Nguyễn Cư Trinh Hồ Chí Minh, Vi', '74 Nguyễn Văn Cừ, Nguyễn Cư Trinh Hồ Chí Minh, Vietnam ', 10.759356, 106.684014, 10.756848, 106.685003, 'red', '0000-00-00 00:00:00'),
(17, 'Bùi Viện,', '57 Bùi Viện, Phạm Ngũ Lão Hồ Chí Minh, Việt Nam ', '43 Bùi Viện, Phạm Ngũ Lão Hồ Chí Minh, Việt Nam ', 10.767613, 106.694432, 10.767162, 106.693451, 'red', '0000-00-00 00:00:00'),
(18, 'Đề Thám', '189-191 Đề Thám, Phạm Ngũ Lão, Quận 1 Hồ Chí Minh,', '173-177 Đề Thám, Phạm Ngũ Lão, Quận 1 Hồ Chí Minh, Việt Nam ', 10.767488, 106.693956, 10.767132, 106.694151, 'green', '0000-00-00 00:00:00'),
(19, 'Phan Văn Trị', '317 Phan Văn Trị, 2 Hồ Chí Minh, Việt Nam ', '22 Phan Văn Trị, 7 Hồ Chí Minh, Việt Nam ', 10.755956, 106.678479, 10.755261, 106.676414, 'green', '0000-00-00 00:00:00'),
(20, 'Nguyễn Trãi', '213 Nguyễn Trãi, 2 Hồ Chí Minh, Việt Nam ', '359 Phan Văn Trị, 2 Hồ Chí Minh, Việt Nam ', 10.756649, 106.67698, 10.755573, 106.677275, 'green', '0000-00-00 00:00:00'),
(21, 'Lê Hồng Phong', '112 Lê Hồng Phong, phường 2, Quận 5 Hồ Chí Minh, V', '2 Lê Hồng Phong, 2 Hồ Chí Minh, Việt Nam ', 10.757065, 106.678172, 10.754746, 106.678954, 'red', '0000-00-00 00:00:00'),
(22, 'An Dương Vươn', '373 An Dương Vương, 3 Hồ Chí Minh, Việt Nam ', '235 Nguyễn Văn Cừ, phường 3, Quận 5 Hồ Chí Minh, Việt Nam ', 10.75912, 106.678131, 10.761175, 106.683351, 'yellow', '0000-00-00 00:00:00'),
(23, 'Nguyễn Văn Cừ', '241 Nguyễn Văn Cừ, 4 Hồ Chí Minh, Việt Nam ', '162 Nguyễn Văn Cừ, Nguyễn Cư Trinh Hồ Chí Minh, Việt Nam ', 10.765177, 106.681771, 10.759365, 106.684013, 'yellow', '0000-00-00 00:00:00'),
(24, 'Nguyễn Trãi', '52-56 Nguyễn Trãi, Quận 5 Hồ Chí Minh, Việt Nam ', '389 Nguyễn Trãi, Quận 5 Hồ Chí Minh, Việt Nam ', 10.758165, 106.680523, 10.759447, 106.684091, 'yellow', '0000-00-00 00:00:00'),
(25, 'Nguyễn Văn Cừ', '74 Nguyễn Văn Cừ, Nguyễn Cư Trinh Hồ Chí Minh, Việ', '62 Nguyễn Văn Cừ, Quận 11 Hồ Chí Minh, Việt Nam ', 10.756849, 106.685002, 10.756377, 106.685201, 'green', '0000-00-00 00:00:00'),
(26, 'Nguyễn Biểu', '261 Nguyễn Biểu, phường 2, Quận 5 Hồ Chí Minh, Việ', '106 Nguyễn Biểu, 1 Hồ Chí Minh, Việt Nam ', 10.758919, 106.682607, 10.755944, 106.683673, 'red', '0000-00-00 00:00:00'),
(28, 'Nguyễn Trãi', '99 Nguyễn Trãi, 2 Hồ Chí Minh, Việt Nam ', '169 Nguyễn Trãi, phường 2, Quận 5 Hồ Chí Minh, Việt Nam ', 10.758165, 106.680513, 10.75715, 106.678073, 'yellow', '0000-00-00 00:00:00'),
(29, 'Trần Hưng Đạo', '679-727 Trần Hưng Đạo, phường 1, Quận 5 Hồ Chí Min', '811-827 Trần Hưng Đạo, phường 1, Quận 5 Hồ Chí Minh, Việt Nam ', 10.756049, 106.683815, 10.755224, 106.680793, 'red', '0000-00-00 00:00:00'),
(30, 'Lê Hồng Phong', '188 Lê Hồng Phong, Quận 5 Hồ Chí Minh, Việt Nam ', '128-130 Lê Hồng Phong, Quận 5 Hồ Chí Minh, Việt Nam ', 10.759081, 106.677591, 10.75742, 106.67817, 'green', '0000-00-00 00:00:00'),
(31, 'Nguyễn Trãi', '169 Nguyễn Trãi, 2 Hồ Chí Minh, Việt Nam ', '268 Nguyễn Trãi, 8 Hồ Chí Minh, Việt Nam ', 10.757126, 106.678008, 10.756252, 106.675971, 'red', '0000-00-00 00:00:00'),
(32, 'Trần Hưng Đạo', '811-827 Trần Hưng Đạo, phường 1, Quận 5 Hồ Chí Min', '758 Trần Hưng Đạo Hồ Chí Minh, Việt Nam ', 10.75526, 106.680794, 10.75414, 106.676833, 'yellow', '0000-00-00 00:00:00'),
(33, 'Nguyễn Văn Cừ', '149 Nguyễn Văn Cừ, phường 1, Quận 5 Hồ Chí Minh, V', '100 Phan Van Tri, 2 Hồ Chí Minh, Việt Nam ', 10.75795, 106.684571, 10.757049, 106.682408, 'green', '0000-00-00 00:00:00'),
(34, 'Trần Bình Trọng', '167 Trần Bình Trọng, Quận 5 Hồ Chí Minh, Việt Nam ', '150-153 Trần Bình Trọng, Quận 5 Hồ Chí Minh, Việt Nam ', 10.758228, 106.680801, 10.757793, 106.680865, 'green', '0000-00-00 00:00:00'),
(35, 'Trần Bình Trọng', '109 Trần Bình Trọng, phường 2, Quận 5 Hồ Chí Minh,', '91 Trần Bình Trọng, 2 Hồ Chí Minh, Việt Nam ', 10.756636, 106.681032, 10.755441, 106.681402, 'green', '0000-00-00 00:00:00'),
(36, '', '164 Trần Bình Trọng, Quận 5 Hồ Chí Minh, Việt Nam ', '188-190 Trần Bình Trọng, phường 3, Quận 5 Hồ Chí Minh, Việt Nam ', 10.759073, 106.68043, 10.759671, 106.680212, 'yellow', '0000-00-00 00:00:00'),
(37, '210 Huỳnh Mẫn Đạt', '210 Huỳnh Mẫn Đạt, phường 3, Quận 5 Hồ Chí Minh, V', '764 Trần Hưng Đạo Hồ Chí Minh, Việt Nam ', 10.758133, 106.675757, 10.754131, 106.676894, 'green', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `names` varchar(45) NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(45) NOT NULL,
  `role` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `names`, `phone`, `address`, `role`) VALUES
(1, 'hongdiepbach@gmail.com', 'lekima_00', 'Hong Diep', 12223444, 'nguyen duy', 'admin'),
(3, 'hoadiepdo@gmail.com', '20eabe5d64b0e216796e834f52d61fd0b70332fc', 'Quynh', 4232322, '168, ly thuong kiet, q10, tphcm', 'user'),
(4, 'admin@gmail.com', 'lekima_00', 'admin', 123456, '', 'admin');
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
