<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Google map thong minh</title>
<link type="text/css" href="../css/style.css">
<style type="text/css">
    html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
    }

    .guide {
        position: absolute;
        right: 0;
        border: 1px solid;
        width: 300px;
        z-index: 5; font-size: 15px; font-family: Arial;
        top: 5px; background: #fff; padding: 15px 6px 6px 10px
    }
    .guide-all div.row {
        float: left;width: 100%;overflow: hidden;height: 40px;
    }

    .guide-all div.red {
        background: red; margin-right: 5px; width: 80px;float: left;height: 10px;
    }

    .guide-all div.yellow {
        background: #ffff00; margin-right: 5px;width: 80px;float: left;height: 10px;
    }
    .guide-all .span{
        margin-top: -4px;
    }

    .guide-all div.green {
        background: green; margin-right: 5px;width: 80px;float: left;height: 10px;
    }

    .option_cities {
        position: absolute;
        top: 5px;
        left: 20%;
        margin-left: -180px;
        z-index: 5;
    }

    .option_cities select {
        height: 32px;
        width: 200px; border: 2px solid blue;
    }
    * {
        margin: 0;
        padding: 0;
    }
    body {
        overflow-x: hidden;
    }
    #demo-top-bar {
        text-align: left;
        background: #222;
        position: relative;
        zoom: 1;
        width: 100% !important;
        z-index: 6000;
        padding: 20px 0 20px;
    }
    #demo-bar-inside {
        width: 960px;
        margin: 0 auto;
        position: relative;
    }
    #demo-bar-buttons {
        padding-top: 10px;
        float: right;
    }
    #demo-bar-buttons a {
        font-size: 12px;
        margin-left: 20px;
        color: white;
        margin: 2px 0;
        text-decoration: none;
        font: 14px "Lucida Grande", Sans-Serif !important;
    }
    #demo-bar-buttons a:hover,
    #demo-bar-buttons a:focus {
        text-decoration: underline;
    }
    #demo-bar-badge {
        display: inline-block;
        width: 302px;
        padding: 0 !important;
        margin: 0 !important;
        background-color: transparent !important;
    }
    #demo-bar-badge a {
        display: block;
        width: 100%;
        height: 38px;
        border-radius: 0;
        bottom: auto;
        margin: 0;

        background-size: 100%;
        overflow: hidden;
        text-indent: -9999px;
    }
    #demo-bar-badge:before, #demo-bar-badge:after {
        display: none !important;
    }

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA35L7WJm-ED4KS9VQe1tHMEPtCvjzeKwY"
        type="text/javascript"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<script src="../js/jquery.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.js"></script>

<script type="text/javascript">

var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var infoWindow;
var info;


// su dung kieu icon cho cac nga tu
var customIcons = {
    green: {
        icon: 'http://bando.dev.com/images/green.png',
        shadow: 'http://labs.google.com/ridefinder/images/mm_20_shadow.png'
    },
    red: {
        icon: 'http://bando.dev.com/images/red.png',
        shadow: 'http://labs.google.com/ridefinder/images/mm_20_shadow.png'
    },
    yellow: {
        icon: 'http://bando.dev.com/images/yellow.png',
        shadow: 'http://labs.google.com/ridefinder/images/mm_20_shadow.png'
    }
    //scaledSize: new google.maps.Size(5, 5)
};
//su dung icon cho diem dau va diem cuoi
var markerIcon = {
    green: {
        icon: 'http://bando.dev.com/images/mgreen.png'
    },
    red: {
        icon: 'http://bando.dev.com/images/mred.png'
    },
    yellow: {
        icon: 'http://bando.dev.com/images/myellow.png'
    }
};

// ham khoi tao google
function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();

    var start = document.getElementById('option_select_city').selected;
    if(document.getElementById('option_select_city').selected){

        var op = new google.maps.LatLng(start);
        var mapOptions= {
            zoom:17,
            center: op,
            mapTypeId: google.maps.MapTypeId.ROADMAP

        };


    }else{
        var tphcm = new google.maps.LatLng(10.760009, 106.680547);
        var mapOptions = {
            zoom:17,
            center: tphcm,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: false,
            scaleControl: true,
            scrollwheel: false,
            disableDoubleClickZoom: false
        };


    }
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    directionsDisplay.setMap(map);
    infoWindow = new google.maps.InfoWindow;
    directionsDisplay.setMap(map);

    //Hien thi marker
    getMarkerIcon();

    //ve duong
    getPoint();
    displayMarkerEndPoint();
}
//end
//ham chon city
function selectCityRoute() {
    var start = document.getElementById('option_select_city').value;
    var end = document.getElementById('option_select_city').value;
    var request = {
        origin:start,
        // destination:end,
        travelMode: google.maps.TravelMode.WALKING


    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });

}


//
//Ham hien thi marker icon
function getMarkerIcon() {
    var id_marker = '1';
    var name = '293 Nguyễn Duy Dương, phường 4, Quận 10, Hồ Chí Minh, Vietnam' + " ";
    var lat_marker = '10.76427';
    var long_marker = '106.67023';
    var color = 'green';
    var point = new google.maps.LatLng(
        parseFloat(lat_marker),
        parseFloat(long_marker));
    var html = "<b>" + name + "</b> <br/>" + color;
    var icon = customIcons[color];
    var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: icon.icon,
        shadow: icon.shadow

    });

    contentString =
        '<div class="content_marker" style="width: 650px;">' +
            '<div id="info-street" style="width: 100%;height: 30px;float: left;text-align: center;">' + name + '</div>' +

            '<div id="siteNotice" style="width: 297px;float: left">' +


            '<table  border="0px" style="width: 290px;">' +
            '<tr>' +
            '<td class="td-top-left" style="height: 65px;width: 130px;text-align: right;vertical-align: top;  border-right: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-right-radius: 5px">' +
            '<img height="70px" width="85px" style="" src="upload/3.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: left">' +
            '<div style="font-family:Arial;font-size:11px; "> Nguyễn Duy Dương</div>' + '</td>' +
            '<td class="td-top-right" style="height: 95px;width: 130px;text-align: right;vertical-align: bottom;bottom: auto; border-left: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-left-radius:5px">' +
            '<img height="65px" style="" width="85px" src="upload/2.jpg"/>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-center-left" style="height: 30px;text-align: center;vertical-align: bottom">' + '<label style="font-size: 11px;">Vĩnh Viễn</label>' + '</td>' +
            '<td style="height: 40px;width: 30px;text-align: center;vertical-align: middle">' +
            '<img src="green.png" height="29px" width="33px"/>' +
            '</td>' +
            '<td class="td-center-right" style="text-align: center;height: 30px;vertical-align: top">' + '<label style="font-size: 11px;">Vĩnh Viễn</label>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-bottom-left" style="height: 95px;text-align: left;vertical-align: top;border-top: 2px solid #cfcfcf;border-right: 2px solid #cfcfcf;border-top-right-radius: 5px">' +
            '<img height="65px" width="85px" style="" src="upload/1.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: right">' + '<div style="font-size:11px;">Nguyễn Duy Dương</div> ' + '</td>' +
            '<td class="td-bottom-right" style="height: 95px;border-top: 2px solid #cfcfcf;border-left: 2px solid #cfcfcf;border-top-left-radius: 5px">' +
            '<img height="65px" width="85px" src="upload/4.jpg"/>' + '</td>' +
            '</tr>' +

            '</table>' +











            '</div>' +
            '<div id="info" style="width: 327px;float: right;overflow: hidden">' +
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +


            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Vĩnh Viễn' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Nguyễn Duy Dương' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '80 giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '10 km/h' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '12 km/h' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '20 m/2' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '13 m/2' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '10 giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '14 giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '10 giây' + '</td>' +
            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</br>'+



            '<form action="parameter.php" method="post"> '+
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +

            '<tr><td colspan="2" style="text-align: center;font-weight:bold ">Điều Chỉnh Thông số</td></tr>'+
            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Vĩnh Viễn' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Nguyễn Duy Dương' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2" class="numbers-row">' + '<input type="text" name="cycle" id="partridge" style="width: 30px;text-align:center;font-size:12px" value="80" >' +' giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_horizontal" style="width: 30px;text-align:center;font-size:12px" value="10">' +' giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_vertical" style="width: 30px;text-align:center;font-size:12px" value="14">'+ ' giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' +'<input type="text" name="time_yellow" style="width: 30px;text-align:center;font-size:12px" value="10">'+ ' giây' +'  ' + '<input type="submit" value="Cập nhật">'+'<input type="hidden" name="id_parama" value="1">' +'</td>' +

            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</form>'+

            '</div>'+
            '</div>';

    bindInfoWindow(marker, map, infoWindow, contentString);

    var id_marker = '2';
    var name = '195 Lê Hồng Phong, Quận 5 Hồ Chí Minh, Vietnam ' + " ";
    var lat_marker = '10.758821';
    var long_marker = '106.677673';
    var color = 'red';
    var point = new google.maps.LatLng(
        parseFloat(lat_marker),
        parseFloat(long_marker));
    var html = "<b>" + name + "</b> <br/>" + color;
    var icon = customIcons[color];
    var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: icon.icon,
        shadow: icon.shadow

    });

    contentString =
        '<div class="content_marker" style="width: 650px;">' +
            '<div id="info-street" style="width: 100%;height: 30px;float: left;text-align: center;">' + name + '</div>' +

            '<div id="siteNotice" style="width: 297px;float: left">' +




            '<table  border="0px" style="width: 290px;">' +
            '<tr>' +
            '<td class="td-top-left" style="height: 65px;width: 130px;text-align: right;vertical-align: top;  border-right: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-right-radius: 5px">' +
            '<img height="70px" width="85px" style="" src="upload/7.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: left">' +
            '<div style="font-family:Arial;font-size:11px; "> Lê Hồng Phong</div>' + '</td>' +
            '<td class="td-top-right" style="height: 95px;width: 130px;text-align: right;vertical-align: bottom;bottom: auto; border-left: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-left-radius:5px">' +
            '<img height="65px" style="" width="85px" src="upload/6.jpg"/>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-center-left" style="height: 30px;text-align: center;vertical-align: bottom">' + '<label style="font-size: 11px;">An Dương Vương</label>' + '</td>' +
            '<td style="height: 40px;width: 30px;text-align: center;vertical-align: middle">' +
            '<img src="red.png" height="30px" width="33px"/>' +
            '</td>' +
            '<td class="td-center-right" style="text-align: center;height: 30px;vertical-align: top">' + '<label style="font-size: 11px;">An Dương Vương</label>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-bottom-left" style="height: 95px;text-align: left;vertical-align: top;border-top: 2px solid #cfcfcf;border-right: 2px solid #cfcfcf;border-top-right-radius: 5px">' +
            '<img height="65px" width="85px" style="" src="upload/5.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: right">' + '<div style="font-size:11px;">Lê Hồng Phong</div> ' + '</td>' +
            '<td class="td-bottom-right" style="height: 95px;border-top: 2px solid #cfcfcf;border-left: 2px solid #cfcfcf;border-top-left-radius: 5px">' +
            '<img height="65px" width="85px" src="upload/8.jpg"/>' + '</td>' +
            '</tr>' +

            '</table>' +









            '</div>' +
            '<div id="info" style="width: 327px;float: right;overflow: hidden">' +
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +


            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'An Dương Vương' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Lê Hồng Phong' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '60 giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '60 km/h' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '50 km/h' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '10 m/2' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '10 m/2' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '5 giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '5 giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '55 giây' + '</td>' +
            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</br>'+



            '<form action="parameter.php" method="post"> '+
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +

            '<tr><td colspan="2" style="text-align: center;font-weight:bold ">Điều Chỉnh Thông số</td></tr>'+
            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'An Dương Vương' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Lê Hồng Phong' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2" class="numbers-row">' + '<input type="text" name="cycle" id="partridge" style="width: 30px;text-align:center;font-size:12px" value="60" >' +' giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_horizontal" style="width: 30px;text-align:center;font-size:12px" value="5">' +' giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_vertical" style="width: 30px;text-align:center;font-size:12px" value="5">'+ ' giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' +'<input type="text" name="time_yellow" style="width: 30px;text-align:center;font-size:12px" value="55">'+ ' giây' +'  ' + '<input type="submit" value="Cập nhật">'+'<input type="hidden" name="id_parama" value="2">' +'</td>' +

            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</form>'+

            '</div>'+
            '</div>';

    bindInfoWindow(marker, map, infoWindow, contentString);

    var id_marker = '3';
    var name = '159 Calmette, Nguyễn Thái Bình, Quận 1 Hồ Chí Minh, Vietnam ' + " ";
    var lat_marker = '10.769348';
    var long_marker = '106.698101';
    var color = 'red';
    var point = new google.maps.LatLng(
        parseFloat(lat_marker),
        parseFloat(long_marker));
    var html = "<b>" + name + "</b> <br/>" + color;
    var icon = customIcons[color];
    var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: icon.icon,
        shadow: icon.shadow

    });

    contentString =
        '<div class="content_marker" style="width: 650px;">' +
            '<div id="info-street" style="width: 100%;height: 30px;float: left;text-align: center;">' + name + '</div>' +

            '<div id="siteNotice" style="width: 297px;float: left">' +






            '<table  border="0px" style="width: 290px;">' +
            '<tr>' +
            '<td class="td-top-left" style="height: 65px;width: 130px;text-align: right;vertical-align: top;  border-right: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-right-radius: 5px">' +
            '<img height="70px" width="85px" style="" src="upload/6.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: left">' +
            '<div style="font-family:Arial;font-size:11px; "> Calmettec</div>' + '</td>' +
            '<td class="td-top-right" style="height: 95px;width: 130px;text-align: right;vertical-align: bottom;bottom: auto; border-left: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-left-radius:5px">' +
            '<img height="65px" style="" width="85px" src="upload/2.jpg"/>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-center-left" style="height: 30px;text-align: center;vertical-align: bottom">' + '<label style="font-size: 11px;">Lê Thị Hồng Gấm</label>' + '</td>' +
            '<td style="height: 40px;width: 30px;text-align: center;vertical-align: middle">' +
            '<img src="red.png" height="30px" width="33px"/>' +
            '</td>' +
            '<td class="td-center-right" style="text-align: center;height: 30px;vertical-align: top">' + '<label style="font-size: 11px;">Lê Thị Hồng Gấm</label>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-bottom-left" style="height: 95px;text-align: left;vertical-align: top;border-top: 2px solid #cfcfcf;border-right: 2px solid #cfcfcf;border-top-right-radius: 5px">' +
            '<img height="65px" width="85px" style="" src="upload/3.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: right">' + '<div style="font-size:11px;">Calmettec</div> ' + '</td>' +
            '<td class="td-bottom-right" style="height: 95px;border-top: 2px solid #cfcfcf;border-left: 2px solid #cfcfcf;border-top-left-radius: 5px">' +
            '<img height="65px" width="85px" src="upload/1.jpg"/>' + '</td>' +
            '</tr>' +

            '</table>' +







            '</div>' +
            '<div id="info" style="width: 327px;float: right;overflow: hidden">' +
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +


            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Lê Thị Hồng Gấm' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Calmettec' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '35 giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '10 km/h' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 km/h' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 m/2' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 m/2' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '30 giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '30 giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '5 giây' + '</td>' +
            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</br>'+



            '<form action="parameter.php" method="post"> '+
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +

            '<tr><td colspan="2" style="text-align: center;font-weight:bold ">Điều Chỉnh Thông số</td></tr>'+
            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Lê Thị Hồng Gấm' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Calmettec' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2" class="numbers-row">' + '<input type="text" name="cycle" id="partridge" style="width: 30px;text-align:center;font-size:12px" value="35" >' +' giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_horizontal" style="width: 30px;text-align:center;font-size:12px" value="30">' +' giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_vertical" style="width: 30px;text-align:center;font-size:12px" value="30">'+ ' giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' +'<input type="text" name="time_yellow" style="width: 30px;text-align:center;font-size:12px" value="5">'+ ' giây' +'  ' + '<input type="submit" value="Cập nhật">'+'<input type="hidden" name="id_parama" value="3">' +'</td>' +

            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</form>'+

            '</div>'+
            '</div>';

    bindInfoWindow(marker, map, infoWindow, contentString);

    var id_marker = '4';
    var name = '106 Nguyễn Biểu, 1 Hồ Chí Minh, Vietnam ' + " ";
    var lat_marker = '10.755978';
    var long_marker = '106.683649';
    var color = 'green';
    var point = new google.maps.LatLng(
        parseFloat(lat_marker),
        parseFloat(long_marker));
    var html = "<b>" + name + "</b> <br/>" + color;
    var icon = customIcons[color];
    var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: icon.icon,
        shadow: icon.shadow

    });

    contentString =
        '<div class="content_marker" style="width: 650px;">' +
            '<div id="info-street" style="width: 100%;height: 30px;float: left;text-align: center;">' + name + '</div>' +

            '<div id="siteNotice" style="width: 297px;float: left">' +








            '<table  border="0px" style="width: 290px;">' +
            '<tr>' +
            '<td class="td-top-left" style="height: 65px;width: 130px;text-align: right;vertical-align: top;  border-right: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-right-radius: 5px">' +
            '<img height="70px" width="85px" style="" src="upload/8.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: left">' +
            '<div style="font-family:Arial;font-size:11px; "> Nguyễn Biểu</div>' + '</td>' +
            '<td class="td-top-right" style="height: 95px;width: 130px;text-align: right;vertical-align: bottom;bottom: auto; border-left: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-left-radius:5px">' +
            '<img height="65px" style="" width="85px" src="upload/7.jpg"/>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-center-left" style="height: 30px;text-align: center;vertical-align: bottom">' + '<label style="font-size: 11px;">Trần Hưng Đạo</label>' + '</td>' +
            '<td style="height: 40px;width: 30px;text-align: center;vertical-align: middle">' +
            '<img src="green.png" height="29px" width="33px"/>' +
            '</td>' +
            '<td class="td-center-right" style="text-align: center;height: 30px;vertical-align: top">' + '<label style="font-size: 11px;">Trần Hưng Đạo</label>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-bottom-left" style="height: 95px;text-align: left;vertical-align: top;border-top: 2px solid #cfcfcf;border-right: 2px solid #cfcfcf;border-top-right-radius: 5px">' +
            '<img height="65px" width="85px" style="" src="upload/6.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: right">' + '<div style="font-size:11px;">Nguyễn Biểu</div> ' + '</td>' +
            '<td class="td-bottom-right" style="height: 95px;border-top: 2px solid #cfcfcf;border-left: 2px solid #cfcfcf;border-top-left-radius: 5px">' +
            '<img height="65px" width="85px" src="upload/1.jpg"/>' + '</td>' +
            '</tr>' +

            '</table>' +





            '</div>' +
            '<div id="info" style="width: 327px;float: right;overflow: hidden">' +
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +


            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Trần Hưng Đạo' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Nguyễn Biểu' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '30 giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 km/h' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '16 km/h' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '16 m/2' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 m/2' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '30 giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '30 giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '5 giây' + '</td>' +
            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</br>'+



            '<form action="parameter.php" method="post"> '+
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +

            '<tr><td colspan="2" style="text-align: center;font-weight:bold ">Điều Chỉnh Thông số</td></tr>'+
            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Trần Hưng Đạo' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Nguyễn Biểu' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2" class="numbers-row">' + '<input type="text" name="cycle" id="partridge" style="width: 30px;text-align:center;font-size:12px" value="30" >' +' giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_horizontal" style="width: 30px;text-align:center;font-size:12px" value="30">' +' giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_vertical" style="width: 30px;text-align:center;font-size:12px" value="30">'+ ' giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' +'<input type="text" name="time_yellow" style="width: 30px;text-align:center;font-size:12px" value="5">'+ ' giây' +'  ' + '<input type="submit" value="Cập nhật">'+'<input type="hidden" name="id_parama" value="4">' +'</td>' +

            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</form>'+

            '</div>'+
            '</div>';

    bindInfoWindow(marker, map, infoWindow, contentString);

    var id_marker = '5';
    var name = '764 Trần Hưng Đạo Hồ Chí Minh, Vietnam ' + " ";
    var lat_marker = '10.754136';
    var long_marker = '106.676887';
    var color = 'green';
    var point = new google.maps.LatLng(
        parseFloat(lat_marker),
        parseFloat(long_marker));
    var html = "<b>" + name + "</b> <br/>" + color;
    var icon = customIcons[color];
    var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: icon.icon,
        shadow: icon.shadow

    });

    contentString =
        '<div class="content_marker" style="width: 650px;">' +
            '<div id="info-street" style="width: 100%;height: 30px;float: left;text-align: center;">' + name + '</div>' +

            '<div id="siteNotice" style="width: 297px;float: left">' +










            '<table  border="0px" style="width: 290px;">' +
            '<tr>' +
            '<td class="td-top-left" style="height: 65px;width: 130px;text-align: right;vertical-align: top;  border-right: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-right-radius: 5px">' +
            '<img height="70px" width="85px" style="" src="upload/ketxemug.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: left">' +
            '<div style="font-family:Arial;font-size:11px; "> Huỳnh Mẫn Đạt</div>' + '</td>' +
            '<td class="td-top-right" style="height: 95px;width: 130px;text-align: right;vertical-align: bottom;bottom: auto; border-left: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-left-radius:5px">' +
            '<img height="65px" style="" width="85px" src="upload/ketxe2.jpg"/>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-center-left" style="height: 30px;text-align: center;vertical-align: bottom">' + '<label style="font-size: 11px;">Trần Hưng Đạo</label>' + '</td>' +
            '<td style="height: 40px;width: 30px;text-align: center;vertical-align: middle">' +
            '<img src="green.png" height="29px" width="33px"/>' +
            '</td>' +
            '<td class="td-center-right" style="text-align: center;height: 30px;vertical-align: top">' + '<label style="font-size: 11px;">Trần Hưng Đạo</label>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-bottom-left" style="height: 95px;text-align: left;vertical-align: top;border-top: 2px solid #cfcfcf;border-right: 2px solid #cfcfcf;border-top-right-radius: 5px">' +
            '<img height="65px" width="85px" style="" src="upload/ketxe2.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: right">' + '<div style="font-size:11px;">Huỳnh Mẫn Đạt</div> ' + '</td>' +
            '<td class="td-bottom-right" style="height: 95px;border-top: 2px solid #cfcfcf;border-left: 2px solid #cfcfcf;border-top-left-radius: 5px">' +
            '<img height="65px" width="85px" src="upload/ketxemug.jpg"/>' + '</td>' +
            '</tr>' +

            '</table>' +



            '</div>' +
            '<div id="info" style="width: 327px;float: right;overflow: hidden">' +
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +


            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Trần Hưng Đạo' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Huỳnh Mẫn Đạt' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '30 giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 km/h' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '14 km/h' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 m/2' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '10 m/2' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '5 giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '5 giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '25 giây' + '</td>' +
            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</br>'+



            '<form action="parameter.php" method="post"> '+
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +

            '<tr><td colspan="2" style="text-align: center;font-weight:bold ">Điều Chỉnh Thông số</td></tr>'+
            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Trần Hưng Đạo' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Huỳnh Mẫn Đạt' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2" class="numbers-row">' + '<input type="text" name="cycle" id="partridge" style="width: 30px;text-align:center;font-size:12px" value="30" >' +' giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_horizontal" style="width: 30px;text-align:center;font-size:12px" value="5">' +' giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_vertical" style="width: 30px;text-align:center;font-size:12px" value="5">'+ ' giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' +'<input type="text" name="time_yellow" style="width: 30px;text-align:center;font-size:12px" value="25">'+ ' giây' +'  ' + '<input type="submit" value="Cập nhật">'+'<input type="hidden" name="id_parama" value="5">' +'</td>' +

            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</form>'+

            '</div>'+
            '</div>';

    bindInfoWindow(marker, map, infoWindow, contentString);

    var id_marker = '6';
    var name = '81 Bùi Hữu Nghĩa, Quận 5 Hồ Chí Minh, Vietnam ' + " ";
    var lat_marker = '10.75271';
    var long_marker = '106.675006';
    var color = 'red';
    var point = new google.maps.LatLng(
        parseFloat(lat_marker),
        parseFloat(long_marker));
    var html = "<b>" + name + "</b> <br/>" + color;
    var icon = customIcons[color];
    var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: icon.icon,
        shadow: icon.shadow

    });

    contentString =
        '<div class="content_marker" style="width: 650px;">' +
            '<div id="info-street" style="width: 100%;height: 30px;float: left;text-align: center;">' + name + '</div>' +

            '<div id="siteNotice" style="width: 297px;float: left">' +












            '<table  border="0px" style="width: 290px;">' +
            '<tr>' +
            '<td class="td-top-left" style="height: 65px;width: 130px;text-align: right;vertical-align: top;  border-right: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-right-radius: 5px">' +
            '<img height="70px" width="85px" style="" src="upload/4.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: left">' +
            '<div style="font-family:Arial;font-size:11px; "> Bùi Hữu Nghĩa</div>' + '</td>' +
            '<td class="td-top-right" style="height: 95px;width: 130px;text-align: right;vertical-align: bottom;bottom: auto; border-left: 2px solid #cfcfcf;  border-bottom: 2px solid #cfcfcf;border-bottom-left-radius:5px">' +
            '<img height="65px" style="" width="85px" src="upload/22.jpg"/>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-center-left" style="height: 30px;text-align: center;vertical-align: bottom">' + '<label style="font-size: 11px;">Nghĩa Thục</label>' + '</td>' +
            '<td style="height: 40px;width: 30px;text-align: center;vertical-align: middle">' +
            '<img src="red.png" height="30px" width="33px"/>' +
            '</td>' +
            '<td class="td-center-right" style="text-align: center;height: 30px;vertical-align: top">' + '<label style="font-size: 11px;">Nghĩa Thục</label>' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="td-bottom-left" style="height: 95px;text-align: left;vertical-align: top;border-top: 2px solid #cfcfcf;border-right: 2px solid #cfcfcf;border-top-right-radius: 5px">' +
            '<img height="65px" width="85px" style="" src="upload/44.jpg"/>' + '</td>' +
            '<td style="width: 50px;text-align: right">' + '<div style="font-size:11px;">Bùi Hữu Nghĩa</div> ' + '</td>' +
            '<td class="td-bottom-right" style="height: 95px;border-top: 2px solid #cfcfcf;border-left: 2px solid #cfcfcf;border-top-left-radius: 5px">' +
            '<img height="65px" width="85px" src="upload/8.jpg"/>' + '</td>' +
            '</tr>' +

            '</table>' +

            '</div>' +
            '<div id="info" style="width: 327px;float: right;overflow: hidden">' +
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +


            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Nghĩa Thục' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Bùi Hữu Nghĩa' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '20 giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '10 km/h' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 km/h' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 m/2' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '15 m/2' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '20 giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '20 giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' + '10 giây' + '</td>' +
            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</br>'+



            '<form action="parameter.php" method="post"> '+
            '<table id="example" class="table-marker" style="font-size: 12px;width: 300px;border: 1px solid #cfcfcf;border-collapse: inherit;" cellspacing="0">' +

            '<tr><td colspan="2" style="text-align: center;font-weight:bold ">Điều Chỉnh Thông số</td></tr>'+
            '<tr style="border: 1px solid #cfcfcf;height: 45px;background: #d3e2e4">' +
            '<th style="border: 1px solid #cfcfcf">' + 'Thông số' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Nghĩa Thục' + '</th>' +
            '<th style="border: 1px solid #cfcfcf">' + 'Bùi Hữu Nghĩa' + '</th>' +
            '</tr>' +
            '<tbody>' +

            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + 'Chu Kỳ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2" class="numbers-row">' + '<input type="text" name="cycle" id="partridge" style="width: 30px;text-align:center;font-size:12px" value="20" >' +' giây' + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Vận tốc' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' Mật độ' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">'  + '</td>' +
            '</tr>' +
            '<tr style="background: #f6f6f6">' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Xanh' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_horizontal" style="width: 30px;text-align:center;font-size:12px" value="20">' +' giây' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center">' +'<input type="text" name="time_green_vertical" style="width: 30px;text-align:center;font-size:12px" value="20">'+ ' giây' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center;height: 30px">' + ' T/g Vàng' + '</td>' +
            '<td style="border: 1px solid #cfcfcf;text-align: center" colspan="2">' +'<input type="text" name="time_yellow" style="width: 30px;text-align:center;font-size:12px" value="10">'+ ' giây' +'  ' + '<input type="submit" value="Cập nhật">'+'<input type="hidden" name="id_parama" value="6">' +'</td>' +

            '</tr>' +

            '</tbody>' +
            '</table>' +
            '</form>'+

            '</div>'+
            '</div>';

    bindInfoWindow(marker, map, infoWindow, contentString);

}
//
//mo hop tohia window
function bindInfoWindow(marker, map, infoWindow, html) {
    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
    });
}



//ham lay point chi duonng
function getPoint() {

    var lat_begin = '10.758894';
    var long_begin = '106.677558';
    var lat_end = '10.758164';
    var long_end = '106.675846';
    var lat_long_start = new google.maps.LatLng(lat_begin, long_begin);
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var myTrip = [lat_long_start, lat_long_end];
    var col = 'green';
    //Khoi tao polyline ve duong dan
    var flightPath = new google.maps.Polyline({
        path: myTrip,
        fillColor: 'green',
        strokeColor: 'green',
        strokeOpacity: 0.5,
        strokeWeight: 9,
        fillOpacity: 0.5
        //scale:2
    });
    var icon = markerIcon[col];//mang icon
    //khoi tao marker
    var marker = new google.maps.Marker({
        position: lat_long_start,
        icon: icon.icon

    });
    contentString = '393 An Dương Vương, Quận 5 Hồ Chí Minh, Vietnam , ';//gan bien chuoi hien thi infoWindow
    bindInfoWindow(marker, map, infoWindow, contentString);
    marker.setMap(map);//setup marker vao map
    flightPath.setMap(map);//ran ve duong vao map
    var lat_begin = '10.764345';
    var long_begin = '106.670208';
    var lat_end = '10.765194';
    var long_end = '106.670007';
    var lat_long_start = new google.maps.LatLng(lat_begin, long_begin);
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var myTrip = [lat_long_start, lat_long_end];
    var col = 'green';
    //Khoi tao polyline ve duong dan
    var flightPath = new google.maps.Polyline({
        path: myTrip,
        fillColor: 'green',
        strokeColor: 'green',
        strokeOpacity: 0.5,
        strokeWeight: 9,
        fillOpacity: 0.5
        //scale:2
    });
    var icon = markerIcon[col];//mang icon
    //khoi tao marker
    var marker = new google.maps.Marker({
        position: lat_long_start,
        icon: icon.icon

    });
    contentString = '305 Nguyễn Duy Dương, phường 4, Quận 10 Hồ Chí Min';//gan bien chuoi hien thi infoWindow
    bindInfoWindow(marker, map, infoWindow, contentString);
    marker.setMap(map);//setup marker vao map
    flightPath.setMap(map);//ran ve duong vao map
    var lat_begin = '10.764319';
    var long_begin = '106.670339';
    var lat_end = '10.764431';
    var long_end = '106.670703';
    var lat_long_start = new google.maps.LatLng(lat_begin, long_begin);
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var myTrip = [lat_long_start, lat_long_end];
    var col = 'red';
    //Khoi tao polyline ve duong dan
    var flightPath = new google.maps.Polyline({
        path: myTrip,
        fillColor: 'red',
        strokeColor: 'red',
        strokeOpacity: 0.5,
        strokeWeight: 9,
        fillOpacity: 0.5
        //scale:2
    });
    var icon = markerIcon[col];//mang icon
    //khoi tao marker
    var marker = new google.maps.Marker({
        position: lat_long_start,
        icon: icon.icon

    });
    contentString = '302 Nguyễn Duy Dương, 4 Hồ Chí Minh, Vietnam ';//gan bien chuoi hien thi infoWindow
    bindInfoWindow(marker, map, infoWindow, contentString);
    marker.setMap(map);//setup marker vao map
    flightPath.setMap(map);//ran ve duong vao map
    var lat_begin = '10.764284';
    var long_begin = '106.670177';
    var lat_end = '10.763798';
    var long_end = '106.668108';
    var lat_long_start = new google.maps.LatLng(lat_begin, long_begin);
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var myTrip = [lat_long_start, lat_long_end];
    var col = 'yellow';
    //Khoi tao polyline ve duong dan
    var flightPath = new google.maps.Polyline({
        path: myTrip,
        fillColor: 'yellow',
        strokeColor: 'yellow',
        strokeOpacity: 0.5,
        strokeWeight: 9,
        fillOpacity: 0.5
        //scale:2
    });
    var icon = markerIcon[col];//mang icon
    //khoi tao marker
    var marker = new google.maps.Marker({
        position: lat_long_start,
        icon: icon.icon

    });
    contentString = '189 Vĩnh Viễn, phường 4, Quận 10 Hồ Chí Minh, Viet';//gan bien chuoi hien thi infoWindow
    bindInfoWindow(marker, map, infoWindow, contentString);
    marker.setMap(map);//setup marker vao map
    flightPath.setMap(map);//ran ve duong vao map
    var lat_begin = '10.769074';
    var long_begin = '106.697767';
    var lat_end = '10.769943';
    var long_end = '106.698721';
    var lat_long_start = new google.maps.LatLng(lat_begin, long_begin);
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var myTrip = [lat_long_start, lat_long_end];
    var col = 'yellow';
    //Khoi tao polyline ve duong dan
    var flightPath = new google.maps.Polyline({
        path: myTrip,
        fillColor: 'yellow',
        strokeColor: 'yellow',
        strokeOpacity: 0.5,
        strokeWeight: 9,
        fillOpacity: 0.5
        //scale:2
    });
    var icon = markerIcon[col];//mang icon
    //khoi tao marker
    var marker = new google.maps.Marker({
        position: lat_long_start,
        icon: icon.icon

    });
    contentString = 'Hẻm 100 Lê Thị Hồng Gấm, Nguyễn Thái Bình, Quận 1 ';//gan bien chuoi hien thi infoWindow
    bindInfoWindow(marker, map, infoWindow, contentString);
    marker.setMap(map);//setup marker vao map
    flightPath.setMap(map);//ran ve duong vao map
    var lat_begin = '10.758955';
    var long_begin = '106.6777';
    var lat_end = '10.759109';
    var long_end = '106.678113';
    var lat_long_start = new google.maps.LatLng(lat_begin, long_begin);
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var myTrip = [lat_long_start, lat_long_end];
    var col = 'yellow';
    //Khoi tao polyline ve duong dan
    var flightPath = new google.maps.Polyline({
        path: myTrip,
        fillColor: 'yellow',
        strokeColor: 'yellow',
        strokeOpacity: 0.5,
        strokeWeight: 9,
        fillOpacity: 0.5
        //scale:2
    });
    var icon = markerIcon[col];//mang icon
    //khoi tao marker
    var marker = new google.maps.Marker({
        position: lat_long_start,
        icon: icon.icon

    });
    contentString = '389 An Dương Vương, 3 Hồ Chí Minh, Vietnam ';//gan bien chuoi hien thi infoWindow
    bindInfoWindow(marker, map, infoWindow, contentString);
    marker.setMap(map);//setup marker vao map
    flightPath.setMap(map);//ran ve duong vao map
    var lat_begin = '10.756444';
    var long_begin = '106.6851';
    var lat_end = '10.75604';
    var long_end = '106.683804';
    var lat_long_start = new google.maps.LatLng(lat_begin, long_begin);
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var myTrip = [lat_long_start, lat_long_end];
    var col = 'red';
    //Khoi tao polyline ve duong dan
    var flightPath = new google.maps.Polyline({
        path: myTrip,
        fillColor: 'red',
        strokeColor: 'red',
        strokeOpacity: 0.5,
        strokeWeight: 9,
        fillOpacity: 0.5
        //scale:2
    });
    var icon = markerIcon[col];//mang icon
    //khoi tao marker
    var marker = new google.maps.Marker({
        position: lat_long_start,
        icon: icon.icon

    });
    contentString = '360 Trần Hưng Đạo, phường 1, Quận 5 Hồ Chí Minh, V';//gan bien chuoi hien thi infoWindow
    bindInfoWindow(marker, map, infoWindow, contentString);
    marker.setMap(map);//setup marker vao map
    flightPath.setMap(map);//ran ve duong vao map
    var lat_begin = '10.759356';
    var long_begin = '106.684014';
    var lat_end = '10.756848';
    var long_end = '106.685003';
    var lat_long_start = new google.maps.LatLng(lat_begin, long_begin);
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var myTrip = [lat_long_start, lat_long_end];
    var col = 'red';
    //Khoi tao polyline ve duong dan
    var flightPath = new google.maps.Polyline({
        path: myTrip,
        fillColor: 'red',
        strokeColor: 'red',
        strokeOpacity: 0.5,
        strokeWeight: 9,
        fillOpacity: 0.5
        //scale:2
    });
    var icon = markerIcon[col];//mang icon
    //khoi tao marker
    var marker = new google.maps.Marker({
        position: lat_long_start,
        icon: icon.icon

    });
    contentString = '162 Nguyễn Văn Cừ, Nguyễn Cư Trinh Hồ Chí Minh, Vi';//gan bien chuoi hien thi infoWindow
    bindInfoWindow(marker, map, infoWindow, contentString);
    marker.setMap(map);//setup marker vao map
    flightPath.setMap(map);//ran ve duong vao map

}
//Hien thi custom icon marker cho lat long diem cuoi
function displayMarkerEndPoint() {

    var lat_begin = '10.758894';
    var long_begin = '106.677558';
    var lat_end = '10.758164';
    var long_end = '106.675846';
    var col = 'green';
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var icon = markerIcon[col];//mang custom icon
    var marker = new google.maps.Marker({
        position: lat_long_end,
        icon: icon.icon //ran icon vao
    });
    contentString = '469 An Dương Vương, 3 Hồ Chí Minh, Vietnam ';
    marker.setMap(map);//set up marker

    bindInfoWindow(marker, map, infoWindow, contentString);

    var lat_begin = '10.764345';
    var long_begin = '106.670208';
    var lat_end = '10.765194';
    var long_end = '106.670007';
    var col = 'green';
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var icon = markerIcon[col];//mang custom icon
    var marker = new google.maps.Marker({
        position: lat_long_end,
        icon: icon.icon //ran icon vao
    });
    contentString = '337-376 Nguyễn Duy Dương, phường 4, Quận 10 Hồ Chí Minh, Vietnam ';
    marker.setMap(map);//set up marker

    bindInfoWindow(marker, map, infoWindow, contentString);

    var lat_begin = '10.764319';
    var long_begin = '106.670339';
    var lat_end = '10.764431';
    var long_end = '106.670703';
    var col = 'red';
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var icon = markerIcon[col];//mang custom icon
    var marker = new google.maps.Marker({
        position: lat_long_end,
        icon: icon.icon //ran icon vao
    });
    contentString = '320 Nguyễn Duy Dương, 4 Hồ Chí Minh, Vietnam 10.764431, 106.670703';
    marker.setMap(map);//set up marker

    bindInfoWindow(marker, map, infoWindow, contentString);

    var lat_begin = '10.764284';
    var long_begin = '106.670177';
    var lat_end = '10.763798';
    var long_end = '106.668108';
    var col = 'yellow';
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var icon = markerIcon[col];//mang custom icon
    var marker = new google.maps.Marker({
        position: lat_long_end,
        icon: icon.icon //ran icon vao
    });
    contentString = '318-322 Vĩnh Viễn, phường 4, Quận 10 Hồ Chí Minh, Vietnam ';
    marker.setMap(map);//set up marker

    bindInfoWindow(marker, map, infoWindow, contentString);

    var lat_begin = '10.769074';
    var long_begin = '106.697767';
    var lat_end = '10.769943';
    var long_end = '106.698721';
    var col = 'yellow';
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var icon = markerIcon[col];//mang custom icon
    var marker = new google.maps.Marker({
        position: lat_long_end,
        icon: icon.icon //ran icon vao
    });
    contentString = '36 Lê Thị Hồng Gấm, Nguyễn Thái Bình Hồ Chí Minh, Vietnam ';
    marker.setMap(map);//set up marker

    bindInfoWindow(marker, map, infoWindow, contentString);

    var lat_begin = '10.758955';
    var long_begin = '106.6777';
    var lat_end = '10.759109';
    var long_end = '106.678113';
    var col = 'yellow';
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var icon = markerIcon[col];//mang custom icon
    var marker = new google.maps.Marker({
        position: lat_long_end,
        icon: icon.icon //ran icon vao
    });
    contentString = '377 An Dương Vương, 3 Hồ Chí Minh, Vietnam';
    marker.setMap(map);//set up marker

    bindInfoWindow(marker, map, infoWindow, contentString);

    var lat_begin = '10.756444';
    var long_begin = '106.6851';
    var lat_end = '10.75604';
    var long_end = '106.683804';
    var col = 'red';
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var icon = markerIcon[col];//mang custom icon
    var marker = new google.maps.Marker({
        position: lat_long_end,
        icon: icon.icon //ran icon vao
    });
    contentString = '711-787 Trần Hưng Đạo, phường 1, Quận 5 Hồ Chí Minh, Vietnam ';
    marker.setMap(map);//set up marker

    bindInfoWindow(marker, map, infoWindow, contentString);

    var lat_begin = '10.759356';
    var long_begin = '106.684014';
    var lat_end = '10.756848';
    var long_end = '106.685003';
    var col = 'red';
    var lat_long_end = new google.maps.LatLng(lat_end, long_end);
    var icon = markerIcon[col];//mang custom icon
    var marker = new google.maps.Marker({
        position: lat_long_end,
        icon: icon.icon //ran icon vao
    });
    contentString = '74 Nguyễn Văn Cừ, Nguyễn Cư Trinh Hồ Chí Minh, Vietnam ';
    marker.setMap(map);//set up marker

    bindInfoWindow(marker, map, infoWindow, contentString);

}
//end

google.maps.event.addDomListener(window, 'load', initialize);
//getPoint();
</script>

</head>

<body onload="getMarkerIcon()">
<input type="hidden" value="" id="getParamete" name="getParamete"/>

<div id="idd" name="idd" class="idd"></div>

<div class="option_cities">
    <select class="option_select_city" id="option_select_city" name="option_select_city" onchange="initialize()">
        <option selected="selected" value="10.825108, 106.605099">Chọn các quận</option>
        <option value="10.776111,106.695833">Quận 1</option>
        <option value="10.780833,106.756944">Quận 2</option>
        <option value="10.78,106.679444">Quận 3</option>
        <option value="10.761667,106.7025">Quận 4</option>
        <option value="10.756667,106.666667">Quận 5</option>
        <option value="10.746111,106.636111">Quận 6</option>
        <option value="10.738611,106.726389">Quận 7</option>
        <option value="10.723333,106.627778">Quận 8</option>
        <option value="10.830278,106.8175">Quận 9</option>
        <option value="10.773167,106.670058">Quận 10</option>
        <option value="10.763386,106.646678">Quận 11</option>
        <option value="10.875195,106.649091">Quận 12</option>
        <option value="10.813306,106.651964">Bình Tân</option>
        <option value="10.81195,106.70634">Bình Thạnh</option>
        <option value="10.843332,106.667452">Gò Vấp</option>
        <option value="10.801509,106.678111">Phú Nhuận</option>
        <option value="10.816594,106.651793">Tân Bình</option>
        <option value="10.805213,106.622996"> Tân Phú</option>
        <option value="10.845764,106.743943">Thủ Đức</option>
    </select>
</div>

<div class="guide">
    <div class="guide-all">
        <div class="row">
            <div class="green"></div>
            <div class="span">Đường thông thoáng</div>
        </div>
        <div class="row">
            <div class="yellow"></div>
            <div class="span">Đường sắp tắc nghẽn</div>
        </div>

        <div class="row">
            <div class="red"></div>
            <div class="span">Đường tắc nghẽn</div>
        </div>
    </div>
</div>
<div id="map-canvas"></div>
</body>
</html>